import React from 'react';
import { LogBox } from 'react-native';
import Navigation from './app/navigations/Navigation';

/* LogBox.ignoreAllLogs();
 */
LogBox.ignoreLogs(['VirtualizedLists should never be nested']);



export default function App() {
  return (
     <Navigation 
     /> 
  );
}