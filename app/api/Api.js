import {URL_CARGA_PLAN_TRABAJO} from '../../app/api/Storage'

    const Api = {  
        getCobrosPlanTrabajo: key => {         
            return (async () => {
                return await fetch(URL_CARGA_PLAN_TRABAJO, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        emp_code       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=> {                        
                        const consulta =  await JSON.stringify(responseJson);  
                        return consulta;
                }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();              
        }          
    };
       
      
      
      
      
     
      
      export default Api;