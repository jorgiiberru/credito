import {URL_COBRO_CONSULTA_CLIENTE, URL_COBRO_FUERA_ZONA, URL_COBRO_TOTALES, URL_DET_FACTURA_PRODUCTO, URL_DET_PAGARE, 
    URL_DET_PAGARE_VENCIDOS, URL_DET_PAGARE_VENC_VENTA, URL_FACTURA_CLIENTE, URL_GPI_TOTALES_COBRO} from './Storage'
import { ToastAndroid } from "react-native";

    const CobrosApi = {  

      /* realizar_pago: datos => {
            return ( async ()=> { 
                console.log(datos +"-------->");
                    return await 
                    fetch('http://192.168.0.104:8000/api/novedades/cobro/fuera/zona', {
                        method: 'POST',
                        headers: {
                        Accept: 'application/json',
                                'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            datos                       
                        })                                                                       
                    }); 
            })();
        }, */
        get: key => {         
            return ( async () => {
                return await fetch(URL_COBRO_CONSULTA_CLIENTE, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cedula       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await JSON.stringify(responseJson);
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        },
        getCliente: key => {         
            return ( async () => {
                return await fetch(URL_COBRO_CONSULTA_CLIENTE, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cedula       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{  
                        var jsonArray = JSON.parse(JSON.stringify(responseJson[0])) 
                        return jsonArray;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        },
      getCobrosTotalGPI: key => {         
            return ( async () => {
                return await fetch(URL_GPI_TOTALES_COBRO+key, {
                    method: 'GET'
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        let consulta  =  await responseJson;
                        console.log(consulta.facturas); 
                       return consulta;                        
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        }, 
        /* getCobrosTotalGPI: key => { 
            console.log(URL_GPI_TOTALES_COBRO+key)            
            return ( async () => {
                return await fetch(URL_GPI_TOTALES_COBRO+key, {
                    method: 'GET'
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{   
                    console.log("hola")
                      
                        let consulta  =  await responseJson;
                    return consulta; 
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        console.log(error)
                        return error;
                });                 
            })();        
        }, */

        getCobrosTotal: key => {         
            return ( async () => {
                return await fetch(URL_COBRO_TOTALES, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cedula       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        console.log(responseJson);
                        const consulta  =  await responseJson;
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        },

        get_fact_cliente: key => {         
            return ( async () => {
                return await 
                fetch(URL_FACTURA_CLIENTE, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cliente       : key ,
                    })                                 
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await JSON.stringify(responseJson);
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        },
        get_det_pagare: (key,pagares) => {         
            return ( async () => {
                return await 
                fetch(URL_DET_PAGARE, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        venta       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{     
                            
                    if(responseJson=='[]' || responseJson=='undefined'){
                        alert("Sin Detalles")
                    }else{
                        for (let j = 0; j < responseJson.length; j++) {
                          pagares.push(responseJson[j]);                
                        }
                        return await pagares; 
                    } 
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                }); 

            })();      

        },  
        get_det_pagare_vencido: (key) => {         
            return ( async () => {
                return await 
                fetch(URL_DET_PAGARE_VENCIDOS, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cliente       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{        
                                       
                    const consulta  =  await JSON.stringify(responseJson); 
                    return consulta;
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                }); 

            })();      

        },
        get_det_productos: (key) => {         
            return ( async () => {
                return await                
                fetch(URL_DET_FACTURA_PRODUCTO, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        venta       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{        
                                       
                    const consulta  =  await JSON.stringify(responseJson); 
                    return consulta;
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error" +error, 1000);
                }); 

            })();      

        },        
        get_det_pagare_venc_venta: (key) => {         
            return ( async () => {
                return await 
                fetch(URL_DET_PAGARE_VENC_VENTA, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        venta       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{        
                                       
                    const consulta  =  await JSON.stringify(responseJson); 
                    return consulta;
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                }); 

            })();      

        },
        get_total_pagare: (key) => {         
            return ( async () => {
                return await 
                fetch(URL_DET_PAGARE, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        venta       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{     
                    const consulta  =  await JSON.stringify(responseJson); 
                    return consulta;                    
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                }); 

            })();      

        }, 
        get_plan_trabajo: (key) => {         
            return ( async () => {
                return await 
                fetch(URL_CARGA_PLAN_TRABAJO, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        venta       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{     
                    const consulta  =  await JSON.stringify(responseJson); 
                    return consulta;                    
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                }); 

            })();      

        },                        
    };
       
      
      
      
      
     
      
export default CobrosApi;