import { URL_BUSCAR_UBICACION_CLIENTE, URL_BUSCAR_ULT_UBICACION_FOTO } from "./Storage";


    const FormularioApi = {
        buscarUbicacionClient: (key,tipo) => {         
            return (async () => {
                return await fetch(URL_BUSCAR_UBICACION_CLIENTE, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        solicitud_id       :  key ,
                        tipo : tipo
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{          
                    let dataFinal = await responseJson;
                    if(dataFinal=='Array []'){
                          dataFinal=null;
                    }

                    return dataFinal;
                }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                    return error;
                });                 
            })();              
        },       
        buscarUbicacionGarante: key => {         
            return (async () => {
                return await fetch(URL_BUSCAR_UBICACION_GARANTE, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        solicitud_id       :  key ,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{          
                    let dataFinal = await responseJson;
                    if(dataFinal=='Array []'){
                          dataFinal=null;
                    }

                    return dataFinal;
                }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                    return error;
                });                 
            })();              
        }, 
        buscarUbicacionFoto: (key,tipo) => {         
            return (async () => {
                return await fetch(URL_BUSCAR_ULT_UBICACION_FOTO, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        solicitud_id       :  key ,
                        tipo               :   tipo,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{          
                    let dataFinal = await responseJson;
                    if(dataFinal=='Array []'){
                          dataFinal=null;
                    }
                    return dataFinal;
                }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                    return error;
                });                 
            })();              
        },                  
    };

export default FormularioApi;
