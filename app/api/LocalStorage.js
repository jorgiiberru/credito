import AsyncStorage from "@react-native-async-storage/async-storage";


const LocalStorage = {  
  getIdUser: key => {
    return AsyncStorage.getItem(key).then(value => {
      let dato = JSON.parse(value);
      return dato[0].id;
    });
  },
  getRetiros: key => {
    return AsyncStorage.getItem(key).then(value => {
      
      let dato = JSON.parse(value);    
      return dato;
    });
  },
  setRetiros: (key, value) => {
    return AsyncStorage.setItem(key, value);
  },   
  
  save: (key, value) => {
    return AsyncStorage.setItem(key, JSON.stringify(value));
  }, 
};

export default LocalStorage;