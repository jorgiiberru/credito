import AsyncStorage from "@react-native-async-storage/async-storage";

const LocalStorageUser = {  
  getIdUser: key => {
    return AsyncStorage.getItem(key).then(value => {
      let dato = JSON.parse(value);
      return dato[0].id;
    });
  },
  getDatosUser: key => {
    return AsyncStorage.getItem(key).then(value => {
      let dato = JSON.parse(value);
      return dato;
    });
  },
  getInfoUser: key => {      
    return AsyncStorage.getItem(key).then(value => {
      let dato = JSON.parse(value);
      return dato[0];
    });
  },  
  save: (key, value) => {
    return AsyncStorage.setItem(key, JSON.stringify(value));
  },

  update: (key, value) => {
    return LocalStorage.delete(key).then(() => {
      LocalStorage.save(key, value);
    });
  },

  delete: key => {
    return AsyncStorage.removeItem(key);
  },

  reset: keys => {
    AsyncStorage.multiRemove(keys);
  }
};

export default LocalStorageUser;