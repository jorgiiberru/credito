import {URL_CARGA_PLAN_RETIRO, URL_CARGA_SOLICITUD, URL_RETIRO_DETALLE_PRODU} from './Storage'
import LocalStorageUser from "./LocalStorageUser";
import { ToastAndroid } from "react-native";

    const RetirosApi = {  
        getPlanRetiro: key => {         
            return (async () => {
                let usuario = await LocalStorageUser.getDatosUser(key);   
                console.log(usuario)       
                return await fetch(URL_CARGA_PLAN_RETIRO, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        recaudador_id       : usuario.scode,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta =  await JSON.stringify(responseJson);  
                        return consulta;
                }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();              
        },
        getDetProdRetiro: (key,detalle) =>{
            return  (async () => {
                return await fetch(URL_RETIRO_DETALLE_PRODU, {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                              'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        orden_retiro       :  key ,
                    })
                  }).then((respuesta)=>respuesta.json())
                  .then( async ( responseJson)=>{ 
                     
                    if(responseJson=='[]' || responseJson=='undefined'){
                        alert("Sin Detalles"); 
                    }else{
                        for (let j = 0; j < responseJson.length; j++) {
                            detalle.push(responseJson[j]);                
                        }
                        return await detalle; 
                    }   
                    
                     
                  }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
                });
            })(); 
        }
    };
       
      
      
      
      
     
      
export default RetirosApi;