import {URL_BUSCAR_SOLICITUD, URL_CARGA_SOLICITUD,URL_BUSCAR_SOLICITUD_NOMB,
    URL_BUSCAR_SOLICITUD_APE_NOM} from './Storage'
import LocalStorageUser from "./LocalStorageUser";
import { ToastAndroid } from "react-native";

    const SolicitudesApi = {  
        get: key => {         
            return ( async () => {
                let usuario = await LocalStorageUser.getInfoUser(key);          
                return await fetch(URL_CARGA_SOLICITUD, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        cod_verif       : usuario.id,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await responseJson;
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();        
        },  

        busPorCliente: key=>{
            return ( async () => {
                return await fetch(URL_BUSCAR_SOLICITUD, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        dcedula       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await JSON.stringify(responseJson);
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();   
        },
        busPorNombreCliente: key=>{
            return ( async () => {
                return await fetch(URL_BUSCAR_SOLICITUD_NOMB, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        dnombre       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await JSON.stringify(responseJson);
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();   
        },
        busPorApeNomCliente: key=>{
            return ( async () => {
                return await fetch(URL_BUSCAR_SOLICITUD_APE_NOM, {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                            'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        dnombre       : key,
                    })
                }).then((respuesta)=>respuesta.json())
                .then( async ( responseJson)=>{                        
                        const consulta  =  await JSON.stringify(responseJson);
                        return consulta;
                }).catch((error)=>{
                        ToastAndroid.show("Ha ocurrido un error", 1000);
                        return 'error';
                });                 
            })();   
        }                  
    };
       
      
      
      
      
     
      
export default SolicitudesApi;