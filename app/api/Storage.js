const SERVIDOR      = 'http://app.gramsa.net/api';
const GPI           = 'https://prueba.gramsa.net/pos';
const PRUEBA1       = 'http://192.168.0.112:8000/api'
const PRUEBA        = 'http://192.168.0.109:8000/api'
const PRUEBA2       = 'http://192.168.0.111:8000/api'
const GPIlocalhost  = 'localhost/gpi/ggpi/public/index.php/pos';
const LOGIN         = 'https://prueba.gramsa.net';
const TEST          = 'https://app.gramsa.net/pos';
const TEST12          = 'https://app.gramsa.net/pos';


export const URL = SERVIDOR;
export const URL_SESION = LOGIN +'/pos/credencial' ;
// RUTAS GPI POST
export const URL_GPI_TOTALES_COBRO     = TEST +'/cobros/buscar/'

export const URL_CARGA_SOLICITUD       = URL +'/solicitud/carga'; 
export const URL_CARGA_PLAN_TRABAJO    = URL +'/cobros/carga';  
export const URL_CARGA_PLAN_RETIRO     = URL +'/re_retiros/carga';   

export const URL_FACTURA_CLIENTE       = URL +'/cobros/detalle/venta';
export const URL_DET_FACTURA_PRODUCTO  = URL +'/cobros/detalle/factura';  
export const URL_DET_PAGARE            = URL +'/venta/carga/pagare' ; 
export const URL_DET_PAGARE_VENCIDOS   = URL +'/cobros/pagares/vencidos' ;    
export const URL_DET_PAGARE_VENC_VENTA = URL +'/cobros/pagares/vencidos/venta' ;    


export const URL_COBRO_CONSULTA_CLIENTE = URL +'/cobros/buscar/deuda';
export const URL_COBRO_FUERA_ZONA       = URL +'/novedades/cobro/fuera/zona';
export const URL_COBRO_TOTALES          = URL +'/cobros/total';



export const URL_RETIRO_DETALLE_PRODU  = URL +'/re_retiros/detalle/productos'

export const URL_SOLICITUDES              = URL +'/solicitudes' 
export const URL_FORMULARIOS_SINC         = URL +'/formulario/nuevo'
export const URL_SOLICITUDES_SINC         = URL +'/solicitudes/actualizar/'
export const URL_BUSCAR_SOLICITUD         = URL +'/solicitudes/buscar'; 
export const URL_BUSCAR_SOLICITUD_NOMB    = URL +'/solicitudes/buscar/nombre'; 
export const URL_BUSCAR_SOLICITUD_APE_NOM = URL +'/solicitudes/buscar/apeNom'; 
export const URL_BUSCAR_UBICACION_CLIENTE = URL +'/formulario/buscar/ubicacion'; 
export const URL_BUSCAR_ULT_UBICACION_FOTO= URL +'/formulario/buscar/foto'; 



export const URL_PLAN_TRABAJO          = URL +'/plan_trabajo/'
export const URL_FORM_NOV_COB          = URL +'/novedades/nuevo'
export const URL_RETIROS               = URL +'/cb_orden_retiro/'
export const URL_PALETAS               = URL +'/paletas/'


