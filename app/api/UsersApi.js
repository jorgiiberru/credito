import { URL_SESION } from "./Storage";
const UsersApi = {  
    iniciarSesion: (user,pass) => {         
        return ( async () => {
            return await fetch(URL_SESION+'/'+user+'/'+pass, {
                method: 'GET'
            }).then((respuesta)=>respuesta.json())
            .then( async ( responseJson)=>{                         
                    let consulta  =  await responseJson;
                return consulta; 
            }).catch((error)=>{
                    ToastAndroid.show("Ha ocurrido un error", 1000);
                    return 'error';
            });                 
        })();        
    },
}

export default UsersApi;