import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from "react-native-elements";

export default function CerrarSesion(props) {
    const {isVisible} = props;
    console.log("Esta visible" +isVisible )
    const cancel=()=>{
        setVisible(false);
        setShowModal(false);
    }

    return (
        <View style={{height:130}}>
        <Text style={{fontWeight:'bold',paddingTop:5,paddingBottom:10}}>¿Desea Cerrar sesión?</Text> 
        <Text style={{paddingTop:2,paddingBottom:30}}>
            Revise si tiene datos sin Sincronizar</Text> 
        <View style={{flexDirection:'row-reverse'}}>
            <View style={{width:'30%'}}>
                <Button
                    title="Salir"
                    onPress={console.log("Prueba")}
                    buttonStyle={{backgroundColor:'#E62E42'}}
                />
            </View>
            <View style={{width:'5%'}}>
            </View>
            <View style={{width:'30%'}}>
                <Button
                    title="Cancelar"
                    onPress={cancel}
                />
            </View>                       
        </View>
    </View>
    )
}

const styles = StyleSheet.create({})
