import { card_style, stylesList } from "../styles/Estilos";
import React from 'react'
import { View,Text } from "react-native";

export function Encabezado(props){
    const {txt_info} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={card_style.card_header_one}>
                <Text style={card_style.card_text_one}>
                   {txt_info}
                </Text>
            </View>          
        </View>
    )
}


export function EncabezadoDoble(props){
    const {txt_info,txt_info2} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={card_style.card_header_two}>
                <Text style={stylesList.txt_code}>
                        {txt_info}
                </Text>
                <Text style={stylesList.txt_agencia}>
                        {txt_info2}
                </Text> 
            </View>          
        </View>
    )
}