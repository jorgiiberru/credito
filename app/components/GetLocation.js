import * as Location from 'expo-location';

const GetLocation = {     
      posicionActual: async() => { 
            let posicion = await Location.getCurrentPositionAsync({});
            return await posicion;   
      },  
}

export default GetLocation;