import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from 'react-native'
import Login from "../screens/Login/Login";
import MenuPrincipal from "../screens/Login/MenuPrincipal";

export default function Inicio() {
    const [login, setLogin] = useState()
    useEffect(() => {
        (async () => {
            const user = await AsyncStorage.getItem('@usuario_logueado');
            console.log(user);
            setLogin(JSON.parse(user));
         })();
      }, []);  
    return login == null ? <Login /> : <MenuPrincipal />;
    
}

