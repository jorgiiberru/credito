import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListaSolicitud from '../screens/Solicitudes/ListaSolicitud';

export default function Listados() {
    const [listado, setListado] = useState([]); 

    useEffect(() => {
        (async () => {
            const dataGet = await AsyncStorage.getItem('@solicitud_finalizadas');
            const lista = JSON.parse(dataGet);
            setListado(lista);

         })();
      }, []); 

    return (
        <ListaSolicitud 
            solicitudes={listado}
        />
    )
}

const styles = StyleSheet.create({})
