
import React ,{useState}from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import {FontAwesome, FontAwesome5 ,MaterialIcons,Ionicons,SimpleLineIcons   } from '@expo/vector-icons';
import {cargarDatos,getData,removeValue} from '../api/Storage'
import {useNavigation} from "@react-navigation/native";

const MenuEmergente = (props) => {
  let _menu = null;
  const navigation = useNavigation();
  

  return (
    <View style={props.menustyle}>
              <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <TouchableOpacity onPress={openMenu}>
                  <SimpleLineIcons 
                    name="options-vertical" size={18} 
                    color="#ffff" 
                  />
                </TouchableOpacity>
            }>
            <Menu.Item onPress={cargarDatos} title="Recargar Solicitudes 1" />
            <Menu.Item onPress={syncSolicitud} title="Sincronizar Solicitudes" />
            <Menu.Item onPress={listarSolicitudes} title="Solicitudes Realizadas" />
            <Menu.Item onPress={removeValue} title="Borrar Datos" />

            <Divider />
              <Menu.Item style ={{color:'#e42320',fontWeight:'bold'}} onPress={syncSolicitud} title="Salir" />
          </Menu>  
    </View>
  );
};

export default MenuEmergente;