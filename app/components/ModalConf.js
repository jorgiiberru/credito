import React from 'react'
import { StyleSheet, Text, View,Modal } from 'react-native'

export default function ModalConf(props) {
    const [stateModal] = props;
    const [modal, setModal] = useState(stateModal);

    return (
        <Modal
            animationType={"slide"}
            transparent={false}
            visible={true}
            onRequestClose={() => {alert("Modal has been closed.")}}
          >
         <View style={{marginTop: 22}}>
          <View>
            <Text>Hello World!</Text>

            <TouchableHighlight onPress={() => {
              setModal(!modal)
            }}>
              <Text>Hide Modal</Text>
            </TouchableHighlight>

          </View>
         </View>
        </Modal>
    )
}

const styles = StyleSheet.create({})
