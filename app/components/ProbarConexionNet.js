import  NetInfo from "@react-native-community/netinfo";

const ProbarConexionNet = {     
      conexion: () => { 
            return NetInfo.fetch().then(state => {
                if(state.isConnected){                    
                    return true; 
                }else{
                    return false;
                } 
            });   
      },  
}

export default ProbarConexionNet;