import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import MapView, { AnimatedRegion, Marker } from "react-native-maps";
import { Provider, FAB } from 'react-native-paper';
import { stylesList } from '../styles/Estilos';
import { showLocation } from 'react-native-map-link'
import * as Location from 'expo-location';

export default function VerMapa(props) {
    const {route} = props;
    const {cli_lat,cli_log} = route.params;
    
    const verRuta = async () => {
        let location= await Location.getCurrentPositionAsync({});
        let lat_origen = location.coords.latitude;
        let lon_origen = location.coords.longitude;
        showLocation({
            latitude: cli_lat,
            longitude: cli_log,
            sourceLatitude: lat_origen,        // optionally specify starting location for directions
            sourceLongitude: lon_origen,       // not optional if sourceLatitude is specified
            googleForceLatLon: true,       // optionally force GoogleMaps to use the latlon for the query instead of the title   
            appsWhiteList: ['google-maps'], // optionally you can set which apps to show (default: will show all supported apps installed on device)        
        })         
    };    
    return (
        <Provider>
              <MapView 
                    initialRegion={{
                        latitude: cli_lat,
                        longitude: cli_log,
                        latitudeDelta: 0.00922,
                        longitudeDelta: 0.00421,
                    }}
                        style={{width:'100%',height:'100%'}}>
                            <MapView.Marker
                                coordinate={{
                                    latitude: cli_lat,
                                    longitude:cli_log
                                }}
                                draggable
                            />
                </MapView> 
                <FAB
                    style={stylesList.btn_float_map}
                    small
                    icon="map-marker-up"
                    label="Ver ruta"
                    onPress={verRuta}                   
                />                   
        </Provider>
    )
}

const styles = StyleSheet.create({})
