import React, { useEffect, useState } from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Solicitud from '../screens/Solicitudes/Solicitud';
import ListaSolicitud from '../screens/Solicitudes/ListaSolicitud';
import DatosSolicitante from '../screens/Solicitudes/Deudores/DatosDeudor';
import DatosGarante from '../screens/Solicitudes/Garantes/DatosGarante';
import Formulario from '../screens/Solicitudes/Deudores/Formulario';
import FormularioGarante from '../screens/Solicitudes/Garantes/FormularioGarante';

import MenuEmergente from '../components/MenuEmergente';
import Login from '../screens/Login/Login';
import MenuPrincipal from '../screens/Login/MenuPrincipal';
import SincronizarSolicitudes from '../screens/Solicitudes/Deudores/SincronizarSolicitudes';
import Listados from '../components/Listados';
import FormularioCobranza from '../screens/Cobranzas/FormularioCobranza';
import FormularioRetiro from '../screens/Retiros/FormularioRetiro';
import NovedadesCobranzas from '../screens/Cobranzas/NovedadesCobranzas';
import SyncronizarNovedadCobro from '../screens/Cobranzas/SyncronizarNovedadCobro';
import Retiros from '../screens/Retiros/Retiros';
import VerMapa from '../components/VerMapa';
import FormularioProducto from '../screens/Retiros/FormularioProducto';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Inicio from '../components/Inicio';
import DetalleCuenta from '../screens/Cobranzas/DetalleCuenta';
import DetallePagare from '../screens/Cobranzas/DetallePagare';
import DetalleRetiro from '../screens/Retiros/DetalleRetiro';
import NuevoCobro from '../screens/Cobranzas/NuevoCobro';
import DetalleCuentaBuscar from '../screens/Cobranzas/DetalleCuentaBuscar';
import DetallePagareBuscar from '../screens/Cobranzas/DetallePagareBuscar';
import BuscadorSolicitud from '../screens/Solicitudes/Deudores/Buscador/BuscadorSolicitud';
import Visualizador from '../screens/Solicitudes/Deudores/Buscador/Visualizador';
import DatosClienteBuscado from '../screens/Solicitudes/Deudores/Buscador/DatosClienteBuscado';
import NewRetiroOtro from '../screens/Retiros/NewRetiroOtro';
import VisualizadorFoto from '../screens/Solicitudes/Deudores/Buscador/VisualizadorFoto';
import DatosGaranteBuscado from '../screens/Solicitudes/Deudores/Buscador/DatosGaranteBuscado';
import FotoRetiro from '../screens/Retiros/FotoRetiro';

const Stack = createStackNavigator();
export default function DeudoresStack() {
    
    return (
        <Stack.Navigator
            initialRouteName="Solicitud"
        >
                     <Stack.Screen 
                        name="inicio" 
                        component={Inicio} 
                        options={{
                            headerShown: false,
                        }}         
                    />   
                    <Stack.Screen 
                    name="login" 
                    component={Login} 
                    options={{
                        headerShown: false,
                    }}                                   
                    />                 
            
           
                    <Stack.Screen 
                        name="menu-principal" 
                        component={MenuPrincipal} 
                        options={{  
                            headerShown: false,
                        }}                             
                    />  
            
                    

                <Stack.Screen 
                    name="listaDeudores" 
                    component={Solicitud} 
                    options={{  
                        title:"SOLICITUDES PENDIENTES" ,
                        headerStyle: { backgroundColor: '#e42320' },
                        headerTintColor: '#fff',
                        headerShown: false,
                        headerRight: () => (
                            <MenuEmergente
                                menutext="Menu"
                                menustyle={{ marginRight: 16 }}
                                textStyle={{ color: 'white' }}
                                isIcon={true}
                            /> 
                        ),
                    }}                                        
                />
                         
                        <Stack.Screen 
                            name="datos-deudor" 
                            component={DatosSolicitante} 
                            options={{  title:"Datos Deudor" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                        />  
                        <Stack.Screen 
                            name="datosGarante" 
                            component={DatosGarante} 
                            options={{  title:"Datos Garante" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                        }}                        
                        />                               
                        <Stack.Screen 
                            name="formulario" 
                            component={Formulario} 
                            options={{  title:"Formulario Deudor" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                        }}                        
                        />  
                        <Stack.Screen 
                            name="formulario-garante" 
                            component={FormularioGarante} 
                            options={{  title:"Formulario Garante" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                        }}                        
                        />                     


                    
                        <Stack.Screen 
                            name="datosDeudores" 
                            component={ListaSolicitud} 
                            options={{title:"Formulario",}}                
                        />
                        <Stack.Screen 
                            name="sync-solicitudes" 
                            component={SincronizarSolicitudes} 
                            options={{  title:"Sincronización" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                        />
                        <Stack.Screen 
                            name="sync-nov-cobro" 
                            component={SyncronizarNovedadCobro} 
                            options={{  title:"Sincronización Cobros" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                        />
                        <Stack.Screen 
                            name="list-solicitudes" 
                            component={Listados} 
                            options={{  title:"Solicitudes Realizadas" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                        />
                        <Stack.Screen 
                            name="buscar-solicitud" 
                            component={BuscadorSolicitud} 
                            options={{  title:"Buscar solicitud" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                        />
                              <Stack.Screen 
                                name="list-doc-cliente" 
                                component={DatosClienteBuscado} 
                                options={{  title:"Datos del cliente" ,
                                            headerStyle: { backgroundColor: '#e42320' },
                                            headerTintColor: '#fff'
                                }}                        
                            />
                              <Stack.Screen 
                                name="list-doc-garante" 
                                component={DatosGaranteBuscado} 
                                options={{  title:"Datos del garante" ,
                                            headerStyle: { backgroundColor: '#e42320' },
                                            headerTintColor: '#fff'
                                }}                        
                            />                            
                            <Stack.Screen 
                            name="visualizar" 
                            component={Visualizador} 
                            options={{  title:"Visualizar" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                            />
                            <Stack.Screen 
                            name="visualizar_foto" 
                            component={VisualizadorFoto} 
                            options={{  title:"Visualizar" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                            }}                        
                            />



                        {/* COBRANZAS */}

                        <Stack.Screen 
                            name="form-cobranza" 
                            component={FormularioCobranza} 
                            options={{  
                                title:"SOLICITUDES PENDIENTES" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff',
                                headerShown: false,
                               
                            }}    
                        />  
                    <Stack.Screen 
                            name="new-cobro" 
                            component={NuevoCobro} 
                            options={{  
                                title:"Registrar cobro" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff'   
                            }}    
                        />  
                        <Stack.Screen 
                            name="form-cobranza-novedad" 
                            component={NovedadesCobranzas} 
                            options={{  title:"Datos del cliente" ,
                                        headerStyle: { backgroundColor: '#e42320' },
                                        headerTintColor: '#fff'
                        }}                        
                        />  
                            <Stack.Screen 
                                name="detalle-deudor-cobros" 
                                component={DetalleCuenta} 
                                options={{  title:"Estado de cuenta" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff'
                            }}                        
                            />  
                            <Stack.Screen 
                                name="detalle-deudor-cobros_buscar" 
                                component={DetalleCuentaBuscar} 
                                options={{  title:"Estado de cuenta" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff'
                            }}                        
                            />  
                            <Stack.Screen 
                                name="detalle-pagare" 
                                component={DetallePagare} 
                                options={{  title:"Listado de Pagáres" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff'
                            }}                        
                            />  
                            <Stack.Screen 
                                name="detalle-pagare-buscar" 
                                component={DetallePagareBuscar} 
                                options={{  title:"Listado de Pagáres" ,
                                headerStyle: { backgroundColor: '#e42320' },
                                headerTintColor: '#fff'
                            }}                        
                            />  
                        {/* RETIROS */}

                        <Stack.Screen 
                            name="form-retiro" 
                            component={FormularioRetiro} 
                            options={{  
                                headerShown: false,
                               
                            }}                                                       
                        />
                        
                        <Stack.Screen 
                            name="detalle-retiro" 
                            component={DetalleRetiro} 
                            options={{  title:"Detalle de Retiro" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                                             
                        />
                        
                        <Stack.Screen 
                            name="retiro" 
                            component={Retiros} 
                            options={{  title:"Retiros" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                  
                        />
                        <Stack.Screen 
                            name="new-retiro-otro" 
                            component={NewRetiroOtro} 
                            options={{  title:"Retiro de otro producto" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                  
                        />
                        <Stack.Screen 
                            name="foto_retiro" 
                            component={FotoRetiro} 
                            options={{  title:"Foto de retiro" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                  
                        />

                           <Stack.Screen 
                            name="vista_ubicación" 
                            component={VerMapa} 
                            options={{  title:"Ubicación" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                  
                        />
                        <Stack.Screen 
                            name="form_producto" 
                            component={FormularioProducto} 
                            options={{  title:"Producto" ,
                            headerStyle: { backgroundColor: '#e42320' },
                            headerTintColor: '#fff'
                            }}                                                  
                        />


                        
                    </Stack.Navigator>            
     
    )
}


