import {NavigationContainer} from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import React from 'react'
import DeudoresStack from './DeudoresStack';
const Tab = createBottomTabNavigator();



export default function Navigation() {
    return (
        <NavigationContainer>
            <DeudoresStack></DeudoresStack>
        </NavigationContainer>

    )
}
