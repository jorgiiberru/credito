import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Icon } from 'react-native-elements';
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import {useNavigation} from "@react-navigation/native";

import {stylesList}  from '../../styles/Estilos';
import {color_form} from '../../styles/Colores';
import {MaterialIcons} from '@expo/vector-icons';



export default function DetalleCuenta(props) {
    const {navigation,route} = props;
    const {cli_id} = route.params;
    return (
        <ScrollView>
            <Encabezado 
                id = {cli_id}    
            />
            <DatosCliente  
                route = {route}
            />
            <ListaProducto 
                id = {cli_id}
            /> 
        </ScrollView>
    )
}

function Encabezado(props){
    const {id} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    Cliente #{id} 
                </Text>
            </View>          
        </View>

    )
}

function DatosCliente(props){
    const { route} = props;
    const {cli_id,cli_nomb,cli_dir} = route.params;
    return(
        <View style={stylesList.view_description}> 
            <Text style={stylesList.txt_nombre} >
                   {cli_nomb}
            </Text>
            <Text style={{ fontSize: 11 }}>
                   {cli_dir}
            </Text>
        </View>
    )
}

function ListaProducto(props){
    const {id} = props;
    const [store, setStore] = useState(); 
   
    useEffect(() => {
      (async () => {
        let fact_credito = [];
        const  dataFinal =  JSON.parse(await AsyncStorage.getItem('@venta_credito'));
        for (let i = 0; i < dataFinal.length; i++) {
             if(dataFinal[i].cliente==id){
                fact_credito.push(dataFinal[i])
            }
        }
        setStore(fact_credito);
      })();
    }, []);  

    return (    
        <FlatList
             data={store}
                renderItem={
                    (solicitud) => 
                        <GetFacturas
                            data={solicitud}
                        />
                }
                keyExtractor={(item, index) => index.toString()} 
        /> 
    )
}

function GetFacturas(props) {
    const {data} = props;
    return (
        <View>
            <CardFacturas 
                data={data}
            />
        </View>                      
    )
}


function CardFacturas(props){
    const {data} = props;
    const {id,femi} = data.item;
    const [producto, setProducto] = useState(); 
    const navigation2 = useNavigation();

    useEffect(() => {
        (async () => {
          let detalle_producto = [];
          const  dataFinal =  JSON.parse(await AsyncStorage.getItem('@venta_det'));
          for (let i = 0; i < dataFinal.length; i++) {
               if(dataFinal[i].venta==id){
                   console.log(dataFinal[i].venta +"="+id)
                  detalle_producto.push(dataFinal[i])
              }
          }
          setProducto(detalle_producto);
        })();
      }, []);  
    
     const detalles = () => {
        navigation2.navigate("detalle-pagare",{id});
     };

    
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:color_form,width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                       Factura Crédito #{id.toString()}     
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                        {femi.toString()}
                </Text>                
             </View>
                <Producto 
                    producto = {producto}
                />
                <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row-reverse',
                    borderTopWidth:0.5,borderTopColor:'#B3BAC4',
                }}>
                
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2}}>
                    <TouchableOpacity  onPress={detalles}>
                        <MaterialIcons 
                            name = "fact-check" size = {24} color = "black"
                            style={{alignSelf:'center'}}         />                        
                        <Text  style={{textAlign:'center',fontSize:10}}>Pagáres</Text>
                    </TouchableOpacity>
                </View>                                                                      
             </View>
         </View>
     )
}


function Producto(props) {
    const {producto}    = props;
    console.log("-------------")

    console.log(producto)
    console.log("-------------")
   /*  const {cantidad}    = producto;
 */
    return(
        <View>
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                            paddingVertical:4,paddingLeft:10, alignContent:'center'
                                ,flexDirection:'row',borderBottomWidth:0.3}}>               
                <Text style={{ fontSize: 13, fontWeight: 'bold',width:'15%'}} >
                    Cant
                </Text>
                <Text style={{ fontSize: 13,fontWeight: 'bold' ,width:'75%'}}>
                   Descripción
                </Text>
            </View>
            <FlatList
             data={producto}
                renderItem={
                    (productos) => 
                        <GetProductos 
                            data={productos}
                        />
                      
                }
                keyExtractor={(item, index) => index.toString()} 
            />  
        </View>
         
    )

}


function GetProductos(props) {
    const {data} = props;
    return (
        <View>
            <CardProductos 
                data={data}
            />
        </View>                      
    )
}

function CardProductos(props){
    const {data} = props;
    const {id,descripcion,cantidad} = data.item;    
     return(
         <View >
            <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                            paddingVertical:4, alignContent:'center'
                                ,flexDirection:'row'}}>               
                <Text style={{ fontSize: 12, width:'15%',textAlign:'center'}} >
                    {cantidad}
                </Text>
                <Text style={{ fontSize: 13,width:'75%'}}>
                    {descripcion}
                </Text>
            </View>             
         </View>
     )
}


const styles = StyleSheet.create({})
