import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import {useNavigation} from "@react-navigation/native";

import {stylesList}  from '../../styles/Estilos';
import {color_form} from '../../styles/Colores';
import {MaterialIcons} from '@expo/vector-icons';
import CobrosApi from '../../api/CobrosApi';



export default function DetalleCuentaBuscar(props) {
    const {route} = props;
    console.log("Datitos")
    const {id,cedula,ssri,dir1,facturas} = route.params;
    console.log('dedede'+facturas+id+cedula);
    for (let index = 0; index < facturas.length; index++) {
       console.log( facturas[index]);
        
    }
    return (
        <ScrollView>
          {/*   <Encabezado 
                id = {id}    
            /> */}
           {/*  <DatosCliente  
                route = {route}
            />
            */}
           {/*  <ListaProducto 
                id = {id}
                factura = {facturas}
            />   */}
        </ScrollView>
    )
}

function Encabezado(props){
    const {id} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    Cliente #{id} 
                </Text>
            </View>          
        </View>
    )
}

function DatosCliente(props){
    const { route} = props;
    const {cli_nomb,cli_dir} = route.params;
    return(
        <View style={stylesList.view_description}> 
            <Text style={stylesList.txt_nombre} >
                   {cli_nomb}
            </Text>
            <Text style={{ fontSize: 11 }}>
                   {cli_dir}
            </Text>
        </View>
    )
}

function ListaProducto(props){
    const {id,factura} = props;
    const [store, setStore] = useState();
    useEffect(() => {
      (async () => {
        let fact_credito = [];
        const  dataFinal = factura;
        for (let i = 0; i < factura.length; i++) {
            fact_credito.push(factura[i])
        }
        setStore(fact_credito); 
      })();
    }, []);  

    return (           
      <FlatList
              data={store}
                renderItem={
                    (solicitud) => 
                        <GetFacturas
                           data={solicitud}
                        />
                }
                keyExtractor={(item, index) => index.toString()} 
        /> 
    )
}

function GetFacturas(props) {
    const {data} = props;
    return (
        <View>
            <CardFacturas 
                data={data}
            />
        </View>                      
    )
}


function CardFacturas(props){
    const {data} = props;
    const {venta_id,fecha_venta_factura} = data.item;
    const [producto, setProducto] = useState(); 
    const navigation = useNavigation();

    useEffect(() => {
        (async () => {      

          let dato = JSON.parse(await CobrosApi.get_det_productos(venta_id));
          console.log(dato);
          setProducto(dato);  

        })();
      }, []);  
    
    const detalles = () => {
        /* navigation.navigate("detalle-pagare-buscar",{id}); */
     };

    
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:color_form,width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                      {/*  Factura Crédito #{venta_id.toString()}      */}
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                        {fecha_venta_factura.toString()}
                </Text>                
             </View>
                {/*  <Producto 
                    producto = {producto}
                />  */}
                <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row-reverse',
                    borderTopWidth:0.5,borderTopColor:'#B3BAC4',
                }}>
                
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2}}>
                    <TouchableOpacity  onPress={detalles}>
                        <MaterialIcons 
                            name = "fact-check" size = {24} color = "black"
                            style={{alignSelf:'center'}}         />                        
                        <Text  style={{textAlign:'center',fontSize:10}}>Pagáres</Text>
                    </TouchableOpacity>
                </View>                                                                  
             </View>
         </View>
     )
}


function Producto(props) {
    const {producto}    = props;
    console.log("-------------")

    console.log(producto)
    console.log("-------------")
    

    return(
        <View>
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                            paddingVertical:4,paddingLeft:10, alignContent:'center'
                                ,flexDirection:'row',borderBottomWidth:0.3}}>               
                <Text style={{ fontSize: 13, fontWeight: 'bold',width:'15%'}} >
                    Cant
                </Text>
                <Text style={{ fontSize: 13,fontWeight: 'bold' ,width:'75%'}}>
                   Descripción
                </Text>
            </View>
            <FlatList
             data={producto}
                renderItem={
                    (productos) => 
                        <GetProductos 
                            data={productos}
                        />
                      
                }
                keyExtractor={(item, index) => index.toString()} 
            />  
        </View>
         
    )

}


function GetProductos(props) {
    const {data} = props;
    return (
        <View>
            <CardProductos 
                data={data}
            />
        </View>                      
    )
}

function CardProductos(props){
    const {data} = props;
    const {id,descripcion,cantidad} = data.item;    
     return(
         <View >
            <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                            paddingVertical:4, alignContent:'center'
                                ,flexDirection:'row'}}>               
                <Text style={{ fontSize: 12, width:'15%',textAlign:'center'}} >
                    {cantidad}
                </Text>
                <Text style={{ fontSize: 13,width:'75%'}}>
                    {descripcion}
                </Text>
            </View>             
         </View>
     )
}


const styles = StyleSheet.create({})
