import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Icon } from 'react-native-elements';
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import {stylesList}  from '../../styles/Estilos';
import {color_form} from '../../styles/Colores';
import {MaterialIcons} from '@expo/vector-icons';
import CobrosApi from '../../api/CobrosApi';



export default function DetallePagareBuscar(props) {
    const {route} = props;
    const {id} = route.params;
   return (
        <ScrollView>           
            <ListaPagare 
                id = {id}
            />   
        </ScrollView>
    )
}

 function Encabezado(props){
    const {id} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    Pagáres #
                </Text>
            </View>          
        </View>

    )
}

function ListaPagare(props){
    const {id} = props;
    const [store, setStore] = useState(); 
    const [total, setTotal] = useState(); 
    useEffect(() => {
        (async () => {
          let dato = JSON.parse (await CobrosApi.get_det_pagare_venc_venta(id));
          let totalPagare = (JSON.parse (await CobrosApi.get_total_pagare(id))).length; 
          setStore(dato);
          setTotal(totalPagare);        
        })();
      }, []);  
  
      return (    
          <FlatList
               data={store}
                  renderItem={
                      (solicitud) =>                       
                          <GetPagares
                              data={solicitud}
                              total ={total}
                          />
                  }
                  keyExtractor={(item, index) => index.toString()} 
          /> 
    )
}

function GetPagares(props) {
    const {data,total} = props;
    return (
        <View>
            <CardPagares
                data={data}
                total={total}
            />
        </View>                      
    )
}

function CardPagares(props){
    const {data,total} = props;
    const {fecha,numero,saldo,abono,multa,f_upago,d_atraso} = data.item;
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:color_form,width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                    Pendiente
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                </Text>                
             </View> 
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                           paddingLeft:10, alignContent:'center',paddingVertical:2
                                ,flexDirection:'row'}}>  

                <View style={{width:'70%'}} >
                    <View  style={{width:'100%',flexDirection:'row'}}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold',width:'20%'}} >
                            Cuota :
                        </Text>
                        <Text style={{ fontSize: 11 ,width:'80%'}}>
                            {numero} de {total}
                        </Text>                        
                    </View>
                    <View  style={{width:'100%',flexDirection:'row'}}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold',width:'20%'}} >
                            Fecha :
                        </Text>               
                        <Text style={{ fontSize: 12, width:'80%'}} >
                            {fecha}
                        </Text>
                    </View>
                    <View  style={{width:'100%',flexDirection:'row'}}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold',width:'20%'}} >
                            Multa :
                        </Text>
                        <Text style={{ fontSize: 12 ,width:'80%'}}>
                        $ {multa}
                        </Text>
                    </View> 
                </View>  
                <View style={{width:'30%',alignSelf:'center'}} >
                    <Text style={{textAlign:'center',fontSize:20}}>
                        $ {saldo}
                    </Text>
                </View>
            </View> 
         </View>
     )
}