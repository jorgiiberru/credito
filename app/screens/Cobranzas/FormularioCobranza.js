import {  useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View ,ScrollView,TouchableOpacity} from 'react-native'
import {URL_CARGA_PLAN_TRABAJO,URL_DET_FACTURA_RETIRO,URL_DET_FACTURA_PRODUCTO,URL_FACTURA_CLIENTE,
        URL_DET_PAGARE} from '../../api/Storage'
import Loading from '../../components/Loading';
import ListaNovedadesCobranzas from './ListaNovedadesCobranzas';
import {FontAwesome, FontAwesome5 ,MaterialIcons,Ionicons,SimpleLineIcons   } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { StatusBar } from 'expo-status-bar';
import { Button, Menu, Divider, Provider, FAB } from 'react-native-paper';
import { ToastAndroid } from 'react-native';
import LocalStorageUser from '../../api/LocalStorageUser';
import Api from '../../api/Api';
import { color_rojo } from '../../styles/Colores';
import { Alert } from 'react-native';
import ProbarConexionNet from '../../components/ProbarConexionNet';



export default function FormularioCobranza({navigation}) {
  const [cobranzaNovedad, setStoreCobranzaNovedad] = useState([]); 
  const [store, setStore] = useState([]); 
  const [factVenta, setFactVenta] = useState([]); 

  const [visible, setVisible] = React.useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  const dataTempoSin = [];
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [facturas, setFacturas] = useState(false);
  const dataTempGuardada =[];
  const navigation2 = useNavigation();

  const removeValue = async () => {
    try {
     await AsyncStorage.removeItem('@storage_novedad_cobranza')
     await AsyncStorage.removeItem('@storage_soli_nov_cob')
     await AsyncStorage.removeItem('@storage_nov_cob')
     await AsyncStorage.removeItem('@venta_credito')

     
     console.log("borro todo");
     setStore('');
     setVisible(false);   
    } catch(e) {
      // remove error
    }
  }

  const syncSolicitud=()=>{
    setVisible(false); 
    navigation.navigate("sync-nov-cobro");
  }

  const newCobro=()=>{    
    navigation.navigate("new-cobro");
  }


  const salir=()=>{
      setVisible(false); 
      Alert.alert(
        '¿Desea cerrar la sesión?',
          'Revise si tiene datos sin Sincronizar',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          { text: 'Salir', onPress: () => {navigation2.navigate("login")} }
        ],
        { cancelable: false }
      );
    }    

    const listarSolicitudes=()=>{
      setVisible(false); 
      navigation2.navigate("list-solicitudes");
    }

 
    useEffect(() => {
     /*  const unsubscribe = navigation.addListener('focus', () => {
        (async () => {
          const jsonValue = await AsyncStorage.getItem('@solicitudes_cob_nov');
          const datos = JSON.parse(jsonValue);
          setStore(datos);
          
        })();               
      });
      return unsubscribe; */
    }, [navigation]);


    let user = {id:''}  
    
    const cargarPlanTrabajo = async () =>{
        setVisible(false);
        let usuario = await LocalStorageUser.getInfoUser('@usuario_logueado');
        let estadoConexion = await ProbarConexionNet.conexion(); 
        setIsLoading(true)
        if(estadoConexion){ 
          let dataDescargada = await Api.getCobrosPlanTrabajo(usuario.id);
          const respaldoCobros = await AsyncStorage.getItem('@solicitudes_cob_nov');
          if(dataDescargada!='[]'){
            const datosDescargados = JSON.parse(dataDescargada);
            setStore(datosDescargados);
            console.log(datosDescargados)
            console.log("Data cargada");

          }else{                    
            Alert.alert('Información','No hay cobros nuevos cargados')
          }          
        }else{
          Alert.alert('No tiene Internet','Intente mas tarde la búsqueda')
        }
        setIsLoading(false)

    }

    const cargarDatos = async ()=>{
        setIsLoading(true)
        setVisible(false);
        let usuario = await LocalStorageUser.getInfoUser('@usuario_logueado');        
        user.id =usuario.id;        
        let prueba = await Api.getCobros(user.id);        
        console.log(prueba);             
        await fetch(URL_CARGA_PLAN_TRABAJO, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
                      'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                emp_code       :  usuario.id ,
            })
          }).then((respuesta)=>respuesta.json())
          .then( async ( responseJson)=>{ 
            console.log(responseJson)
            const consulta =  JSON.stringify(responseJson);            
            const respaldoCobros = await AsyncStorage.getItem('@solicitudes_cob_nov');
              if(respaldoCobros!=null){
                let resp = JSON.parse(respaldoCobros);
                console.log("Id Guardados");
                for(let i = 0; i < resp.length;i++){
                  if(resp[i].estado=='SIN'){
                      dataTempGuardada.push(resp[i]);
                      console.log(resp[i])
                  }
                } 
              }  

            if(consulta=='[]' || consulta=='undefined'){
              ToastAndroid.show("No hay data nueva", 1000);
            }                  
            console.log("Id Descargados");
            const jsonValue = JSON.stringify(responseJson);
            await AsyncStorage.setItem('@solicitudes_cob_nov', jsonValue);
            const dataGet = await AsyncStorage.getItem('@solicitudes_cob_nov')
            const datos = JSON.parse(dataGet);

            console.log("Id Comparados");
            for(let i = 0; i < datos.length;i++){             
              for(let j = 0; j < dataTempGuardada.length;j++){
                 if(dataTempGuardada[j].id==datos[i].id){
                    console.log(datos[i].estado+"-"+datos[i].cli_nomb);
                    datos[i]=dataTempGuardada[j];
                    console.log("Son iguales" + j +"-"+i);
                 }
              }
            } 
            const datosMerge = JSON.stringify(datos);
            await AsyncStorage.setItem('@solicitudes_cob_nov', datosMerge); 
            const dataFinal = JSON.parse(await AsyncStorage.getItem('@solicitudes_cob_nov'));
 
            console.log("Lista Final");
            for(let i = 0; i < datos.length;i++){
               console.log(datos[i].id+"-"+datos[i].estado+"-"+ datos[i].cli_nomb); 
             }
           setStore(dataFinal); 

          }).catch((error)=>{
            ToastAndroid.show("Ha ocurrido un error", 1000);
        }); 
        
        console.log("Me quede en el cliente")
        let id_cliente = [];
        let id_factura = [];
        let detalle_retiro_venta = []
        let factura_producto = []
        let pagares = []

        for (let i = 0; i < store.length; i++) {
            id_cliente.push(store[i].cli_id);
            console.log(id_cliente[i]);
        }
        console.log("Me quede en el cliente")
 
        for (let j = 0; j < id_cliente.length; j++) { 
            await fetch(URL_FACTURA_CLIENTE, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                        'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  cliente       :  id_cliente[j] ,
              })
            }).then((respuesta)=>respuesta.json())
            .then( async ( responseJson)=>{  
              console.log(responseJson);           
               if(responseJson=='[]' || responseJson=='undefined'){
                  alert(cliente +" - No tiene productos:")
              }else{
                  for (let k = 0; k < responseJson.length; k++) {
                      console.log(responseJson[k]) 
                      detalle_retiro_venta.push(responseJson[k]);                
                }
              } 
            }).catch((error)=>{
              ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
          });
        }
      
        await AsyncStorage.setItem('@venta_credito',JSON.stringify(detalle_retiro_venta)); 
          for (let l = 0; l < detalle_retiro_venta.length; l++) {
                id_factura.push(detalle_retiro_venta[l].id);
        }

         for (let i = 0; i < id_factura.length; i++) {
          await fetch(URL_DET_FACTURA_PRODUCTO, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
                      'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                venta       :  id_factura[i] ,
            })
          }).then((respuesta)=>respuesta.json())
          .then( async ( responseJson)=>{     
              console.log(responseJson) ;        
            if(responseJson=='[]' || responseJson=='undefined'){
                alert("Sin Detalles")
            }else{
                for (let j = 0; j < responseJson.length; j++) {
                  factura_producto.push(responseJson[j]);                
                } 
            } 
          }).catch((error)=>{
            ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
        }); 
        }
        await AsyncStorage.setItem('@venta_det',JSON.stringify(factura_producto)); 
        const dataFinal = JSON.parse(await AsyncStorage.getItem('@venta_det'));

        for (let ll = 0; ll < dataFinal.length; ll++) {
          console.log(dataFinal[ll].venta +"-"+dataFinal[ll].id) 
        }


        for (let i = 0; i < id_factura.length; i++) {
          await fetch(URL_DET_PAGARE, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
                      'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                venta       :  id_factura[i] ,
            })
          }).then((respuesta)=>respuesta.json())
          .then( async ( responseJson)=>{     
              console.log(responseJson) ;        
            if(responseJson=='[]' || responseJson=='undefined'){
                alert("Sin Detalles")
            }else{
                for (let j = 0; j < responseJson.length; j++) {
                  pagares.push(responseJson[j]);                
                } 
            } 
          }).catch((error)=>{
            ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
        }); 
        }        
        await AsyncStorage.setItem('@cb_pagare',JSON.stringify(pagares)); 
       
   
    
    setIsLoading(false);
    };
  
    const cargar = async () =>{
      console.log("Carga de Datos");
    }
    const regresar=()=>{
      setVisible(false);
      setShowModal(false);
      navigation2.navigate("menu-principal");
    }  
 
    return (
      <Provider>       
        <View>
            <Text> </Text>
            <StatusBar barStyle = "dark-content" hidden = {false} 
            backgroundColor = "#e42320" translucent = {true}/>
        </View>
        <View style={{ flexDirection: 'row'}}>
        <View style={styles.boxStackOne} > 
            <TouchableOpacity onPress={regresar}>
                <Ionicons name="arrow-back" size={24} color="white"  />
            </TouchableOpacity>        
          </View>          
          <View style={styles.boxStack} >
            <Text style={styles.textStack}> 
              PLAN DE TRABAJO 
            </Text>
          </View>
        <View style={styles.boxStack2} >              
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <TouchableOpacity onPress={openMenu}>
                  <SimpleLineIcons 
                    name="options-vertical" size={18} 
                    color="#ffff" 
                  />
                </TouchableOpacity>
            }>
            <Menu.Item onPress={cargarDatos} title="Recargar Solicitudes" />
            <Menu.Item onPress={syncSolicitud} title="Sincronizar Solicitudes" />
            <Menu.Item onPress={cargarPlanTrabajo} title="Solicitudes Realizadas" />
            <Menu.Item onPress={removeValue} title="Borrar Datos" />

            <Divider />
              <Menu.Item style ={{color:'#e42320',fontWeight:'bold'}} onPress={salir} title="Salir" />
          </Menu>    
        </View>
      </View>
            <ScrollView>

              <ListaNovedadesCobranzas 
                  cobranzaNovedad = {store}
                  
                />              
            </ScrollView>
            <Loading isVisible={isLoading} text="Cargando Nuevas solicitudes" />
            <FAB
              style={styles.fab}
              small
              icon="plus"
              onPress={newCobro}
            />
      </Provider>   

   

  
    )
}




const styles = StyleSheet.create({
  boxStack: { width: '70%', height: 60, backgroundColor: '#e42320', justifyContent: 'center' },
  textStack: {fontSize: 22, textAlign: 'center', color: '#fff',fontSize:14,fontWeight:'bold'},
  boxStack2:{ width: '15%', height: 60, backgroundColor: '#e42320', 
  justifyContent: 'center',alignSelf:'center' },
  boxStackOne: { width: '15%', height: 60, backgroundColor: '#e42320',justifyContent: 'center',alignItems:'center' },
  btnIcon:{ backgroundColor:'#e42320',height:50,width:200,},
  fab: {
    position: 'absolute',
    margin: 20,
    right: 0,
    bottom: 0,
    backgroundColor: color_rojo,
    width:55,
    height:55,
    borderRadius:55,
    justifyContent:'center',
    alignItems:'center'
  },
});