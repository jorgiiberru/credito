import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import {useNavigation} from "@react-navigation/native";
export default function ListaNovedadesCobranzas(props) {

    const {cobranzaNovedad} = props;
    const navigation = useNavigation();

     return (
        <View>
              <FlatList
                    data={cobranzaNovedad}
                    renderItem={(cobranzasNovedad) => 
                        <GetCobranzasNovedades
                            cobranzasNovedad={cobranzasNovedad}
                            navigation={navigation}
                        />
                    }
                    keyExtractor={(item, index) => index.toString()}
                /> 
             
        </View>
    )
}

function GetCobranzasNovedades(props) {
    const {cobranzasNovedad,navigation} = props;
    const {estado} = cobranzasNovedad.item; 

    return (
            <View>
                {estado=='IN' ?
                     <CardNovedadesCobranzas 
                     cobranzasNovedad={cobranzasNovedad}
                     navigation={navigation}                      
                     colorDeudor={'#148CB2'}
                     destadoEdit={false}
                     colorBtn={'#148CB2'}                                          
                    />
                    :null
                }
                 {estado=='SIN' ?
                     <CardNovedadesCobranzas 
                     cobranzasNovedad={cobranzasNovedad}
                     navigation={navigation}                      
                     colorDeudor={'#FF5733'}
                     destadoEdit={false}
                     colorBtn={'#148CB2'}                                          
                    />
                    :null
                }
                 {estado=='FI' ?
                     <CardNovedadesCobranzas 
                     cobranzasNovedad={cobranzasNovedad}
                     navigation={navigation}                      
                     colorDeudor={'#239B56'}
                     destadoEdit={false}
                     colorBtn={'#148CB2'}                                          
                    />
                    :null
                }
       
            </View>                      
    )
}


function CardNovedadesCobranzas(props){
    const {cobranzasNovedad,navigation,colorDeudor,destadoEdit,colorBtn} = props;
    const {id,cli_id,cli_ced,cli_nomb,cli_dir,cli_lat,
        cli_log,emp_id,emp_code,age_id,age_nomb,
        saldo,tipo,gtipo,fecha    
    } = cobranzasNovedad.item; 


    const verMapa = () => {
        navigation.navigate("vista_ubicación", {
            cli_lat,cli_log
        });        
    };  
    
     const verDeudor = () => {
        const deuGar = 'DEUDOR';    
        navigation.navigate("form-cobranza-novedad",{
            id,cli_id,cli_ced,cli_nomb,cli_dir,cli_lat,
            cli_log,emp_id,emp_code,age_id,age_nomb,
            saldo,tipo,gtipo,fecha
        });
     };

    
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:colorDeudor,width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                                    
                  # {id.toString()} 
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                        {age_nomb.toString()}  
                        
                </Text>                
             </View>
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',paddingVertical:4,paddingLeft:10}}> 
                <Text style={{ fontSize: 13, fontWeight: 'bold'}} >
                {cli_nomb}
                
                </Text>
                <Text style={{ fontSize: 11 }}>
                    {cli_dir}
                </Text>
            </View>
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row-reverse',
                 borderTopWidth:0.5,borderTopColor:'#B3BAC4',
                }}>
                
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2}}>
                    <TouchableOpacity  onPress={verDeudor}>
                        <Icon name='user' type='font-awesome' color='#000'  size={18} />
                        <Text  style={{textAlign:'center',fontSize:10}}>Deudor</Text>
                    </TouchableOpacity>
                </View> 
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5}}>
                    <TouchableOpacity  onPress={verMapa}>
                        <Icon name='map-marker' type='font-awesome' size={16}/>
                        <Text  style={{textAlign:'center',fontSize:10}}>Ubicación</Text>
                    </TouchableOpacity>
                </View>                                                       
             </View>
         </View>
     )
}

const styles = StyleSheet.create({})


