import React, { useEffect, useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import {Picker} from '@react-native-community/picker';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import * as Location from 'expo-location';
import { Icon ,ButtonGroup,CheckBox} from 'react-native-elements';
import { MaterialCommunityIcons ,FontAwesome } from '@expo/vector-icons'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loading from "../../components/Loading";
import {useNavigation} from "@react-navigation/native";
import {stylesList}  from '../../styles/Estilos';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { format } from "date-fns";
import { Encabezado, EncabezadoDoble } from '../../components/ComponentesComunes';
import Validaciones from '../../utils/Validaciones';
import Cobro from '../../utils/Cobro';

export default function NovedadesCobranzas(props) {
    const {route} = props;   
    const {age_nomb,cli_ced} = route.params;
    let cedula = '# ' +cli_ced;
    return (
        <ScrollView>            
            <EncabezadoDoble
                txt_info =  {cedula}
                txt_info2 = {age_nomb}
            />
            <DatosCliente  
                route = {route}
            />  
            <DatosCuentaCobro />           
            <FormCobranzasNove 
                route = {route}
            /> 
                      
                
        </ScrollView>

    )
}


function GetCalendario(){
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [fecha,setFecha] = useState('2021-02-24');
    
    const showDatePicker = () => {
      setDatePickerVisibility(true);
    };
  
    const hideDatePicker = () => {
      setDatePickerVisibility(false);
    };
    let fechita =[]
    const handleConfirm = (date) => {
      console.log("A date has been picked: ", date);
      var date = new Date(date.toString());
      var formattedDate = format(date, "yyyy-MM-dd");
      setFecha(formattedDate.toString())
      hideDatePicker();
    };
    return(
        <View>
                <Text style={{width:'95%',alignSelf:'center',backgroundColor:'#fff',flexDirection:'row',fontWeight:'bold'}}> 
                                Fecha próxima visita:
                </Text>    
                    <View style={{width:'95%',alignSelf:'center',backgroundColor:'#fff',flexDirection:'row'}}>

                            <View style={{width:'80%'}}>
                                <TouchableOpacity 
                                    style={{width:'98%',backgroundColor:'#E9F2F3',alignSelf:'center',paddingVertical:10
                                            ,borderRadius:8}}
                                    onPress ={showDatePicker}>
                                    <Text style={{textAlign:'center',fontWeight:'bold'}}>{fecha}</Text>
                                </TouchableOpacity>
                            </View>   
                            <View style={{width:'20%'}}>
                                <TouchableOpacity 
                                    style={{width:'100%',paddingVertical:10,alignItems:'center'}}
                                    onPress ={showDatePicker}>
                                    <FontAwesome name = "calendar" size = {21} color = "black" />
                                </TouchableOpacity>
                            </View>                                                        
                    </View>
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />                    
                </View>    
    )
}





function DatosCliente(props){
    const {route} = props;
    const {cli_nomb,cli_dir} = route.params;
    return(

        <View style={stylesList.view_div_prin}>
        <View style={stylesList.view_div_one} >
            <Text style={stylesList.txt_nombre} >
                {cli_nomb}
            </Text> 
            <Text style={{ fontSize: 11 }} >
                {cli_dir}
            </Text>    
        </View> 
        <View style={stylesList.view_div_two} >
            <Deuda /> 
        </View>                                                                              
    </View>

    )
}


function Deuda(props){
    const {cli_id,cli_nomb,cli_dir,factura} = props;
    const navigation = useNavigation();
    const detalleDeuda = () =>{
        
  /*   navigation.navigate("detalle-deudor-cobros_buscar" ,{
            cli_id,cli_nomb,cli_dir,factura
        });  */ 
    }
    
    return(
        <View>             
            <TouchableOpacity   
                        onPress={detalleDeuda}>
                        <View style={{alignSelf:'center'}}>
                            <MaterialCommunityIcons name = "account-details" 
                            size = {25} color = "black" />                            
                        </View>                        
                        <Text  style={stylesList.txt_btn}>Detalles</Text>
            </TouchableOpacity>                      
        </View>    
    )
}
const valor_por_defecto = "Seleccione una opción";

const data = {
        cliente : '',
        respon : '',
        agencia : '',
        tipo : '',
        gtipo : '',
        fecha : '',
        f_promesa : '',
        f_crea : '',
        monto : '',
        lat : '',
        lng : '',
        t_mobi : '',
        t_cli : '',
        ets : '',
        obs : '',
}

    
function FormCobranzasNove(props){
    const {route} = props;
    const {id,cli_id,age_id,emp_code,cli_dir,saldo,tipo,gtipo,fecha} = route.params;
    const novedades = [];
    const cobros = [];
    const [isLoading, setIsLoading] = useState(false);
    const [recibo, setRecibo] = useState('');

    const navigation = useNavigation();
    const cuotaVencida = 21;
/*     let cuotaVencida = 230;
 */
    const getMonto = (text) => {               
        setMonto(text);
    };
    const getRecibo = (text) => {               
        setRecibo(text);
    };


  /*   useEffect(() => {
        (async () => {
            const jsonValue = await AsyncStorage.getItem('@storage_paleta');
            const datos = JSON.parse(jsonValue); 

            for (let i = 0; i < datos.length; i++) {
                if(datos[i].tipo=='PAG'){
                    novedades.push(datos[i]);
                }
                if(datos[i].tipo=='COB'){
                    cobros.push(datos[i]);
                }
            }
            const jsonNovCob = await AsyncStorage.getItem('@storage_nov_cob');
            const datos_nov_cob = JSON.parse(jsonNovCob);   

        })(); 
      }, []);  */

   



    var opc_nov_cob         = ["Seleccione una opción","NOVEDAD","COBRO"];
    var opc_nov             = ["Seleccione una opción","ACUERDO DE PAGO", "CAMBIO DE DOMICILIO", 
                                "CLIENTE FALLECIDO","CLIENTE PRESO","NO ENCONTRADO EN CASA",
                                "PROMESA DE PAGO","SERVICIO TÉCNICO","VISITA DE TRABAJO"];       
    var opc_cob             = ["Seleccione una opción","PARCIAL", "TOTAL"];    
    var deu_gar             = ["Seleccione una opción","CLIENTE", "GARANTE"];

    const opc_novItems = opc_nov.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));
    const opc_nov_cobItems = opc_nov_cob.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const opc_cobItems = opc_cob.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     
    const deu_garItems = deu_gar.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));    
    const [stateOpc_nov, setStateOpc_nov] = useState("Seleccione una opción");
    const [stateOpc_nov_cob, setStateOpc_nov_cob] = useState("Seleccione una opción");
    const [stateOpc_cob, setStateOpc_cob] = useState("Seleccione una opción");  
    const [stateDeu_gar, setStateDeu_gar] = useState("Seleccione una opción");  

    const [date, setDate] = useState('');
    const [monto, setMonto] = useState('');
    const [observacion, setObservacion] = useState('');
    const [stateNov, setNov] = useState(false);
    const [stateCob, setStatCob] = useState(false);
    const [tipoCobro, setTipoCobro] = useState(false);

   /*  data.situacionVida          = stateOpc_nov    
    data.opc_nov_cobes                = stateOpc_nov_cob    
    data.tiempoEnAniosopc_nov_cobes   = stateOpc_cob     */
    let estadoGuardado = false;    

    const guardar= async()=>{
        if(Validaciones.esValorDefecto(stateOpc_nov_cob)){
            if(Validaciones.esNovedadCobro(stateOpc_nov_cob)){  // Si es false es cobro true es Novedad
                console.log("Soy Novedad")
            }else{
                if( Validaciones.esValorDefecto(stateOpc_cob) &&  
                    Validaciones.esValorDefecto(stateDeu_gar)){
                        if(Validaciones.esPagoParcial(stateOpc_cob)){
                            console.log("Es parcial");
                            data.gtipo  = 'COB';
                            data.tipo   = 11;
                            estadoGuardado=true;
                        }else{
                            console.log("Es total");
                            data.gtipo  ='COB';
                            data.tipo   =12;
                            estadoGuardado=true;
                        }

                }else{
                    alert("Elija la opciones de pago, para continuar");
                }                
            }
        }else{
            alert("Elija una opción");
        } 

        if(estadoGuardado){
            var fecha_pag = (format(new Date(),"yyyy-MM-dd HH:mm:ss").toString());
            let id_usuario  = await LocalStorageUser.getIdUser('@usuario_logueado');                    
            let posicionActual = await GetLocation.posicionActual();
            let datos_cobros = {
                cliente     :   id_cliente,
                respon      :   id_usuario,
                agencia     :   6,
                tipo        :   data.tipo,
                gtipo       :   data.gtipo,
                fecha       :   fecha_pag,
                monto       :   saldoCobrado,
                lat         :   posicionActual.coords.latitude,
                lng         :   posicionActual.coords.longitude,
                t_mobi      :   'MOBILE',
                t_cli       :   'DEUDOR',
                ets         :   'SN',
                obs         :   ''
            }
            console.log("Se puede guardar con normalidad");
        }else{
            console.log("No Se puede guardar aún");
        }
        
    } 
    return (

        <View style={{paddingTop:20}}>
            <Loading isVisible={isLoading} text="Guardando Novedad" />

        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>                
           <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS DEL FORMULARIO
           </Text>
        </View>

        
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>

                <View style={{width:'100%',flexDirection:'row'}}>
                        <View style={{width:'100%'}}>
                            <Text style={{ fontWeight:'bold', }}> Tipo de solicitud:</Text> 
                                <Picker
                                        selectedValue={stateOpc_nov_cob}
                                        style={styles.inputAndroid}
                                        onValueChange={(itemValue, itemIndex) =>
                                        setStateOpc_nov_cob(itemValue)
                                    } >
                                    {opc_nov_cobItems}
                                </Picker>                         
                        </View>                       
                    </View>

                    {stateOpc_nov_cob == 'NOVEDAD' ?
                        <View>
                                
                                <Text style={{ fontWeight:'bold', }}> Tipo novedades:</Text>                  
                                <Picker
                                    selectedValue={stateOpc_nov}
                                    style={styles.inputAndroid}
                                    onValueChange={(itemValue, itemIndex) =>
                                    setStateOpc_nov(itemValue)
                                    } >
                                        {opc_novItems}
                                </Picker>
                            
                            {stateOpc_nov =='CLIENTE FALLECIDO'|| stateOpc_nov =='CLIENTE PRESO' 
                            || stateOpc_nov =='CAMBIO DE DOMICILIO'
                                    || stateOpc_nov =='Seleccione una opción'? 
                            null :
                                <GetCalendario />
                                          
                            } 
                            <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>Observación:</Text>
                                <TextInput 
                                    style={{height:100,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                    width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                    multiline={true}
                                    placeholder={'Escriba una observación'}
                                    onChangeText={text => setObservacion(text)}
                                    value={observacion}                  
                                />
                        </View>                       
                    :null
                    } 


                     {stateOpc_nov_cob == 'COBRO' ? 
                     
                     <View>
                            <View style={{width:'100%'}}>
                            <Text style={{ fontWeight:'bold'}}> Número de Recibo:</Text>
                            <TextInput style={{paddingVertical:10,paddingLeft:20,
                                width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,alignSelf:'center'}}
                                keyboardType="numeric" 
                                onChangeText={text => getRecibo(text)}    
                                value={recibo}   
                            />   
                        </View> 
                        <Text style={{ fontWeight:'bold', }}> Tipo de pago:</Text>       
                        <Picker
                            selectedValue={stateOpc_cob}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateOpc_cob(itemValue)
                        } >
                        {opc_cobItems}
                        </Picker> 

                        <Text style={{ fontWeight:'bold', }}> Cliente:</Text>       
                        <Picker
                            selectedValue={stateDeu_gar}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateDeu_gar(itemValue)
                        } >
                        {deu_garItems}
                        </Picker>  
                        
                       
                        <View>
                            
                            <Text style={{ fontWeight:'bold', }}> Monto:</Text>
                                {stateOpc_cob == 'TOTAL' ?           
                                    <View style={{paddingVertical:13,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}>
                                        <Text>{saldo}</Text>                                   
                                    </View>                                                                                 
                                : 
                                    <TextInput style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                        keyboardType="numeric" 
                                        onChangeText={(text => getMonto(text))}    
                                        value={monto}   
                                    />
                                }
                        </View>
                     </View>
                        :                     
                     null}

                        <TouchableOpacity
                            style={{backgroundColor:'#E52859',borderRadius:5,width:'90%',alignSelf:'center',marginVertical:10}}
                            onPress ={guardar}
                        >
                            <Text  style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingVertical:10}}>Guardar</Text>
                        </TouchableOpacity>
              

        </View>
        </View>
                     
        </View>

    )
}

function DatosCuentaCobro( props ){
    const {totales} = props;
    return(
        <View >
            <Encabezado
                txt_info="Datos de la cuenta"
            /> 
            <View style={stylesList.view_buttons}>            
                <View style={{width:'100%',flexDirection:'row',alignSelf:'center',justifyContent:'center'}}>
                    <View style={stylesList.view_celda_30} >
                        <Text style={stylesList.txt_celda}> Cuota Vencida </Text>
                        <Text style={{textAlign:'center'}}> 12,30 </Text>
                        </View>
                    <View  style={stylesList.view_celda_30} >
                        <Text  style={stylesList.txt_celda}> Pendiente </Text>
                        <Text style={{textAlign:'center'}}> $1200,90 </Text>
                    </View> 
                    <View  style={stylesList.view_celda_40} >
                        <Text  style={stylesList.txt_celda}>  </Text>
                        <Text style={{textAlign:'center'}}>  </Text>
                    </View>                                                                                                  
                </View>
            </View> 

             <View style={stylesList.view_buttons2}>            
                <View style={{width:'100%',flexDirection:'row',alignSelf:'center',justifyContent:'center'}}>
                    <View style={stylesList.view_celda_30} >
                        <Text style={stylesList.txt_celda}> Cancelado </Text>
                        <Text style={{textAlign:'center'}}> $25 </Text>
                        </View>
                    <View  style={stylesList.view_celda_30} >
                        <Text  style={stylesList.txt_celda}> Por cancelar </Text>
                        <Text style={{textAlign:'center'}}> $1213,20 </Text>
                    </View> 
                    <View  style={stylesList.view_celda_40} >
                        <Text  style={stylesList.txt_celda}> Total de crédito </Text>
                        <Text style={{textAlign:'center'}}> $1228,20 </Text>
                    </View>
                                                                                                                                        
                </View>
            </View>             
        </View>
    );
}



function DatosSolicitud(props) {
    const navigation2 = useNavigation();
     const {route} = props;
     const {id,cli_nomb,cli_ced,cli_dir,saldo,tipo,gtipo,fecha,cli_id} = route.params;
     console.log(route.params);
     /*
    const {code,deuGar,garante,dnombre,dcedula,dtelefono,ddireccion,dreferencia} = route.params; */
    const [dirCorr, setDirCorr] = useState(false)
    const [direccion, setDireccion] = useState('');

    const editarDireccion = (text) => {
        setDireccion(text)
        datosObservacion.direccion = text;
        console.log(text);
     }; 

    const detalleDeuda = () =>{
        navigation2.navigate("detalle-deudor-cobros",{
            cli_id,cli_nomb,cli_dir
        });  
    }

    return(
        <View>             
             <View style={stylesList.view_buttons}>
                <View style={stylesList.view_btn}>
                    <TouchableOpacity   
                        onPress={detalleDeuda}>
                        <View style={{alignSelf:'center'}}>
                            <MaterialCommunityIcons name = "account-details" 
                            size = {25} color = "black" />                            
                        </View>                        
                        <Text  style={stylesList.txt_btn}>Detalles</Text>
                    </TouchableOpacity>                    
                </View>
                <View style={{width:'75%',flexDirection:'row',alignSelf:'center'}}>
                    <View style={{width:'20%',paddingLeft:10}}>
                        <Text>Deuda</Text>
                    </View>
                    <View style={{width:'70%',alignItems:'flex-start',alignSelf:'center'}}>
                        <Text> $ {saldo.toString()}</Text> 
                    </View>                                                    
                </View>   
            </View>            
        </View>
    )
}



const styles = StyleSheet.create({})

