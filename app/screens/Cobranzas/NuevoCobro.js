import React, { useEffect, useState } from 'react';
import {Text, View } from 'react-native';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import {card_style,stylesList}  from '../../styles/Estilos';
import { FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { color_desa } from '../../styles/Colores';
import CobrosApi from '../../api/CobrosApi';
import {URL_COBRO_FUERA_ZONA } from '../../api/Storage';
import {useNavigation} from "@react-navigation/native";
import {Picker} from '@react-native-community/picker';
import Loading from '../../components/Loading';
import { ToastAndroid } from 'react-native';
import ProbarConexionNet from '../../components/ProbarConexionNet';
import { Alert } from 'react-native';
import { format } from 'date-fns';
import LocalStorageUser from '../../api/LocalStorageUser';
import GetLocation from '../../components/GetLocation';
import { EncabezadoDoble } from '../../components/ComponentesComunes';

export default function NuevoCobro() {
    const [buscar, setBuscar] = useState('');
    const [cliente, setCliente] = useState();
    const [facturas, setFacturas] = useState('');
    const [total, setTotal] = useState('');
    const [datosCobros,setDatosCobros] = useState('');
    const [pagare, setPagares] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [valorVencido, setValorVencido] = useState();
    const [valorPendiente, setValorPendiente] = useState();
    const [valorCancelado, setValorCancelado] = useState();
    const [valorPorCancelar, setValorPorCancelar] = useState();
    const [totalCredito, setTotalCredito] = useState();
    const [agenciaId, setAgenciaId] = useState();

    let detalle_cobro ;
    let totales =[];
/*                 let totales = await CobrosApi.getCobrosTotal(datos_cliente.cedula);
 */   
   const buscando = async () => {             
        let  estadoConexion = await ProbarConexionNet.conexion(); 
        if(estadoConexion){ 
/*             setIsLoading(true);        
 */         const cuenta_cliente = await CobrosApi.get(buscar);
            const datos_cliente = await CobrosApi.getCliente(buscar);
             if(cuenta_cliente!='[]'){             
                detalle_cobro = await CobrosApi.getCobrosTotalGPI(datos_cliente.cedula);
/*                 console.log(detalle_cobro)
 */              /*   setValorVencido(detalle_cobro.valor_vencido.valor_vencido);
                setValorPendiente(detalle_cobro.valor_pendiente.valor_pendiente);
                setValorCancelado(detalle_cobro.valor_cancelado.valor_cancelado);
                setValorPorCancelar(detalle_cobro.valor_por_cancelar.valor_por_cancelar);
                setTotalCredito(detalle_cobro.total_credito.total_credito);
                setAgenciaId(detalle_cobro.datos_personales_cliente.agencia_id);
                setCliente(datos_cliente); 
                setFacturas(detalle_cobro.facturas); */
               /*  setFacturas(); */
               /*  totales = [valor_vencido,valor_pendiente,valor_cancelado,valor_por_cance,total_credito] */;
/*                 setTotal(totales);
 */               

/*                 console.log(totalitos.deuda_pendiente);
              console.log(detalle_cobro);
 */  
/*                 setTotal(totales); 
 */               /*  var datitos = await CobrosApi.getCobrosTotalGPI(datos_cliente.cedula); */

       
/*                 let id_cliente = JSON.parse(cuenta_cliente);
                console.log(datos_cliente.id)

                let factura_vencidas = await CobrosApi.get_fact_cliente(datos_cliente.id);
                let id_facturas =  JSON.parse (await factura_vencidas);             
                let pagareTotal = []
                console.log(datos_cliente.cedula)
                let totales = await CobrosApi.getCobrosTotal(datos_cliente.cedula);
                               
                for (let i = 0; i < id_facturas.length; i++) {
                        pagareTotal = await CobrosApi.get_det_pagare(id_facturas[i],pagares);            
                } */
                /* setCliente(id_cliente); */
/*                 setFacturas(factura_vencidas);
                setPagares(pagareTotal); */
            }else{
                Alert.alert('No tiene Internet','Intente mas tarde la búsqueda')
            } 
        }else{
            setCliente('');
            alert("Es incorrecto el número de cedula");
        }
        setIsLoading(false);
      
    };

    return (
        <ScrollView>            
            <EncabezadoDoble 
                txt_info ="Buscar cliente"             
            />            
                <View style={{ backgroundColor:'#fff',width:'95%', alignSelf:'center',
                    paddingVertical:15,flexDirection:'row'}}> 
                    <View style={{width:'90%'}}>
                        <TextInput 
                            style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                            width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10,
                                }}
                                keyboardType="numeric" 
                                onChangeText={text => setBuscar(text)}    
                                value={buscar}   
                                placeholder={'Ingrese el número de cedula del cliente'}
                        />   
                    </View>
                    <View style={{width:'10%',alignItems:'flex-start',justifyContent:'center'}}>
                        <TouchableOpacity
                            onPress={buscando}
                            >
                            <FontAwesome name = "search" size = {25} color = {color_desa} />  
                        </TouchableOpacity>                       
                    </View>                  
                </View> 
                <Loading isVisible={isLoading} text="Cargando cliente" />
             {/*    {
                   cliente &&
                        <View> */}
                            {/* <DatosCliente 
                                cliente={cliente}
                                facturas={facturas}
                                totales={totales} 
                                agenciaId={agenciaId}
                                valorVencido ={valorVencido}
                                valorPendiente ={valorPendiente}
                                valorCancelado={valorCancelado}
                                valorPorCancelar={valorPorCancelar}
                                totalCredito={totalCredito}
                            />  */}
                           {/*  <FormularioCobros  
                                saldo       = {valorVencido}
                                cedula      = {cliente.cedula}
                                id_cliente  = {cliente.id}
                                agenciaId   = {agenciaId}
                                resetear    = {true}
                            />  */}
                     {/*    </View>
                }         */}                       
        </ScrollView>
    )
}

function Encabezado(props){
    const {txt_info} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={card_style.card_header_one}>
                <Text style={card_style.card_text_one}>
                   {txt_info}
                </Text>
            </View>          
        </View>
    )
}

function  DatosCliente(props){
    const {cliente,facturas,valorVencido,valorPendiente,valorCancelado,valorPorCancelar,totalCredito,agenciaId} = props;
    const {ssri,cedula,dir1,id} = cliente; 

    let num_cedula = '# ' +cedula ;
/*     
 */    return(
        <View>
            <Encabezado 
                txt_info = {num_cedula} 
            />
            <View style={stylesList.view_div_prin}>
                <View style={stylesList.view_div_one} >
                    <Text style={stylesList.txt_nombre} >
                        {ssri}
                    </Text> 
                    <Text style={{ fontSize: 11 }} >
                        {dir1}
                    </Text>    
                </View> 
                <View style={stylesList.view_div_two} >
                    <Deuda 
                        cliente     ={cliente}
                        facturas    ={facturas}                        
                    />
                </View>                                                                              
            </View>
            <DatosCuentaCobro
                valorVencido        ={valorVencido}
                valorPendiente      ={valorPendiente}
                valorCancelado      ={valorCancelado}
                valorPorCancelar    ={valorPorCancelar}
                totalCredito        ={totalCredito}
            />     
        </View>
    )
}


function DatosCuentaCobro( props ){
    const {valorVencido,valorPendiente,valorCancelado,valorPorCancelar,totalCredito} = props;


    return(
        <View >
            <Encabezado
                txt_info="Datos de la cuenta"
            >              
            </Encabezado>
            <View style={stylesList.view_buttons}>            
                <View style={{width:'100%',flexDirection:'row',alignSelf:'center',justifyContent:'center'}}>
                    <View style={stylesList.view_celda_30} >
                        <Text style={stylesList.txt_celda}> Vencido </Text>
                        <Text style={{textAlign:'center'}}> $ {valorVencido}</Text>
                        </View>
                    <View  style={stylesList.view_celda_30} >
                        <Text  style={stylesList.txt_celda}> Pendiente </Text>
                        <Text style={{textAlign:'center'}}> $ {valorPendiente}</Text>
                    </View> 
                    <View  style={stylesList.view_celda_40} >
{/*                     <Text  style={stylesList.txt_celda}> Deuda por cancelar </Text>
                        <Text style={{textAlign:'center'}}> ${totales.deuda_pendiente+totales.deuda_vencida} </Text> */}
                    </View>
                                                                                                      
                </View>
            </View> 

            <View style={stylesList.view_buttons2}>            
                <View style={{width:'100%',flexDirection:'row',alignSelf:'center',justifyContent:'center'}}>
                    <View style={stylesList.view_celda_30} >
                        <Text style={stylesList.txt_celda}> Cancelado </Text>
                        <Text style={{textAlign:'center'}}> $ {valorCancelado}</Text>
                        </View>
                    <View  style={stylesList.view_celda_30} >
                        <Text  style={stylesList.txt_celda}> Por cancelar </Text>
                        <Text style={{textAlign:'center'}}> $ {valorPorCancelar}</Text>
                    </View> 
                    <View  style={stylesList.view_celda_40} >
                        <Text  style={stylesList.txt_celda}> Total de crédito </Text>
                        <Text style={{textAlign:'center'}}> $ {totalCredito}</Text>
                    </View>
                                                                                                                                        
                </View>
            </View>             
        </View>
    );
}

function Deuda(props){
    const {facturas,cliente} = props;
    const {ssri,cedula,dir1,id} = cliente; 
    const navigation = useNavigation();
    const detalleDeuda = () =>{        
       navigation.navigate("detalle-deudor-cobros_buscar" ,{
                id,cedula,ssri,dir1,facturas    
        }); 
    }
    
    return(
        <View>             
            <TouchableOpacity   
                        onPress={detalleDeuda}>
                        <View style={{alignSelf:'center'}}>
                            <MaterialCommunityIcons name = "account-details" 
                            size = {25} color = "black" />                            
                        </View>                        
                        <Text  style={stylesList.txt_btn}>Detalles</Text>
            </TouchableOpacity>                      
        </View>    
    )
}

function FormularioCobros(props){
    const {saldo,cedula,id_cliente,agenciaId} = props;
    const novedades = [];
    const cobros = [];
    const [isLoading, setIsLoading] = useState(false);
    const navigation = useNavigation();
    var opc_cob             = ["Seleccione una opción","PARCIAL", "TOTAL"];    
    var deu_gar             = ["Seleccione una opción","CLIENTE"];
    var defecto             = 'Seleccione una opción';

    const opc_cobItems = opc_cob.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     
    const deu_garItems = deu_gar.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));    

    const [stateOpc_cob, setStateOpc_cob] = useState("Seleccione una opción");  
    const [stateDeu_gar, setStateDeu_gar] = useState("Seleccione una opción");  

    const [monto, setMonto] = useState('');
    const [recibo, setRecibo] = useState('');
    const [tipoPago, setTipoPago] = useState(defecto);
    const [tipoCliente, setTipoCliente] = useState(defecto);
    let saldoCobrado = 0;
    let guardarCobro = 0;
    let idPago = 0;
    // Limpiar Registros 

    const getRecibo = (text) => {               
        setRecibo(text);
    };

    const setTipo = (text) => {
        setStateOpc_cob(text);
        setTipoPago(text);
    };

    const setCliente = (text) => {
        setStateDeu_gar(text);
        setTipoCliente(text);
    };
    
    const getPagos = (text) => {
        setMonto(text);         
    }; 
    const guardar= async()=>{                   
        if(recibo == '' || tipoPago == defecto 
                || tipoCliente==defecto  ){  
                    guardarCobro = false;     
                Alert.alert('No se puede realizar el cobro','LLene todos los datos')               
            }else{
                if(stateOpc_cob=='TOTAL'){
                    if(saldo>0){
                        saldoCobrado = saldo;
                        idPago=12;
                        guardarCobro = true;
                        
                    }else{
                        guardarCobro = false;
                        alert("Debe hacer un pago mayor que cero");
                    }
                }
                if(stateOpc_cob=='PARCIAL'){
                    var montoInt = parseInt(monto);
                    if(monto=='' || montoInt<=0){                    
                        if(monto==''){                            
                            guardarCobro = false;
                            alert("¡Debe agregar cuanto esta pagando el Cliente!");
                        }
                        if(montoInt<=0){
                            guardarCobro = false;
                            alert("Debe hacer un pago mayor que cero");
                        }
                    }else{
                        saldoCobrado = monto;
                        idPago=11;
                        guardarCobro = true;

                    }                
                }
        }
        let  estadoConexion = await ProbarConexionNet.conexion();              
        if(estadoConexion){            
            if(guardarCobro){   
                setIsLoading(true);              
                    var fecha_pag = (format(new Date(),"yyyy-MM-dd HH:mm:ss").toString());
                    let id_usuario  = await LocalStorageUser.getIdUser('@usuario_logueado');                    
                    let posicionActual = await GetLocation.posicionActual();
                    let datos_cobros = {
                        cliente     :   id_cliente,
                        respon      :   id_usuario,
                        agencia     :   agenciaId,      
                        tipo        :   idPago,
                        gtipo       :   'COB',
                        fecha       :   fecha_pag,
                        monto       :   saldoCobrado,
                        lat         :   posicionActual.coords.latitude,
                        lng         :   posicionActual.coords.longitude,
                        t_mobi      :   'MOBILE',
                        t_cli       :   'DEUDOR',
                        ets         :   'SN',
                        obs         :   'Recibo Pago # '+recibo
                    }
                    fetch(URL_COBRO_FUERA_ZONA, {
                        method: 'POST',
                        headers: {
                        Accept: 'application/json',
                                'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(datos_cobros)                                                                       
                    }).then((respuesta)=>respuesta.json())
                    .then(async(responseJson)=>{
                        console.log(responseJson);
                        let datoGuardado =await responseJson;
                        if(datoGuardado==true){
                            console.log(datos_cobros);
                            ToastAndroid.show("Se ha guardado exitosamente el cobro", 1000);
                            navigation.navigate("form-cobranza"); 
                        }else{
                            ToastAndroid.show("Ha ocurrido un error,intente nuevamente guardar", 1000);
                        }
                    } )                          
            }                        
        }else{
            Alert.alert('No tiene Internet','No se puede registrar el pago')
        } 
        setIsLoading(false);          
    } 
    return (

        <View style={{paddingTop:20}}>
        <Loading isVisible={isLoading} text="Guardando cobro" />

        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>                
           <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS DEL FORMULARIO DE COBRO
           </Text>
        </View>        
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    <View style={{width:'100%',flexDirection:'row'}}>
                        <View style={{width:'50%'}}>
                        <Text style={{ fontWeight:'bold', }}> Tipo de solicitud:</Text> 
                        <Text style={{ paddingLeft:5,fontSize:16,paddingVertical:10 }}> COBRO </Text>
                        </View>
                        <View style={{width:'50%'}}>
                            <Text style={{ fontWeight:'bold'}}> Número de Recibo:</Text>
                            <TextInput style={{paddingVertical:10,paddingLeft:10,
                                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,alignSelf:'center'}}
                                keyboardType="numeric" 
                                onChangeText={text => getRecibo(text)}    
                                value={recibo}   
                            />   
                        </View>                        
                    </View>  
                     <View>
                        <Text style={{ fontWeight:'bold', paddingVertical:5 }}> Tipo de pago:</Text>       
                            <Picker
                                selectedValue={stateOpc_cob}
                                onValueChange={(itemValue, itemIndex) =>
                                setTipo(itemValue)
                            } >
                            {opc_cobItems}
                            </Picker>  

                        <Text style={{ fontWeight:'bold', }}> Cliente:</Text>       
                            <Picker
                                selectedValue={stateDeu_gar}
                                onValueChange={(itemValue, itemIndex) =>
                                    setCliente(itemValue)
                                }>
                        {deu_garItems}
                        </Picker>  
                       <View>                            
                            <Text style={{ fontWeight:'bold', }}> Monto:</Text>
                                {stateOpc_cob == 'TOTAL' ?           
                                    <View style={{paddingVertical:13,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}>
                                        <Text>{saldo}</Text>                                  
                                    </View>                                                                                 
                                : 
                                    <TextInput style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                        keyboardType="numeric" 
                                        
                                        onChangeText={text => getPagos(text)}    
                                        value={monto}   
                                    />
                                }
                        </View>
                     </View>

                        <TouchableOpacity
                            style={{backgroundColor:'#E52859',borderRadius:5,width:'90%',alignSelf:'center',marginVertical:10}}
                            onPress ={guardar}
                        >
                            <Text  style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingVertical:10}}>Guardar</Text>
                        </TouchableOpacity>
              

        </View>
        </View>
                     
        </View>

    )
}
