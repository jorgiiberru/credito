import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View,ScrollView, Image, ToastAndroid } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import {useNavigation} from "@react-navigation/native";
import { URL_SESION,URL_PALETAS ,URL_PLAN_TRABAJO,URL_RETIROS,URL_CARGA_SOLICITUD} from '../../api/Storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {color_rojo,color_azul,color_blanco,color_fondo,color_desa, color_form, color_fin} from '../../styles/Colores'
import { FontAwesome, Ionicons,MaterialCommunityIcons,MaterialIcons,SimpleLineIcons} from '@expo/vector-icons'; 
import { Menu, Divider, Provider } from 'react-native-paper';
import { Alert } from 'react-native';
import Modal from '../../components/Modal';
import { Button } from "react-native-elements";
import LocalStorageUser from '../../api/LocalStorageUser';
import { Encabezado } from '../../components/ComponentesComunes';

export default function MenuPrincipal() {
    const [contCredPend, setcontCredPend] = useState(0); 
    const [contCredFin, setcontCredFin] = useState(0); 
    const [contRetPend, setContRetPend] = useState([]); 
    const [contNovCob, setContNovCob] = useState([]); 
    let user = {id:'',tipo:''}
    const [usuario, setUsuario] = useState(); 
    const [tipo, setTipo] = useState(); 

    const listarSolicitudes=()=>{
        navigation.navigate("listaDeudores");
    }    
    const solicitudesRealizadas=()=>{
        navigation.navigate("list-solicitudes");
    }
    const solicitudesPorSyn=()=>{
        navigation.navigate("list-solicitudes");
    }
    const listarCobranzas=()=>{
        navigation.navigate("form-cobranza");
    } 
    
    const listarRetiros=()=>{
        navigation.navigate("form-retiro");
    }     
   
        useEffect(() => {
        (async () => {        
            let keys = await AsyncStorage.getAllKeys();
            console.log(keys);    
            user = { id: ''}
            let user_logueado = await LocalStorageUser.getDatosUser('@usuario_logueado');
            setUsuario(user_logueado);
            setTipo(user_logueado.tipo)
            user.id = user_logueado.scode;
            console.log(user_logueado.scode);
           await fetch(URL_CARGA_SOLICITUD, {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                          'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    cod_verif       :  user_logueado.scode ,
                })
              }).then((respuesta)=>respuesta.json())
              .then( async ( responseJson)=>{                  
                const datos =  JSON.stringify(responseJson);              
                if(datos=='[]' || datos=='undefined'){
                  ToastAndroid.show("No hay data nueva", 1000);
                }else{
                   await AsyncStorage.setItem('@gh_solicitud_descargas',datos);
                        const pendientes = await AsyncStorage.getItem('@gh_solicitud_descargas');
                        const finalizadas = await AsyncStorage.getItem('@solicitud_finalizadas');  
                    if(pendientes!=null){                        
                        setcontCredPend(JSON.parse(pendientes).length);
                    }else{
                        setcontCredPend(0);
                    }
                    if(finalizadas!=null){
                        setcontCredFin(JSON.parse(finalizadas).length);
                    }else{
                        setcontCredFin(0);
                    }
                } 
                }).catch((error)=>{
                  ToastAndroid.show("Ha surgido un error", 1000);
              }); 
         })();
      }, []);  

    return (

        <View style={{backgroundColor:color_blanco,height:'100%'}}>
            <StatusBar barStyle = "dark-content" hidden = {false} 
                backgroundColor = "#e42320" translucent = {true}/>  
                <ScrollView>
                    <DatosUsuario 
                    />
                  {tipo==1 ?
                        <View>
                            <DatosProductos 
                            />
                            <Dashboard 
                                contCredPend = {contCredPend}
                                contCredFin = {contCredFin}
                            />
                            <CabezeraOperacion
                                texto = "Operaciones" 
                            />
                            <TiposOperaciones
                                user_id={usuario}
                            /> 

                           {/*  <BuscarCliente
                                user_id={usuario}
                            />  */}
                        </View>
                     :
                        <View style={{backgroundColor:color_fondo,alignItems:'center'}}>
                            <Text style={{textAlign:'center',paddingTop:15}}>Usted se ha logueado , pero no tiene acceso a estos modulos</Text>
                            <FontAwesome name="lock" size={300} color={color_desa} />
                        </View>  
                   }
                </ScrollView>
                <View style={{paddingVertical:12}}>
                        <Text style={{textAlign:'center',color:color_azul,fontWeight:'bold'}}>
                        Gran Hogar 2021
                        </Text>                
                </View> 
                 
               
            </View>
    )
}

function BuscarSolicitudesAnteriores(){
    const [buscar, setBuscar] = useState('');

    return(     

        <View style={{ backgroundColor:color_azul,width:'100%', alignSelf:'center',
        paddingVertical:10,flexDirection:'row',}}> 
        <View style={{width:'90%'}}>
            <TextInput 
                style={{paddingVertical:5,textAlignVertical:'top',alignSelf:'center',
                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10,
                    }}
                    keyboardType="numeric" 
                    onChangeText={text => setBuscar(text)}    
                    value={buscar}   
                    placeholder={'Ingrese cédula o nombres del cliente...'}
            />   
        </View>
        <View style={{width:'10%',alignItems:'flex-start',justifyContent:'center'}}>
            <TouchableOpacity
                onPress={console.log("de")}
                >
                <FontAwesome name = "search" size = {25} color = {color_desa} />  
            </TouchableOpacity>                       
        </View>                  
    </View>         
    )
}

function DatosUsuario(){
    const [id, setId] = useState(); 
    const [nombre, setNombre] = useState(); 
    const [tipou, setTipoU] = useState(); 
    const [inicial, setInicial] = useState(); 
    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    const [showModal, setShowModal] = useState(false);
    const navigation2 = useNavigation();
    const [user, setUser] = useState(); 

    const [isVisible, setIsVisible] = useState(false);

    const salir=()=>{
        setVisible(false);
        setShowModal(true);
    }  
    
    const cerrarSesion = ()=>{
        (async () => {
            await AsyncStorage.removeItem('@usuario_logueado');
            await AsyncStorage.removeItem('@gh_solicitud_descargas');
            await AsyncStorage.removeItem('@gh_solicitudes');

            await AsyncStorage.removeItem('@formulario_deudor');
            await AsyncStorage.removeItem('@cb_detalle_orden_retiro');
            await AsyncStorage.removeItem('@cb_pagare');
            await AsyncStorage.removeItem('@formulario_garante');
            await AsyncStorage.removeItem('@re_detalle_retiro');
            await AsyncStorage.removeItem('@re_retiro');
            await AsyncStorage.removeItem('@solicitud_finalizadas');
            await AsyncStorage.removeItem('@storage_paleta');
            await AsyncStorage.removeItem('@venta');
            await AsyncStorage.removeItem('@venta_credito');
            await AsyncStorage.removeItem('@venta_det');
            await AsyncStorage.removeItem('@solicitudes_cob_nov');

            datos = {
                id: '',
                nombre:'',
                tipou:''
            }
            setShowModal(false);            
            navigation2.navigate("login");            
         })();
    }

    const cancel=()=>{
        setVisible(false);
        setShowModal(false);
    }

    let datos = {
        id: '',
        nombre:'',
        tipou:''
    }

    useEffect(() => {
        (async () => {            
            let usuario = await LocalStorageUser.getDatosUser('@usuario_logueado');       
            let ini  = (usuario.nombre).substring(-1,1);
            setId(usuario.id);
            setNombre(usuario.nombre +' '+usuario.apellido);
            if(usuario.tipo==1){
                setTipoU('Recaudador');
            }
            setInicial(ini);
         })();
      }, [])    
    

    return(
        <Provider >
            <Modal 
                isVisible={showModal}
                setIsVisible={setShowModal}
            >
                <View style={{height:130}}>
                    <Text style={{fontWeight:'bold',paddingTop:5,paddingBottom:10}}>¿Desea Cerrar sesión?</Text> 
                    <Text style={{paddingTop:2,paddingBottom:30}}>
                        Revise si tiene datos sin Sincronizar</Text> 

                    <View style={{flexDirection:'row-reverse'}}>
                        <View style={{width:'30%'}}>
                            <Button
                                title="Salir"
                                onPress={cerrarSesion}
                                buttonStyle={{backgroundColor:'#E62E42'}}
                            />
                        </View>
                        <View style={{width:'5%'}}>
                        </View>
                        <View style={{width:'30%'}}>
                            <Button
                                title="Cancelar"
                                onPress={cancel}
                            />
                        </View>                       
                        
                    </View>
                    
                   
                </View>
            </Modal>
            <View style={{backgroundColor:color_rojo}}>
            <View style={{flexDirection:'row',height:30}}>   
            </View>
            <View style={{flexDirection:'row'}}>           
                <View style={{width:'30%',backgroundColor:color_rojo,alignItems:'flex-end'}}>
                        <Image 
                                style={{width:'60%', height: 50}}
                                source={require('../../../assets/imagen/logo-gh.png')} 
                                resizeMode="contain"      
                        />
                </View>
                <View style={{width:'60%',backgroundColor:color_rojo,alignItems:'flex-start',
                        justifyContent:'center'}}>
                        <Text style={{fontSize:20,fontWeight:'bold',color:'#fff'}}>GRAN HOGAR</Text>
                </View>

                <View style={{width:'10%',backgroundColor:color_rojo,alignItems:'flex-start',
                        justifyContent:'center'}}>
                        <Menu
                            visible={visible}
                            onDismiss={closeMenu}
                            anchor={
                                <TouchableOpacity onPress={openMenu}>
                                    <SimpleLineIcons 
                                    name="options-vertical" size={18} 
                                    color="#ffff" 
                                />
                                </TouchableOpacity>
                            }>
                            <Menu.Item style ={{color:'#e42320',fontWeight:'bold'}} onPress={salir} title="Salir" />
                        </Menu>    
                </View>

            </View>
                <View style={{flexDirection:'row'}}>   
                    <View style={{width:'20%',backgroundColor:color_blanco,alignItems:'flex-end',
                    paddingVertical:10}}>
                        <View style={{backgroundColor:'#CDDFF5',width:60,borderRadius:50,height:60,
                                        alignItems:'center',justifyContent:'center'}}> 
                            <Text style={{textAlign:'center',fontSize:20}}>{inicial}</Text>
                        </View>               
                    </View>
                    <View style={{width:'80%',backgroundColor:color_blanco,alignItems:'flex-start',justifyContent:'center'}}>
                        <Text style={{paddingLeft:15,fontSize:12,fontWeight:'bold',color:'#000'}}>{nombre}</Text>
                        <Text style={{paddingLeft:15,fontSize:10,color:'#000'}}> {tipou} {/* - MACHALA 1 */}</Text>
                    </View>
                </View>
            </View>
        
            
        </Provider>

    )


}

function DatosProductos(){
    return(
    <View style={{   height:50,width:'100%',backgroundColor:color_blanco,}}>
       
        <View style={{flexDirection:'row'}}>           
           <View style={{   height:60,width:'100%',backgroundColor:color_fondo,
                            borderTopLeftRadius:50,borderTopRightRadius:50,
                            justifyContent:'center'
           }}>
                <Text style={{fontSize:17,color:'#000',paddingLeft:60,
                       }}>
                    Mis productos
                </Text>
           </View>           
        </View>
    </View>
    )
}


function Dashboard(props){
    const {contCredPend,contCredFin} = props;

    return(
    <View style={{ width:'100%',backgroundColor:color_fondo,}}>       
        <View style={{flexDirection:'row',
                    justifyContent:'center',paddingVertical:15}}>           
           <View style={{   height:150,width:'90%',backgroundColor:color_blanco,
                            borderRadius:10,borderWidth:0.5,borderColor:color_desa,
                            /* justifyContent:'center' */
                        }}>
            <View style={{height:30,backgroundColor:color_azul,width:'100%',
                borderTopStartRadius:10,borderTopRightRadius:10,justifyContent:'center'}}>
                        <Text style={{color:color_blanco,fontWeight:'bold',paddingLeft:20}}> Solicitudes </Text>    
            </View>
                <Text style={{fontSize:12,color:'#000',paddingLeft:20,paddingTop:10,fontWeight:'bold'
                       }}>
                    PENDIENTES  :   {contCredPend}
                </Text>
                <Text style={{fontSize:12,color:'#000',paddingLeft:20,paddingTop:10,fontWeight:'bold'
                       }}>
                    FINALIZADAS :   {contCredFin}
                </Text>
           </View>           
        </View>
    </View>
    )
}

function CabezeraOperacion(props){
    const {texto} = props;
    return(
    <View style={{width:'100%'}}>       
        <View style={{flexDirection:'row'}}>           
           <View style={{height:60,width:'100%',backgroundColor:color_fondo,
                            justifyContent:'center'
                        }}>
                <Text style={{fontSize:20,color:'#000',paddingLeft:60,
                       }}>
                    {texto}
                </Text>
           </View>           
        </View>
    </View>
    )
}

function TiposOperaciones(props){
    const {user_id} =props;
    const navigation = useNavigation();

    const listarSolicitudes=()=>{
        navigation.navigate("listaDeudores");
    } 
    const listarRetiros=()=>{
        navigation.navigate("form-retiro");
    }   
    
    const listarCobranzas=()=>{
        navigation.navigate("form-cobranza");
    }  
    let tam = 70;
    return(
    <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                    justifyContent:'center'}}>        
        <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
            <View>
                <TouchableOpacity 
                    onPress={listarSolicitudes}
                    >                    
                    <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                            height:tam,justifyContent:'center',alignItems:'center'}}>
                        <Ionicons name="document-attach" size={30} color='#2E4E7A' />
                    </View>        
                    <Text style={{paddingVertical:10,textAlign:'center'}}>Solicitudes</Text>            
                </TouchableOpacity>                
            </View>
        </View>
        <BuscarCliente 
            user_id={user_id}
        />
      {/*  <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
            <View>
                <TouchableOpacity 
                    onPress={listarCobranzas}

                >                    
                    <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                            height:tam,justifyContent:'center',alignItems:'center'}}>
                        <MaterialCommunityIcons name="account-cash" size={30} color='#2E4E7A' />
                    </View>        
                    <Text style={{paddingVertical:10,textAlign:'center'}}>Cobros</Text>            
                </TouchableOpacity>                
            </View>
        </View> */}
       {/*  <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
            <View>
                <TouchableOpacity 
                    onPress={listarRetiros}            
                >                    
                    <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                            height:tam,justifyContent:'center',alignItems:'center'}}>
                        <MaterialIcons name="assignment-return" size={30} color='#2E4E7A' />
                    </View>        
                    <Text style={{paddingVertical:10,textAlign:'center'}}>Retiros</Text>            
                </TouchableOpacity>                
            </View>
        </View>  */}
     
    </View>
    )
}


function BuscarCliente(props){
    const {user_id} =props;
    const navigation = useNavigation();

    const buscarSolicitud=()=>{
        navigation.navigate("buscar-solicitud");
    }    
   
  
    let tam = 70;
    return(
/*     <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                    justifyContent:'center'}}>         */
        <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
            <View>
                <TouchableOpacity 
                    onPress={buscarSolicitud}
                    >                    
                    <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                            height:tam,justifyContent:'center',alignItems:'center'}}>
                        <MaterialIcons name="person-search" size={30} color='#2E4E7A' />
                    </View>        
                    <Text style={{paddingVertical:10,textAlign:'center'}}>Info Cliente</Text>            
                </TouchableOpacity>                
            </View>
        </View>

/*     </View>
 */    )
}


const styles = StyleSheet.create({
    boxStack: { width: '85%', height: 60, backgroundColor: '#e42320', justifyContent: 'center' },
    textStack: {fontSize: 22, textAlign: 'center', color: '#fff',fontSize:14,fontWeight:'bold'},
    boxStack2:{ width: '15%', height: 60, backgroundColor: '#e42320', 
    justifyContent: 'center',alignSelf:'center' },
    btnIcon:{ backgroundColor:'#e42320',height:50,width:200,}
  });
  
  