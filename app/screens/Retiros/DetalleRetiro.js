import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { CheckBox, Icon } from 'react-native-elements';
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import {useNavigation} from "@react-navigation/native";

import {stylesList}  from '../../styles/Estilos';
import {color_azul, color_fin, color_form, color_rojo,color_confirm} from '../../styles/Colores';
import {MaterialIcons,MaterialCommunityIcons, FontAwesome} from '@expo/vector-icons';

import { Button, Menu, Divider, Provider, FAB } from 'react-native-paper';
import { Alert } from 'react-native';


export default function DetalleRetiro(props) {
    const {navigation,route} = props;
    const {id_cliente,id} = route.params;
    const newRetiroOtro=()=>{ 
        navigation.navigate("new-retiro-otro",{id});
    }
    
    return (
        <Provider >
             <ScrollView>
                <Encabezado 
                    id = {id_cliente}    
                />
                <DatosCliente  
                    route = {route}
                />                
                <ListaProducto 
                    id = {id}
                />                              
            </ScrollView>            
                <FAB
                    style={stylesList.fab}
                    small
                    icon="plus"
                    onPress={newRetiroOtro}
                    /> 
        </Provider>
       
    )
}

function Encabezado(props){
    const {id} = props;
    return(
        <View style={{paddingTop:10}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    Cliente #{id} 
                </Text>
            </View>          
        </View>

    )
}

function DatosCliente(props){
    const { route} = props;
    const {nombre_cliente,direccion_cliente} = route.params;
    return(
        <View style={stylesList.view_description}> 
            <Text style={stylesList.txt_nombre} >
                   {nombre_cliente}
            </Text>
            <Text style={{ fontSize: 11 }}>
                   {direccion_cliente}
            </Text>
        </View>
    )
}

function ListaProducto(props){
    const {id} = props;
    const [store, setStore] = useState(); 
   
    useEffect(() => {
      (async () => {
        let fact_credito = [];        
        const  dataFinal =  JSON.parse(await AsyncStorage.getItem('@re_detalle_retiro'));
            for (let i = 0; i < dataFinal.length; i++) {
                console.log(dataFinal[i].orden_retiro+"----------------------"+id +"<-----")
                if(dataFinal[i].orden_retiro==id){
                    fact_credito.push(dataFinal[i])
                    console.log(dataFinal[i].id)
                }
            }            
         setStore(fact_credito); 
      })();
    }, []);  

    return (    
        <FlatList
             data={store}
                renderItem={
                    (solicitud) => 
                        <GetFacturas
                            data={solicitud}
                        />
                }
                keyExtractor={(item, index) => index.toString()} 
        /> 
    )
}

function GetFacturas(props) {
    const {data} = props;
    return (
        <View>
            <CardFacturas 
                data={data}
            />
        </View>                      
    )
}


function CardFacturas(props){
    const {data} = props;
    const {id,kardex,orden_retiro,nombre,etd_retiro} = data.item;
    const [producto, setProducto] = useState(); 
    const navigation2 = useNavigation();

    useEffect(() => {
        (async () => {
          let detalle_producto = [];
          /* const  dataFinal =  JSON.parse(await AsyncStorage.getItem('@venta_det'));
          for (let i = 0; i < dataFinal.length; i++) {
               if(dataFinal[i].orden_retiro==orden_retiro){
                    console.log(dataFinal[i].orden_retiro +"="+orden_retiro)
                    detalle_producto.push(dataFinal[i])
              }
          }
          setProducto(detalle_producto); */
        })();
      }, []);  
    
     const detalles = () => {
        navigation2.navigate("foto_retiro",{id});
     };

    
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:color_form,width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                       Item # {kardex.toString()}  
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                         {/* {femi.toString()} */}
                </Text>                
             </View>
                <Producto 
                    data = {data}
                /> 
                <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row-reverse',
                    borderTopWidth:0.5,borderTopColor:'#B3BAC4',
                }}>
                
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2}}>
                    <TouchableOpacity  onPress={detalles}>
                        <FontAwesome 
                            name = "camera" size = {22} color = "black"
                            style={{alignSelf:'center'}}         />                        
                        <Text  style={{textAlign:'center',fontSize:10}}>Fotos</Text>
                    </TouchableOpacity>
                </View>                                                                           
             </View>
         </View>
     )
}


function Producto(props) {
    const {data} = props;
    const {nombre,serie,id,etd_retiro}    = data.item;

    const [estadoRetiro, setEstadoRetiro] = useState();
    const [estadoTemp,setEstadoTemp]       = useState(etd_retiro);
    const [estadoColor, setEstadoColor] = useState(color_rojo);
    const [nomb, setNomb] = useState(color_rojo);

    const checkRetiro = (text) => {   
        if(text=='NORE'){
            setEstadoRetiro(true);
            setEstadoTemp('RE');   
            setEstadoColor(color_confirm);
            setNomb("Retirado");
        }
        if(text=='RE'){
            setEstadoRetiro(false);
            setEstadoTemp('NORE'); 
            setEstadoColor(color_rojo);
            setNomb("No Retirado");

        }
    };
   
    useEffect(() => {       
          if(etd_retiro=='NORE'){
            setEstadoRetiro(false);
            setEstadoColor(color_rojo);
            setNomb("No Retirado");

          }          
          if(etd_retiro=='RE'){
            setEstadoRetiro(true);
            setEstadoColor(color_confirm);
            setNomb("Retirado");
          } 
       
      }, []); 
    return(
        <View>
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',
                            paddingVertical:4,paddingLeft:10, alignContent:'center',
                                flexDirection:'row'}}> 

                    <View style={{width:'80%'}}>
                        <Text style={{ fontSize: 12 ,width:'100%',fontWeight:'bold'}}>
                            {nombre}
                        </Text>
                        <Text style={{ fontSize: 12 ,width:'100%'}}>
                            {serie}
                        </Text> 
                        <Text style={{ fontSize: 10 ,width:'100%',color:estadoColor,fontWeight:'bold'}}>
                            {nomb}
                        </Text>                        
                    </View> 
                    <View style={{width:'20%',alignSelf:'flex-start'}}>
                        <CheckBox                         
                            checked={estadoRetiro} 
                            checkedColor='#07B639'
                            onPress={()=>checkRetiro(estadoTemp)}                     
                        />   
                    </View>                   
            </View>
        </View>
         
    )

}




