import React, { useRef, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View,Image} from 'react-native'


import { Camera } from 'expo-camera';

import {Fontisto ,AntDesign } from '@expo/vector-icons';

const pathDefault = '../../../assets/imagen/no-image.png';
const foto ={
    url:        pathDefault,
    urlBase:    ''
}
export default function FormularioProducto() {
    return (
        <View>
            <DescripcionProducto />
            <VistaFoto/>
        </View>
    )
}

function FormProductoRetiro(){
    return(
        <View>
            <Text> Producto </Text>
        </View>
    )
}
function DescripcionProducto(){
    return(
        <View>
            <Text>Producto</Text>
        </View>

    )
}

function VistaFoto(){
    const [errorMsg, setErrorMsg] = useState(null);
    const [hasPermission, setHasPermission] = useState(null);
    const [camara, setCamara] = useState(null);
    const [imagen, setImagen] = useState(foto.url);

    const camRef = useRef(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [open, setOpen] = useState(null);
    const [openCamara, setOpenCamara] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [previsualizar, setPrevisualizar] = useState(false);

    async function takePicture(){
        if(camRef){
           setIsLoading(true);
           const dataPhoto = await camRef.current.takePictureAsync(options);
           foto.urlBase = dataPhoto;
           data.photo = dataPhoto.uri;
           foto.url = dataPhoto.uri
           setImagen(dataPhoto.uri);
           setOpenCamara(false);
           setIsLoading(false);           
        }
      }
  



    return(

        <View style={{paddingTop:20}}>
            <View style={styles.headerCard}>
                <Text style={styles.headerTextCard}>
                    FOTO DEL PRODUCTO
                </Text>
            </View>
            <View style={{backgroundColor:'#fff',width:'95%', 
                alignSelf:'center',paddingVertical:4,paddingLeft:10,borderBottomWidth:1,
                borderBottomColor:'#B3BAC4'}}> 
                <Text style={{ fontSize: 15, fontWeight: 'bold',textAlign:'center'}} >
                    Vista Frontal
                </Text>
            </View>       
            <View style={{alignSelf:'center'}}>
                    <Fontisto name="tv" size={150} color="#5D6967" />                    
            </View>
            
            <View style={{backgroundColor:'#fff',width:'95%', 
                alignSelf:'center',paddingVertical:4,paddingLeft:10,borderBottomWidth:1,
                borderBottomColor:'#B3BAC4'}}> 
               
                <TouchableOpacity
                    style={{width:65,height:65,backgroundColor:'#DDE4E4',borderRadius:5,
                    alignItems:'center',justifyContent:'center'
                }}
                    onPress={()=>alert("duplicar")}
                >
                    <AntDesign name = "camera" size = {24} color = "black" />
               </TouchableOpacity>
            </View>    
          
        </View>
    )
}




const styles = StyleSheet.create({
    headerCard:{
            backgroundColor:'#0061a7',width:'95%',height:40,
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10
    },
    headerTextCard:{
        width:'100%',alignSelf:'flex-start',color:'#fff',
        fontWeight:'bold',paddingVertical:10,paddingLeft:10
    }, 
    viewImages: {
        flexDirection: "row",
        marginLeft: 20,
        marginRight: 20,
        marginTop: 30,
  },
})
