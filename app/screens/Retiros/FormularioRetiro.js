import { useFocusEffect } from '@react-navigation/native';
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import {URL_CARGA_PLAN_RETIRO,URL_RETIRO_DETALLE_PRODU} from '../../api/Storage'
import ListaRetiros from './ListaRetiros';
import { Button, Menu, Divider, Provider, FAB } from 'react-native-paper';
import { StatusBar } from 'expo-status-bar';
import {Ionicons,SimpleLineIcons   } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';
import Loading from '../../components/Loading';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RetirosApi from '../../api/RetirosApi';
import LocalStorage from '../../api/LocalStorage';
import { color_rojo } from '../../styles/Colores';
import LocalStorageUser from '../../api/LocalStorageUser';
import ProbarConexionNet from '../../components/ProbarConexionNet';


export default function FormularioRetiro({navigation}) {
  const [data, setData] = useState([]); 
  const [store, setStore] = useState([]); 

  const [dataDetalleProducto, setDataDetalleProducto] = useState([]); 

  const [visible, setVisible] = React.useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  const soliTempGuard = [];
  const [isLoading, setIsLoading] = useState(false);
  
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      (async () => {
        let retiros_almacenados = await LocalStorage.getRetiros('@re_retiro'); 

        let datos = retiros_almacenados; 
        setStore(datos);        
      })();               
    });
    return unsubscribe;
  }, [navigation]);

  let user = {id:''}  
  const borrarDatos = async ()=>{
    await AsyncStorage.removeItem('@re_retiro')
    console.log("Borrado exitoso");
  }
  const cargarDatos = async ()=>{
    let  estadoConexion = await ProbarConexionNet.conexion(); 
    let dataFinal; 
    let dataDescargada; 

    if(estadoConexion){
        setIsLoading(true);
        let retiros_almacenados   = await LocalStorage.getRetiros('@re_retiro'); 
        let retiros_descargados   = await RetirosApi.getPlanRetiro('@usuario_logueado'); 
        if(retiros_almacenados != null){
          dataDescargada = JSON.parse(retiros_descargados);
          dataFinal = retiros_almacenados; 
          
        }else{
          await LocalStorage.setRetiros('@re_retiro',retiros_descargados);
          dataFinal = JSON.parse(retiros_descargados); 
          console.log(dataFinal);     
        } 
        if(dataFinal !='' ){
          setStore(dataFinal);
          console.log("Esta es mi data");
          let id_orden_retiro = [];
          let detalle_Temporal= [];
          let detalle_retiro_venta = [];
          let factura_producto = [];
          let pagares = [];
          for (let i = 0; i < dataFinal.length; i++) {      
           id_orden_retiro.push(dataFinal[i].id);       
          }
          for (let i = 0; i < id_orden_retiro.length; i++) {
            console.log(id_orden_retiro[i]+"datias")
            detalle_retiro_venta = await RetirosApi.getDetProdRetiro(id_orden_retiro[i],detalle_Temporal);  
            console.log(detalle_retiro_venta);        
          }
          await AsyncStorage.setItem('@re_detalle_retiro',JSON.stringify(detalle_retiro_venta)); 
          const  dataFinalll =  JSON.parse(await AsyncStorage.getItem('@re_detalle_retiro'));
          console.log(dataFinalll)
        }else{
          alert("No hay data nueva");
        }
    }else{
      Alert.alert("No tiene internet","¡Intente mas tarde!");        
    }  
      setVisible(false);    
      setIsLoading(false);

   

    
 /*    await fetch(URL_CARGA_PLAN_RETIRO, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
                  'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            recaudador_id       :  '86258' ,
        })
      }).then((respuesta)=>respuesta.json())
      .then( async ( responseJson)=>{ 
        console.log(responseJson)
        const consulta =  JSON.stringify(responseJson);            
        const respaldoCobros = await AsyncStorage.getItem('@re_retiros');
        console.log(JSON.parse(respaldoCobros.length)); */
         /*  if(respaldoCobros!=null){
            let resp = JSON.parse(respaldoCobros);
            console.log("Id Guardados");            
            for(let i = 0; i < resp.length;i++){
                  console.log(resp[i])
              if(resp[i].etd_retiro=='SIN'){
                  dataTempGuardada.push(resp[i]);
                  
              }
            } 
          }   */
/* 
        if(consulta=='[]' || consulta=='undefined'){
          ToastAndroid.show("No hay data nueva", 1000);
        }       */            
       /*  console.log("Id Descargados");
        const jsonValue = JSON.stringify(responseJson);
        await AsyncStorage.setItem('@re_retiros', jsonValue);
        const dataGet = await AsyncStorage.getItem('@re_retiros')
        const datos = JSON.parse(dataGet);

        console.log("Id Comparados");
        for(let i = 0; i < datos.length;i++){             
          for(let j = 0; j < dataTempGuardada.length;j++){
             if(dataTempGuardada[j].id==datos[i].id){
                console.log(datos[i].etd_retiro+"-"+datos[i].cli_nomb);
                datos[i]=dataTempGuardada[j];
                console.log("Son iguales" + j +"-"+i);
             }
          }
        }  */
      /*   const datosMerge = JSON.stringify(datos);
        await AsyncStorage.setItem('@solicitudes_cob_nov', datosMerge); 
         */

    /*   }).catch((error)=>{
        ToastAndroid.show("Ha ocurrido un error", 1000);
    });  */
   /*  cc
    console.log("Me quede en el cliente")
    let id_orden_retiro = [];
    let detalle_retiro_venta = [];
    let factura_producto = [];
    let pagares = [];

    for (let i = 0; i < store.length; i++) {
        id_orden_retiro.push(store[i].id);
    } */

   /*  console.log(id_orden_retiro);
    console.log("Me quede en el cliente")

    for (let j = 0; j < id_orden_retiro.length; j++) { 
        await fetch(URL_RETIRO_DETALLE_PRODU, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
                    'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              orden_retiro       :  id_orden_retiro[j] ,
          })
        }).then((respuesta)=>respuesta.json())
        .then( async ( responseJson)=>{  
          console.log(responseJson)
           if(responseJson=='[]' || responseJson=='undefined'){
              alert(cliente +" - No tiene productos:")
          }else{
              for (let k = 0; k < responseJson.length; k++) {
                  console.log(responseJson[k].nombre) 
                  detalle_retiro_venta.push(responseJson[k]);                
              }
          } 
        }).catch((error)=>{
          ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
      });
    }
    

    await AsyncStorage.setItem('@re_detalle_retiro',JSON.stringify(detalle_retiro_venta)); 
      for (let l = 0; l < detalle_retiro_venta.length; l++) {
         console.log( l +"--"+detalle_retiro_venta[l].id);
      } */
      
    /* await AsyncStorage.setItem('@venta_credito',JSON.stringify(detalle_retiro_venta)); 
      for (let l = 0; l < detalle_retiro_venta.length; l++) {
            id_factura.push(detalle_retiro_venta[l].id);
      }

     for (let i = 0; i < id_factura.length; i++) {
      await fetch(URL_DET_FACTURA_PRODUCTO, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
                  'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            venta       :  id_factura[i] ,
        })
      }).then((respuesta)=>respuesta.json())
      .then( async ( responseJson)=>{     
          console.log(responseJson) ;        
        if(responseJson=='[]' || responseJson=='undefined'){
            alert("Sin Detalles")
        }else{
            for (let j = 0; j < responseJson.length; j++) {
              factura_producto.push(responseJson[j]);                
            } 
        } 
      }).catch((error)=>{
        ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
    }); 
    }
    await AsyncStorage.setItem('@venta_det',JSON.stringify(factura_producto)); 
    const dataFinal = JSON.parse(await AsyncStorage.getItem('@venta_det'));

    for (let ll = 0; ll < dataFinal.length; ll++) {
      console.log(dataFinal[ll].venta +"-"+dataFinal[ll].id) 
    }

 */
    /* for (let i = 0; i < id_factura.length; i++) {
      await fetch(URL_DET_PAGARE, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
                  'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            venta       :  id_factura[i] ,
        })
      }).then((respuesta)=>respuesta.json())
      .then( async ( responseJson)=>{     
          console.log(responseJson) ;        
        if(responseJson=='[]' || responseJson=='undefined'){
            alert("Sin Detalles")
        }else{
            for (let j = 0; j < responseJson.length; j++) {
              pagares.push(responseJson[j]);                
            } 
        } 
      }).catch((error)=>{
        ToastAndroid.show("Ha ocurrido un error 2" +error, 1000);
    }); 
    }        
    await AsyncStorage.setItem('@cb_pagare',JSON.stringify(pagares)); 
   

 */
       /*  const dataFinal = JSON.parse(await AsyncStorage.getItem('@re_retiros'));
        setStore(dataFinal); 
        setIsLoading(false); */
  };

  const regresar=()=>{
    setVisible(false);
    /* setShowModal(false); */
    navigation.navigate("menu-principal");
  }   
    return (

      <Provider>
        <View>
            <Text> </Text>
            <StatusBar barStyle = "dark-content" hidden = {false} 
            backgroundColor = "#e42320" translucent = {true}/>
        </View>
        <View style={{ flexDirection: 'row'}}>
          <View style={styles.boxStackOne} > 
            <TouchableOpacity onPress={regresar}>
                <Ionicons name="arrow-back" size={24} color="white"  />

              </TouchableOpacity>        
          </View> 
          
          <View style={styles.boxStack} >
            
            <Text style={styles.textStack}> 
              Retiros
            </Text>
        </View>
        <View style={styles.boxStack2} >              
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
                <TouchableOpacity onPress={openMenu}>
                  <SimpleLineIcons 
                    name="options-vertical" size={18} 
                    color="#ffff" 
                  />
                </TouchableOpacity>
            }>
            <Menu.Item onPress={cargarDatos} title="Recargar Solicitudes" />
            <Menu.Item onPress={console.log("")} title="Sincronizar Solicitudes" />
            <Menu.Item onPress={console.log("")} title="Solicitudes Realizadas" />
           <Menu.Item onPress={borrarDatos} title="Borrar Datos" />
 
            <Divider />
              <Menu.Item style ={{color:'#e42320',fontWeight:'bold'}} onPress={console.log("")} title="Salir" />
          </Menu>    
        </View>
      </View>
            <ScrollView>

            {store  ? 
                <ListaRetiros
                  data={store}
                /> 
              :null}
           
                  
            </ScrollView>
            <Loading isVisible={isLoading} text="Cargando Nuevas solicitudes" />
            

      </Provider>   
     



    )
}


const styles = StyleSheet.create({
  boxStack: { width: '70%', height: 60, backgroundColor: '#e42320', justifyContent: 'center' },
  boxStackOne: { width: '15%', height: 60, backgroundColor: '#e42320', 
  justifyContent: 'center',alignItems:'center' },

  textStack: {fontSize: 22, textAlign: 'center', color: '#fff',fontSize:14,fontWeight:'bold'},
  boxStack2:{ width: '15%', height: 60, backgroundColor: '#e42320', 
  justifyContent: 'center',alignSelf:'center' },
  btnIcon:{ backgroundColor:'#e42320',height:50,width:200,}
});


