import React, { useRef, useState } from 'react'
import { View, Text, Image } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Encabezado } from '../../components/ComponentesComunes';
import {  stylesRetiro,stylesAreaFoto } from '../../styles/Estilos';
import { Icon,CheckBox} from 'react-native-elements';
import { color_azul, color_blanco, color_desa, color_form, color_rojo } from '../../styles/Colores';
import { Modal } from 'react-native';
import { SafeAreaView } from 'react-native';
import { Camera } from 'expo-camera';
import Loading from '../../components/Loading';

export default function FotoRetiro() {

    return (
        <ScrollView>
            <View style={{backgroundColor:color_blanco,flex:1}}>
                          
                <VistaFoto 
                    tipo_foto= "frontal"
                /> 
                <VistaFoto 
                    tipo_foto= "lateral izquierda"
                /> 
            </View>
        </ScrollView>

    )
}



function VistaFoto(props){
    const {tipo_foto} = props;
    const camRef = useRef(null);
    const [open, setOpen] = useState(null);

    const pathDefault = '../../../assets/imagen/no-image.png';
    const [openCamara, setOpenCamara] = useState(false);

    const tomarFoto = () =>{
        console.log("Tomando Foto" +tipo_foto );
        setOpen(true);
    }
    return(
        <View style={{paddingTop:15}}>
             <Encabezado 
                    txt_info = "# Kardex 12345"            
                /> 
            <View style={stylesRetiro.fondo_foto}> 
                <Image 
                    style={{width:'100%', height: 400}}
                    source={require(pathDefault)}
                    resizeMode="contain"   
                />  
            </View>
            <TouchableOpacity style={stylesRetiro.view_tom_foto}
                onPress={tomarFoto}
                >
                <Icon name='camera-retro' type='font-awesome' color='#ffff' />
                <Text style={stylesRetiro.txt_tom_foto}>  Foto {tipo_foto}</Text>
            </TouchableOpacity>
            { open &&
                <Modal animationType="slide" transparent ={false} visible={openCamara}> 
               <SafeAreaView style={stylesAreaFoto.safecontainer}>
                    <Camera 
                        style={{flex:1}}
                        ref={camRef}
                    >
                        <View  style={{
                            flex:1,
                            backgroundColor:'transparent',
                            flexDirection:'row'}}>
                        </View>
                    </Camera>
                </SafeAreaView> 
                <View >     

            </View>
x        </Modal>
            }            
        </View>
    )
}