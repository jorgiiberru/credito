import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import {useNavigation} from "@react-navigation/native";


export default function ListaRetiros(props) {
    const {data} = props;
    const navigation = useNavigation();

    

     return (
        <View>
              <FlatList
                    data={data}
                    renderItem={(datos) => 
                        <GetRetiros
                            datos={datos}
                            navigation={navigation}
                        />                    
                    }
                    keyExtractor={(item, index) => index.toString()}
                /> 
        </View>
    )
}
function GetRetiros(props) {
    const {datos,navigation} = props;    
    return (
            <View>
               <CardNovedadesCobranzas 
                        datos={datos}
                        navigation={navigation}
                        
                        /*
                        colorDeudor={'#0061a7'}
                        destadoEdit={false}
                        colorBtn={'#148CB2'} */
                                            
                />
                              
            </View>                      
    )
}


function CardNovedadesCobranzas(props){
    
    const {datos,navigation/* ,colorDeudor,destadoEdit,colorBtn */} = props;
  /*   id,etd_retiro,fecha_retiro,observacion_retiro,
            agencia,calific_cliente,nombre_cliente,
            cedula_cliente,direccion_cliente,telefono_cliente,
            zona_cliente,recaudador_id,factura_id,latitud_cliente,
            longitud_cliente,id_cliente */
    const {id,nombre_cliente,cedula_cliente,direccion_cliente,telefono_cliente,
            agencia,factura_id,latitud_cliente,longitud_cliente,id_cliente} = datos.item; 
    const cli_lat =latitud_cliente;
    const cli_log =longitud_cliente;
/*     const {dcedula,dnombre,destado,ddireccion,dreferencia,dtelefono,code,id,cod_verif,
           gcedula,gnombre,gestado,gdireccion,greferencia,gtelefono,agencia} = solicitud.item; */

    const verMapa = () => {
        navigation.navigate("vista_ubicación", {
            cli_lat,cli_log
        }); 

    };  
     const verDeudor = () => {
        const deuGar = 'DEUDOR';    
        navigation.navigate("detalle-retiro" , {
            id,nombre_cliente,cedula_cliente,direccion_cliente,telefono_cliente,
            agencia,factura_id,latitud_cliente,longitud_cliente,id_cliente
        } );
     };

     

    
     return(
         <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:'#148CB2',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingLeft:10}}>
                                    # {id.toString()} 
               </Text>
                <Text style={{width:'50%',textAlign:'right',color:'#fff',
                                fontWeight:'bold',paddingVertical:2,paddingRight:10}}>
                                     {agencia.toString()}
                </Text>                
             </View>
             <View style={{backgroundColor:'#fff',width:'95%', alignSelf:'center',paddingVertical:4,paddingLeft:10}}> 
                <Text style={{ fontSize: 13, fontWeight: 'bold'}} >
                                {nombre_cliente.toString()}
                
                </Text>
                <Text style={{ fontSize: 11 }}>
                    {direccion_cliente.toString()}               
                    </Text>
            </View>
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row-reverse',
                 borderTopWidth:0.5,borderTopColor:'#B3BAC4',
                }}>
                
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:2}}>
                    <TouchableOpacity  onPress={verDeudor}>
                        <Icon name='user' type='font-awesome' color='#000'  size={18} />
                        <Text  style={{textAlign:'center',fontSize:10}}>Deudor</Text>
                    </TouchableOpacity>
                </View> 
               
                <View style={{backgroundColor:'#fff',width:'25%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5}}>
                    <TouchableOpacity  onPress={verMapa}>
                        <Icon name='map-marker' type='font-awesome' size={16}/>
                        <Text  style={{textAlign:'center',fontSize:10}}>Ubicacion</Text>
                    </TouchableOpacity>
                </View>                                                       
             </View>
         </View>
     )
}

const styles = StyleSheet.create({})


