import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState } from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Encabezado } from '../../components/ComponentesComunes'

export default function NewRetiroOtro(props) {
    const {route} = props;
    const {id} = route.params;

    return (
        
        <View>
            <Encabezado
                txt_info= "Datos del producto"            
            />
            <DatosProducto 
                id={id}
            />
        </View>
    )
}

function DatosProducto(props){
    const {id} = props;

    const [nombreProd, setNombreProd] = useState('');
    const [numSerie, setNumSerie] = useState('');

    const getProducto = (text) => {               
        setNombreProd(text);
    };
    const getNumSerie = (text) => {               
        setNumSerie(text);
    };
    const guardar = async () => { 
        if(nombreProd!='' && numSerie!=''){
            const  dataFinal =  JSON.parse(await AsyncStorage.getItem('@re_detalle_retiro'));
            let otro_retiro = {
                "id":                       id, 
                "nombre":                   nombreProd,   
                "serie":                    numSerie,
                "imagen_frontal":           null,
                "imagen_lateral_derecho":   null,
                "imagen_lateral_izquierdo": null,
                "imagen_posterior":         null,
                "etd_retiro":               "RE",
            }
            console.log(dataFinal);
            console.log("Sere Guardado ")
            alert("EStoy siendo Guardado" + nombreProd +"-"+numSerie +"-"+ id);
            console.log(otro_retiro);


        }else{
            console.log("No sere guaradado ")
            alert("No tengo datita");
        }

        
    };
    return(
        <View style={{
            backgroundColor:'#fff',width:'95%',
            alignSelf:'center',flexDirection:'row',
           }}>


                    <View style={{width:'100%'}}>
                        <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5,paddingTop:15 }}>
                           # Orden de retiro :
                        </Text>

                        <Text  style={{height:50,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}>
                                 {id}       
                        </Text>
                       


                        <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>
                           Nombre del producto :
                        </Text>
                            <TextInput 
                                style={{height:100,paddingVertical:10,textAlignVertical:'top',
                                alignSelf:'center',
                                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,
                                paddingHorizontal:10}}
                                multiline={true}
                                placeholder={'Ingrese un nombre al producto...'}
                                onChangeText={text => getProducto(text)}
                                value={nombreProd}   
                                            
                            />
                        <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5,paddingTop:15 }}>
                           Serie del producto :
                        </Text>
                        <TextInput 
                                style={{height:50,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                keyboardType={'numeric'} 
                                placeholder={'Ingrese un nombre al producto...'}
                                onChangeText={text => getNumSerie(text)}
                                value={numSerie}           
                        />

                        <TouchableOpacity
                            style={{backgroundColor:'#E52859',borderRadius:5,width:'90%',alignSelf:'center',marginVertical:10}}
                            onPress ={guardar}
                        >
                            <Text  style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingVertical:10}}>Guardar</Text>
                        </TouchableOpacity>
                </View>

        </View>
    )

}
const styles = StyleSheet.create({})
