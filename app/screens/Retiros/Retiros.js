import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Picker} from '@react-native-community/picker';
import { FlatList, ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import * as Location from 'expo-location';
import { Icon ,ButtonGroup,CheckBox} from 'react-native-elements';
import { MaterialCommunityIcons ,Entypo,FontAwesome } from '@expo/vector-icons'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loading from "../../components/Loading";
import {useNavigation} from "@react-navigation/native";



export default function Retiros(props) {
    const {route} = props;
    const {id} = route.params;
    let detaProductoTemp = [];

    const [data, setData] = useState([]); 
    useEffect(() => {
        (async () => {
            const dataGet = await AsyncStorage.getItem('@re_detalle_retiro')
            const datos = JSON.parse(dataGet);
            for (let i = 0; i < datos.length; i++) {
                console.log(datos[i].orden_retiro)
                if(datos[i].orden_retiro ==id ){ 
                    detaProductoTemp.push(datos[i]) 
                 }                 
            }
            setData(detaProductoTemp);
          })();      
       
    

      }, []); 

    return (
        <View>
        <ScrollView>
            <DatosSolicitud 
                route = {route}
            />
            <Encabezado 
                route={route}               
            /> 
            <FlatList
                data={data}
                renderItem={(datos) => 
                        <Text>hola</Text>
                   /*  <CardProductosRetiro 
                        datos={datos}
                    />       */                 
                }
                    keyExtractor={(item, index) => index.toString()}
            />          
        </ScrollView>
        </View>

    )
}

function Encabezado(props){
    const {route} = props;
    const {id} = route.params; 

    return(
        <View style={{paddingTop:10}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    # DETALLE DEL RETIRO
                </Text>
            </View>          
        </View>

    )
}

function CardProductosRetiro(props){
    const navigation = useNavigation();
    const {datos} = props;
    const {nombre,producto,serie,kardex} = datos.item; 
    const cargarFormProducto = () => {
        navigation.navigate("form_producto", {
            nombre
        });
    }; 
    
    return(
        <View style={{paddingTop:0}}>
            <TouchableOpacity
                 onPress = {cargarFormProducto}
            >
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row'
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    <FontAwesome name="tags" size={24} color="black" />
                </View>        
                <Text style={{width:'90%',color:'#000',
                                paddingVertical:5,paddingLeft:10}}>
                                {nombre.toString()}
                 </Text>                                
             </View>  

            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row'
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                   
                </View>        
                <Text style={styles.txt_item_tit}>
                                Código :  
                 </Text>            
                 <Text style={styles.txt_item_det}>
                    {producto.toString()}
                 </Text>                     
             </View>                 
             <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row'
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                      
                </View>        
                <Text style={styles.txt_item_tit}>
                        Serie    :
                 </Text> 
                 <Text style={styles.txt_item_det}>
                        {serie.toString()}
                 </Text>                                
             </View> 
             <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:1,
                    borderColor:'#000',
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                </View>        
                <Text style={styles.txt_item_tit}>
                    Kardex :   
                 </Text>
                <Text style={styles.txt_item_det}>
                    {kardex.toString()}
                 </Text>                  

             </View> 
             </TouchableOpacity>

        </View>
        
    )
}

const styles = StyleSheet.create({
    txt_item_tit:{
        width:'20%',
        color:'#000',
        paddingVertical:1,
        paddingLeft:10,
        fontWeight:'bold'
    },
    txt_item_det:{
        width:'60%',
        color:'#000',
        paddingVertical:1,
        paddingLeft:10,
    }

})



const defautl = "Seleccione una opción";
    const data = {
        cliente : '',
        respon : '',
        agencia : '',
        tipo : '',
        gtipo : '',
        fecha : '',
        f_promesa : '',
        f_crea : '',
        monto : '',
        lat : '',
        lng : '',
        t_mobi : '',
        t_cli : '',
        ets : '',
        obs : '',
    }

    
function FormularioRetiro(props){
    const {route} = props;
/*     const {id,cli_id,age_id,emp_code,cli_dir,saldo,tipo,gtipo,fecha} = route.params;
 */    console.log(cli_id);
    const novedades = [];
    const cobros = [];
    const [isLoading, setIsLoading] = useState(false);
    const navigation = useNavigation();

    useEffect(() => {
        (async () => {
            const jsonValue = await AsyncStorage.getItem('@storage_paleta');
            const datos = JSON.parse(jsonValue); 
            for (let i = 0; i < datos.length; i++) {
                if(datos[i].tipo=='PAG'){
                    novedades.push(datos[i]);
                }
                if(datos[i].tipo=='COB'){
                    cobros.push(datos[i]);
                }
            }
            const jsonNovCob = await AsyncStorage.getItem('@storage_nov_cob');
            const datos_nov_cob = JSON.parse(jsonNovCob);   

        })(); 
      }, []); 

   



    var opc_nov_cob         = ["Seleccione una opción","NOVEDAD","COBRO"];
    var opc_nov             = ["Seleccione una opción","ACUERDO DE PAGO", "CAMBIO DE DOMICILIO", 
                                "CLIENTE FALLECIDO","CLIENTE PRESO","NO ENCONTRADO EN CASA",
                                "PROMESA DE PAGO","SERVICIO TÉCNICO","VISITA DE TRABAJO"];       
    var opc_cob             = ["Seleccione una opción","PARCIAL", "TOTAL"];    
    var deu_gar             = ["Seleccione una opción","CLIENTE", "GARANTE"];

    const opc_novItems = opc_nov.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));
    const opc_nov_cobItems = opc_nov_cob.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const opc_cobItems = opc_cob.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     
    const deu_garItems = deu_gar.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));    
    const [stateOpc_nov, setStateOpc_nov] = useState("Seleccione una opción");
    const [stateOpc_nov_cob, setStateOpc_nov_cob] = useState("Seleccione una opción");
    const [stateOpc_cob, setStateOpc_cob] = useState("Seleccione una opción");  
    const [stateDeu_gar, setStateDeu_gar] = useState("Seleccione una opción");  

    const [date, setDate] = useState('');
    const [monto, setMonto] = useState('');
    const [observacion, setObservacion] = useState('');
    const [stateNov, setNov] = useState(false);
    const [stateCob, setStatCob] = useState(false);
    const [tipoCobro, setTipoCobro] = useState(false);


   /*  data.situacionVida          = stateOpc_nov    
    data.opc_nov_cobes                = stateOpc_nov_cob    
    data.tiempoEnAniosopc_nov_cobes   = stateOpc_cob     */
    const guardar=()=>{
        setNov(false);
        setStatCob(false);
        if(stateOpc_nov_cob!=defautl){           
            if(stateOpc_nov_cob=='NOVEDAD'){ 
                 data.gtipo = 'PAG';
                 setStateOpc_cob(defautl);
                if(stateOpc_nov!=defautl){
                    if(stateOpc_nov!=defautl){
                        data.monto = '';
                        data.t_cli ='';
                        if(observacion!=''){
                            if(stateOpc_nov =='CLIENTE FALLECIDO'|| 
                                stateOpc_nov =='CLIENTE PRESO' 
                                || stateOpc_nov =='CAMBIO DE DOMICILIO'){
                                    data.f_promesa= '';
                            }else{
                                    data.f_promesa = '2020-04-09 12:10:12'
                            }
                                data.obs =observacion
                                setNov(true); 
                        }else{
                            alert("Escriba una observación");
                        }
                    }
                }else{          
                    alert("ELIJA UNA TIPO DE NOVEDAD");
                    setNov(false);
                }
            }else{
                if(stateOpc_nov_cob=='COBRO'){
                    setNov(false);
                    if(stateOpc_cob!=defautl){
                        if(stateDeu_gar!=defautl){ 
                            if(stateOpc_cob=='TOTAL'){
                               setMonto(saldo);
                            }                         
                            if(monto>0 ){
                                data.monto = monto;
                                data.t_cli = stateDeu_gar;
                                setStatCob(true);
                            }else{
                              
                                alert("Debe hacer un pago mayor que cero");
    
                            }
                        }else{
                            setStatCob(false);
                            alert("ELIJA EL TIPO DE CLIENTE");
                        }
                    }else{
                        setStatCob(false);
                        alert("ELIJA UNA TIPO DE COBRO");
                    }
                }
            }
/*             alert(stateNov + "-" +stateCob)
 */
            if(stateNov || stateCob){
                (async () => {
                    setIsLoading(true);
                    let location= await Location.getCurrentPositionAsync({});
                    data.lat = location.coords.latitude;
                    data.lng = location.coords.longitude;

                    data.cliente = cli_id
                    data.agencia = age_id
                    data.respon = emp_code

                    var hora = new Date(); 
                    var year = hora.getFullYear();
                    var month = hora.getMonth() + 1;
                    var date = hora.getDate();
                    var hours = hora.getHours();
                    var min = hora.getMinutes();
                    var sec = hora.getSeconds();   
                    data.f_crea = year +"-"+month+"-"+date+" "+hours+":"+min+":"+sec,                                     
                    data.tipo = tipo;
                    data.gtipo = gtipo;
                    data.fecha = fecha;
                    data.t_mobi = 'MOBILE';
                    data.ets = 'SIN';
                    const listaFormulario = [];
                    let nov_cob = await AsyncStorage.getItem('@storage_novedad_cobranza');             
                    if(nov_cob!=null){
                          let nuevoform = JSON.parse(nov_cob);
                          for(let i = 0; i < nuevoform.length; i++){
                              listaFormulario.push(nuevoform[i]);
                          }
                          listaFormulario.push(data);
                          await AsyncStorage.setItem('@storage_novedad_cobranza',JSON.stringify(listaFormulario)); 
                          let pruebita = await AsyncStorage.getItem('@storage_novedad_cobranza');             
                    }else{
                          listaFormulario.push(data);
                          await AsyncStorage.setItem('@storage_novedad_cobranza',JSON.stringify( listaFormulario));
                      }     
                      setIsLoading(false);
                      navigation.navigate("form-cobranza");
                      
                })();


            }else{
                alert("No se puede guardar , llene los campos");

            }
           
        } else{

            alert("ELIJA UNA OPCION");
        }

        
    } 
    return (



        
        <View style={{paddingTop:20}}>
            <Loading isVisible={isLoading} text="Guardando Novedad" />

        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>
                
           <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS DEL FORMULARIO
           </Text>
        </View>

        
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>

                    <Text style={{ fontWeight:'bold', }}> Tipo de solicitud:</Text>  
                    <Picker
                                selectedValue={stateOpc_nov_cob}
                                style={styles.inputAndroid}
                                onValueChange={(itemValue, itemIndex) =>
                                setStateOpc_nov_cob(itemValue)
                            } >
                            {opc_nov_cobItems}
                    </Picker> 
                    {stateOpc_nov_cob == 'NOVEDAD' ?
                        <View>
                                
                                <Text style={{ fontWeight:'bold', }}> Tipo novedades:</Text>                  
                                <Picker
                                    selectedValue={stateOpc_nov}
                                    style={styles.inputAndroid}
                                    onValueChange={(itemValue, itemIndex) =>
                                    setStateOpc_nov(itemValue)
                                    } >
                                        {opc_novItems}
                                </Picker>
                            
                            {stateOpc_nov =='CLIENTE FALLECIDO'|| stateOpc_nov =='CLIENTE PRESO' 
                            || stateOpc_nov =='CAMBIO DE DOMICILIO'
                                    || stateOpc_nov =='Seleccione una opción'? 
                            null :
                            <View>
                                <Text style={{ fontWeight:'bold', }}> 
                                Fecha próxima visita</Text>                    
                            </View>
                                          
                            } 
                            <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>Observación:</Text>
                                <TextInput 
                                    style={{height:100,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                    width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                    multiline={true}
                                    placeholder={'Escriba una observación'}
                                    onChangeText={text => setObservacion(text)}
                                    value={observacion}                  
                            />
                        </View>                       
                    :null
                    } 


                     {stateOpc_nov_cob == 'COBRO' ? 
                     
                     <View>
                        <Text style={{ fontWeight:'bold', }}> Tipo de pago:</Text>       
                        <Picker
                            selectedValue={stateOpc_cob}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateOpc_cob(itemValue)
                        } >
                        {opc_cobItems}
                        </Picker> 

                        <Text style={{ fontWeight:'bold', }}> Cliente:</Text>       
                        <Picker
                            selectedValue={stateDeu_gar}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateDeu_gar(itemValue)
                        } >
                        {deu_garItems}
                        </Picker>  
                        
                       
                        <View>
                            
                            <Text style={{ fontWeight:'bold', }}> Monto:</Text>
                                {stateOpc_cob == 'TOTAL' ?           
                                    <View style={{paddingVertical:13,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}>
                                        <Text>{saldo}</Text>                                   
                                    </View>                                                                                 
                                : 
                                    <TextInput style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                        width:'95%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                        keyboardType="numeric" 
                                        onChangeText={text => setMonto(text)}    
                                        value={monto}   
                                    />
                                }
                        </View>
                     </View>
                        :                     
                     null}

                        <TouchableOpacity
                            style={{backgroundColor:'#E52859',borderRadius:5,width:'90%',alignSelf:'center',marginVertical:10}}
                            onPress ={guardar}
                        >
                            <Text  style={{color:'#fff',fontWeight:'bold',textAlign:'center',paddingVertical:10}}>Guardar</Text>
                        </TouchableOpacity>
              

        </View>
        </View>
                     
        </View>

    )
}


function DatosSolicitud(props) {
     const {route} = props;
     const {id,nombre_cliente,direccion,cli_dir,saldo,tipo,gtipo,fecha} = route.params;
      
     /*
    const {code,deuGar,garante,dnombre,dcedula,dtelefono,ddireccion,dreferencia} = route.params; */


    return(
        <View style={{paddingTop:10}}>
             <View style={{
                 backgroundColor:'#0061a7',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    DATOS DEL CLIENTE
                </Text>
             </View>                               
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row'
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    <Icon name='user' type='font-awesome' />
                </View>        
                <Text style={{width:'90%',color:'#000',
                                paddingVertical:5,paddingLeft:10}}>
                                    {nombre_cliente.toString()}
                 </Text>                
             </View>             
             <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                  borderTopWidth:1,
                    borderColor:'#B3BAC4',paddingTop:10
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10,}}>
                      <Text style={{ fontWeight:'bold', }}>Dirección:</Text>                  
                      <Text>
                             {/* {direccion.toString()}    */}                  
                      </Text> 
                </View>                                     
             </View>                
    </View>
    )
}

