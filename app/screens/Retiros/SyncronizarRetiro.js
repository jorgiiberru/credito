import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'; 
import ProgressCircle from 'react-native-progress-circle';
import { Button } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {URL_FORM_NOV_COB} from '../../api/Storage'

export default function SyncronizarRetiro() {
    const [porcentaje, setPorcentaje] = useState(0); 
    const [totalSolicitud, setTotalSolicitud] = useState(0); 
     useEffect(() => {
         (async () => {
            const solicitudesGuardadas = await AsyncStorage.getItem('@storage_novedad_cobranza');

/*               const solicitudesGuardadas = await AsyncStorage.getItem('@gh_solicitudes');
 */
            if(solicitudesGuardadas!=null){
                 let solicitudes = JSON.parse(solicitudesGuardadas);
                 let total = 0;
                 for(let i = 0; i < solicitudes.length;i++){
                   if(solicitudes[i].ets=='SIN'){
                     total = total+1;
                   }
                 }
                setTotalSolicitud(total); 
                 const store_terminadas = await AsyncStorage.getItem('@solicitud_finalizadas');
                 console.log(solicitudesGuardadas)
               }else{
                
             } 

         })();
       }, []); 

      const sincronizar = async() =>{
        try {


           

                 if(totalSolicitud!=0){
                  const store_deudor = await AsyncStorage.getItem('@gh_solicitudes');
                    const store_formulario = await AsyncStorage.getItem('@storage_novedad_cobranza');
                    const store_terminadas = await AsyncStorage.getItem('@solicitud_finalizadas');
                    let listaTerminadas = []
                    let temp_deudor = JSON.parse(store_deudor);
                    let temp_form = JSON.parse(store_formulario);
                    let temp_terminadas = JSON.parse(store_terminadas);
                    console.log(temp_form);
                    if(porcentaje!=100){
                        if(store_formulario!=null){
                            console.log("formulario" + " -" +store_formulario)
                            for(let i = 0; i < temp_form.length; i++){
                                fetch(URL_FORM_NOV_COB, {
                                    method: 'POST',
                                    headers: {
                                    Accept: 'application/json',
                                            'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify({
                                        cliente     :temp_form[i].cliente,
                                        respon      :temp_form[i].respon,
                                        agencia     :temp_form[i].agencia,
                                        tipo        :11,
                                        gtipo       :'PAG',
                                        fecha       :'2021-02-10',
                                        f_promesa   :temp_form[i].f_promesa,
                                        f_crea      :temp_form[i].f_crea,
                                        monto       :temp_form[i].monto,
                                        lat         :temp_form[i].lat,
                                        lng         :temp_form[i].lng,
                                        t_mobi      :'MOBILE',
                                        t_cli       :temp_form[i].t_cli,
                                        ets         :'SN',
                                        obs         :temp_form[i].obs 
                                    })
                                }); 
                        } 
                           /*  for(let i = 0; i < temp_deudor.length; i++){
                                if(temp_deudor[i].destado==='SIN'){
                                    fetch((URL_SOLICITUDES_SINC+temp_deudor[i].id),{
                                    method: 'POST',
                                    headers: {
                                        Accept: 'application/json',
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify({
                                        destado: 'FI',
                                        gestado: 'FI',
                                        ddireccion_cor: temp_deudor[i].ddireccion_cor,
                                        dobservacion: temp_deudor[i].dobservacion,
                                        gdireccion_cor:" temp_deudor[i].gdireccion_cor",
                                        gobservacion: "temp_deudor[i].gobservacion",                                    
                                    })
                                    })               
                                    temp_deudor[i].destado='FI';
                                    listaTerminadas.push(temp_deudor[i]);
                                }
                            }

                            */ 
                           
                          /*   if(temp_terminadas != null){
                                for (let i = 0; i < listaTerminadas.length; i++) {
                                    temp_terminadas.unshift(listaTerminadas[i]);
                                }
                                await AsyncStorage.setItem('@solicitud_finalizadas', JSON.stringify(temp_terminadas));

                                const solic = await AsyncStorage.getItem('@solicitud_finalizadas');                                         
                                console.log("Lista Vacia")
                                console.log(solic) 
                            }else{
                                console.log("Guardas finalizadas")
                                await AsyncStorage.setItem('@solicitud_finalizadas', JSON.stringify(listaTerminadas));
                                const solic = await AsyncStorage.getItem('@solicitud_finalizadas'); 
                                console.log("Terminada")
                                console.log(solic);
                            }


                          
                            */ 
                            /* await AsyncStorage.setItem('@gh_solicitudes', JSON.stringify(temp_deudor)         )
                            const st = await AsyncStorage.getItem('@gh_solicitudes');
                            let st1 = JSON.parse(st); */
                    }else{
                        alert("No hay datos por Sincronizar");
                    }
                        setPorcentaje(prevCount => prevCount + 100);
                        setTotalSolicitud(0);
                }
            }else{
                alert("No hay solicitudes pendietes por Sincronizar");
            }       

          


        } catch(e) {
          console.log(e);
          // error reading value
        }
    }
    

    return (
        <View style={{backgroundColor: "#fff"}}>
            <View  style= {styles.body}>
            <View style={{marginTop:10,borderColor:'#D7DFE6',borderWidth:0.4}}>
                <Text style={{marginVertical:10,marginStart:10,fontWeight:'bold'}}>
                    Pendientes por Sincronizar : {totalSolicitud}
                </Text>
            </View>
            <View 
                style={{alignItems:'center',marginVertical:20,borderWidth:0.4,borderColor:'#D7DFE6',paddingVertical:15}}>
                <ProgressCircle
                    percent={porcentaje}
                    radius={100}
                    borderWidth={30}
                    color="#3399FF"
                    shadowColor="#D7DFE6"
                    bgColor="#fff"
                >
                    <Text style={{ fontSize: 18 }}>{porcentaje} %</Text>
                </ProgressCircle>
            </View>
             <Button
                    icon={
                        <FontAwesome name="cloud-upload" size={24} color="#fff" />
                    }
                    title="  Sincronizar"
                    onPress={sincronizar}
                />
        </View>
        </View>    
    )
}



  

const styles = StyleSheet.create({
    body: {
        height: "auto",
        width: "90%",
        marginHorizontal:10,
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center',
        backgroundColor: "#fff"
    },
    btnWithIcon:{
        display:'flex',
        flexDirection:'row',
        borderRadius:5
    }
});