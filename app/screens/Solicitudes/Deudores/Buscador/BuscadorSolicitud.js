import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { Encabezado, EncabezadoDoble } from '../../../../components/ComponentesComunes'
import { FlatList, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesome, MaterialCommunityIcons, SimpleLineIcons } from '@expo/vector-icons';
import { color_azul, color_blanco, color_confirm, color_desa } from '../../../../styles/Colores';
import {useNavigation} from "@react-navigation/native";
import SolicitudesApi from '../../../../api/SolicitudesApi';
import { card_style, stylesList } from '../../../../styles/Estilos';
import { Picker } from '@react-native-community/picker';
import Loading from '../../../../components/Loading';

export default function BuscadorSolicitud() {
    const [buscar, setBuscar]       = useState('');
    const [store, setStore]         = useState('');
    const [tipoInput, setTipoInput] = useState();
    const [isLoading, setIsLoading] = useState(false);

    
    var opcionesBuscar              = ["Seleccione una opción","CEDULA", "APELLIDOS Y NOMBRE"];
    const valorDefecto = "Seleccione una opción";
    const [stateTipoBusqueda, setStateTipoBusqueda] = useState(valorDefecto);  

    const opcBuscItems = opcionesBuscar.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));

    const setTipoBusqueda = (text) => {
        setStore(null);
        setBuscar('');
        setStateTipoBusqueda(text);
    };

 

    const buscando = async () => {        
               
        if(stateTipoBusqueda!=valorDefecto){
            if(buscar!=''){
                setIsLoading(true); 
                let data = null;
               if(stateTipoBusqueda=='CEDULA'){
                    setTipoInput('numeric')
                    data = await SolicitudesApi.busPorCliente(buscar);
               }
               if(stateTipoBusqueda=='APELLIDOS Y NOMBRE'){
                    setTipoInput('')
                      data = await SolicitudesApi.busPorApeNomCliente(buscar); 
                      console.log(data);
               }
               if(data!='[]'){
                    setStore(JSON.parse(data));  
               }else{                    
                alert("¡No tiene solicitudes de créditos!");
               }            
            }else{
                alert("¡Ingrese un dato para la búsqueda!");
            }
        }else{
            setStore(null);             
        }
        setIsLoading(false);         

    };
   
    let tam = 70;

    return (
        <View>
            <Loading isVisible={isLoading} text="Cargando Solicitudes" />

            <Encabezado 
                txt_info ="Buscar cliente"             
            /> 
            <View  style={card_style.card_body_search_cmb}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                <Text style={{ fontWeight:'bold'}}> Buscar por:</Text>       
                <Picker         
                                selectedValue={stateTipoBusqueda}
                                onValueChange={(itemValue, itemIndex) =>
                                    {setTipoBusqueda(itemValue)}
                            } >
                            {opcBuscItems}
                </Picker> 
                </View>                                      
            </View>
            {stateTipoBusqueda!=valorDefecto && 
                <View style={card_style.card_body_search}> 
                    <View style={{width:'90%'}}>                    
                        {stateTipoBusqueda=='CEDULA' ? 
                        <TextInput 
                            style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                            width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10,
                                }}
                                keyboardType={'numeric'} 
                                onChangeText={text => setBuscar(text)}    
                                value={buscar}   
                                placeholder={'Ingrese el número de cedula del cliente'}
                        /> 
                        :
                        <TextInput 
                            style={{paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                            width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10,
                                }}
                                onChangeText={text => setBuscar(text)}    
                                value={buscar}   
                                placeholder={'Ingrese el nombre del cliente'}
                        /> 
                        }

                    </View>
                    <View style={{width:'10%',alignItems:'flex-start',justifyContent:'center'}}>
                        <TouchableOpacity
                            onPress={buscando}
                            >
                            <FontAwesome name = "search" size = {25} color = {color_desa} />  
                        </TouchableOpacity>                       
                    </View>                  
                </View>             
            }
            <View>
                <ScrollView>
                   
                        <ListaSolicitud
                                    solicitudes={store}
                        />
                    
                </ScrollView>
            </View>
        </View>
    )
}

    function ListaSolicitud (props){
        const {solicitudes} = props;

        return(
        <FlatList
            style={{flex:0}}
            data={solicitudes}
             initialNumToRender={6}
/*              initialScrollIndex={6}
 */            renderItem={(solicitud) => 
            <GetSolicitudes 
                    solicitud={solicitud}
            />

            }
            keyExtractor={(item, index) => index.toString()}
        />   
    )
    }

    function GetSolicitudes(props) {
        const {solicitud} = props;
        const {id,cod_verif,dnombre,ddireccion,agencia,dcedula,code,fecha,gcedula,gdireccion,gnombre} = solicitud.item;    
        const navigation = useNavigation();
        const visualizar =  () => { 

            navigation.navigate("list-doc-cliente",{code,dcedula,agencia,dnombre,
                ddireccion,id,fecha,cod_verif,
                gcedula,gdireccion,gnombre});      
        }; 

        return (
            <View>               
                <EncabezadoDoble 
                    txt_info    =   {dcedula} 
                    txt_info2   =   {agencia} 
                />
                <View style={stylesList.view_description_two}>                     
                    <Text style={stylesList.txt_nombre} >
                        {dnombre}
                    </Text>               
                    <View style ={{width:'100%',flexDirection:'row'}}>
                        <Text style={{ fontSize: 10,width:'80%' }}>
                            {ddireccion}
                        </Text>
                        <View style={{width:'20%'}}>
                            <TouchableOpacity   onPress={()=>visualizar()} style={{alignItems:'center'}}>
                                <MaterialCommunityIcons                                
                                    name = "account-details" 
                                    size = {20} color = "black" />   
                                <Text  style={{ fontSize: 10 }}>Detalle</Text>                           
                            </TouchableOpacity>                             
                        </View>
                        <View style={{width:'20%',alignItems:'center' }}>
                            <TouchableOpacity   onPress={()=>visualizar()} style={{backgroundColor:color_confirm}}>
                                <MaterialCommunityIcons                                
                                name = "account-details" 
                                    size = {20} color = "black" />                                 
                            </TouchableOpacity> 
                            <Text  style={{ fontSize: 10 }}>Detalle</Text>
                        </View>                                 
                    </View>                                                 
                </View>                            
            </View>                      
        )
    }