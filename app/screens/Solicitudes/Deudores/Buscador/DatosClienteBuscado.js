import { Ionicons, MaterialCommunityIcons, MaterialIcons,Foundation, FontAwesome,FontAwesome5
 } from '@expo/vector-icons';
import React, { useState } from 'react'
import { Alert,StyleSheet, Text, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {useNavigation} from "@react-navigation/native";
import { color_blanco, color_fondo } from '../../../../styles/Colores';
import { EncabezadoDoble } from '../../../../components/ComponentesComunes';
import { stylesList } from '../../../../styles/Estilos';
import FormularioApi from '../../../../api/FormularioApi';

export default function DatosClienteBuscado(props) {
    const {route} = props;
    const {code,id} = route.params;
    return (
        <ScrollView style={{backgroundColor:color_fondo}}>
            <DatosClientes 
                route={route}
            />
               
                <VerDocumentos 
                    code={code}
                    solicitud_id={id}
                    route ={route}
                />



        </ScrollView>
    )
}

function DatosClientes(props){
    const {route} = props;
    const {id,fecha,code} = route.params;
    let codigo = '# '+code;
    return (
        <View>
            <EncabezadoDoble 
                    txt_info    =  {codigo} 
                    txt_info2   =  {fecha} 
            /> 
            <DetalleCliente 
                    route     =   {route} 
            /> 
        </View>
    );
}


function DetalleCliente(props){
    const {route} = props;
    const {code,dnombre,ddireccion,id,dcedula} = route.params;

    return(
        <View style={stylesList.view_description_two}> 
            <View style ={{width:'100%',flexDirection:'row'}}>
                <Text style={{ fontSize: 15,width:'80%',fontWeight:'bold' }}>
                    {dcedula}
                </Text>                                           
            </View>                                   
            <Text style={stylesList.txt_nombre} >
                {dnombre}
            </Text>                       
            <View style ={{width:'100%',flexDirection:'row'}}>
                <Text style={{ fontSize: 10,width:'80%' }}>
                    {ddireccion}
                </Text>                                           
            </View>
                                                                  
        </View>      
    )
}


function VerDocumentos(props){
    const {code,solicitud_id,route} = props;
    const {gcedula,gdireccion,gnombre,fecha} = route.params;
    const navigation = useNavigation();
    const visualizar =  (tipoDocumento) => {  
        navigation.navigate("visualizar",{tipoDocumento,code});  
 
    }; 
    const visualizarFoto =  (tipoDocumento) => {  
        navigation.navigate("visualizar_foto",{tipoDocumento,code,solicitud_id});  
 
    }; 

    const verDatosGarante =  (tipoDocumento) => {  
        navigation.navigate("list-doc-garante",{tipoDocumento,gcedula,gdireccion,gnombre,fecha,code,solicitud_id});  
 
    }; 
    
    const verUbicacion =  async() => {
        let cli_lat =null;
        let cli_log =null;
        let dataFinal = null;

        dataFinal = await FormularioApi.buscarUbicacionClient(solicitud_id,'d');
        if(dataFinal!=''){
                cli_lat =  Number (dataFinal[0].latitud);
                cli_log =  Number (dataFinal[0].longitud);
                if(dataFinal[0].latitud!='' && dataFinal[0].longitud!=''){
                    navigation.navigate("vista_ubicación" , {
                        cli_lat,cli_log
                    });
                }
            }else{
                Alert.alert("Sin resultados","El cliente no tiene una ubicación GPS");
            } 
    };  

    let tam = 70;
    return(
        <View style={{backgroundColor:color_fondo,height:'100%'}}>
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(1)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialCommunityIcons name="card-account-details" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Cédula</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(2)}

                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="exchange" size={30} color='#2E4E7A' />
                            </View> 
                                   
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Dación</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(3)}            
                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialCommunityIcons name="folder-information" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Información</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>        
            </View>
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(4)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialIcons name="library-books" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Págare</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(5)}

                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialCommunityIcons name="account-cash" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Reserva</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(6)}            
                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <Ionicons name="document" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Solicitud</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>        
            </View>        
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(11)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="calculator" size={30} color="#2E4E7A" />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Buró D.</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(12)}

                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialIcons name="policy" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>F. Judicial</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(12)}            
                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialIcons name="assignment-return" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Sol. Manual</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>        
            </View>        
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(30)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="map" size={28} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Croquis</Text>            
                        </TouchableOpacity>                
                    </View>
                </View> 

                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(10)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <Foundation name="book" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>GBook</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={verUbicacion}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="map-marker" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Ubicación</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>               
                    
            </View>        
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
            {gcedula   &&        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={verDatosGarante}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="users" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Garante</Text>            
                        </TouchableOpacity>                
                    </View>
                </View> 
            }  
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizarFoto('d')}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="image" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Foto</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
              
                    
            </View>                  
        </View>

    )
}
const styles = StyleSheet.create({})
