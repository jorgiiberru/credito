import { Ionicons, MaterialCommunityIcons, MaterialIcons,Foundation, FontAwesome,FontAwesome5
} from '@expo/vector-icons';
import React, { useState } from 'react'
import { Alert,StyleSheet, Text, View } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {useNavigation} from "@react-navigation/native";
import { color_blanco, color_fondo } from '../../../../styles/Colores';
import { EncabezadoDoble } from '../../../../components/ComponentesComunes';
import { stylesList } from '../../../../styles/Estilos';
import FormularioApi from '../../../../api/FormularioApi';

export default function DatosGaranteBuscado(props) {
    const {route} = props;
    const {code,solicitud_id} = route.params;
    return (
        <ScrollView style={{backgroundColor:color_fondo}}>
                <DatosClientes 
                    route={route}
                />
                <VerDocumentos 
                    code={code}
                    solicitud_id={solicitud_id}
                />            
        </ScrollView>
    )
}

function DatosClientes(props){
    const {route} = props;
    const {code,fecha} = route.params;
    let codigo = '# '+code;

    return (
        <View>
            <EncabezadoDoble 
                    txt_info    =  {codigo} 
                    txt_info2   =  {fecha} 
            /> 
            <DetalleGarante 
                    route     =   {route} 
            /> 
        </View>
    );
}

function DetalleGarante(props){
    const {route} = props;
    const {code,gnombre,gcedula,gdireccion} = route.params;

    return(
        <View style={stylesList.view_description_two}> 
            <View style ={{width:'100%',flexDirection:'row'}}>
                <Text style={{ fontSize: 15,width:'80%',fontWeight:'bold' }}>
                    {gcedula}
                </Text>                                           
            </View>                                   
            <Text style={stylesList.txt_nombre} >
                {gnombre}
            </Text>                       
            <View style ={{width:'100%',flexDirection:'row'}}>
                <Text style={{ fontSize: 10,width:'80%' }}>
                    {gdireccion}
                </Text>                                           
            </View>
                                                                  
        </View>      
    )
}
function VerDocumentos(props){
    const {code,solicitud_id} = props;
    const navigation = useNavigation();
    const visualizar =  (tipoDocumento) => {  
        navigation.navigate("visualizar",{tipoDocumento,code});  
 
    }; 
    const visualizarFoto =  (tipoDocumento) => {  
        navigation.navigate("visualizar_foto",{tipoDocumento,code});  
 
    }; 
    
    const verUbicacion =  async() => {
        let cli_lat =null;
        let cli_log =null;
        let dataFinal = null;
        dataFinal = await FormularioApi.buscarUbicacionClient(solicitud_id,'g');
        if(dataFinal!=''){
                cli_lat =  Number (dataFinal[0].latitud);
                cli_log =  Number (dataFinal[0].longitud);
                if(dataFinal[0].latitud!='' && dataFinal[0].longitud!=''){
                    navigation.navigate("vista_ubicación" , {
                        cli_lat,cli_log
                    });
                }
            }else{
                Alert.alert("Sin resultados","El cliente no tiene una ubicación GPS");
            } 
    };  

    let tam = 70;
    return(
        <View style={{backgroundColor:color_fondo,height:'100%'}}>
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(8)}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <MaterialCommunityIcons name="card-account-details" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Cédula</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(9)}

                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="exchange" size={30} color='#2E4E7A' />
                            </View> 
                                   
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Dación</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizar(10)}            
                        >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="calculator" size={30} color="#2E4E7A" />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Información</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>        
            </View>
            <View style={{width:'100%',flexDirection:'row',backgroundColor:color_fondo,
                            justifyContent:'center'}}>        

                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={verUbicacion}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="map-marker" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Ubicación</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>      
                <View style={{width:'32%',alignItems:'center',paddingVertical:15}}>
                    <View>
                        <TouchableOpacity 
                            onPress={()=>visualizarFoto('g')}
                            >                    
                            <View style={{width:tam,backgroundColor:color_blanco,borderRadius:5,
                                    height:tam,justifyContent:'center',alignItems:'center'}}>
                                <FontAwesome name="image" size={30} color='#2E4E7A' />
                            </View>        
                            <Text style={{paddingVertical:10,textAlign:'center'}}>Foto</Text>            
                        </TouchableOpacity>                
                    </View>
                </View>
              
                    
            </View>                  
        </View>

    )
}