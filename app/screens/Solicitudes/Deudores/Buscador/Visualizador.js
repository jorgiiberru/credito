import React from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'
import PDFReader from 'rn-pdf-reader-js'

export default function Visualizador(props) {
    const {navigation, route} = props;
    const {code,tipoDocumento} = route.params;
    return (
        <PDFReader 
            source={{ uri: "https://ghc.gramsa.net/pub/file/"+code+"/"+tipoDocumento}} 
        />
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
   
});