import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'
import FormularioApi from '../../../../api/FormularioApi';

export default function VisualizadorFoto(props) {
    const {route} = props;
    const {code,tipoDocumento,solicitud_id} = route.params;
    let ruta = 'https://ghc.gramsa.net/pub/phone/'+code+'/'+tipoDocumento;
    const [rutaFoto, setRutaFoto] = useState();
    const [isBase64, setIsBase64] = useState(false);
    const [rutaImg, setRutImg] = useState(false);


    useEffect(() => {
        (async () => {
            var dataFinal = await FormularioApi.buscarUbicacionFoto(solicitud_id,tipoDocumento);
            var rutaImg = JSON.stringify(dataFinal[0].foto_base)
            console.log(rutaImg)
            if(rutaImg!='""'){
                var imagen_source = 'data:image/png;base64,'+rutaImg;
                setRutaFoto(imagen_source);
            }else{
                var imagen_source = ruta;
                setRutaFoto(imagen_source);
            }
         })();
      }, []);

    return (
        <View style={{flexDirection:'row',backgroundColor:'#000',flex:1}}>
            <Image
                    style={{width:'100%', height: 600}}
                    source={{
                        uri: rutaFoto,
                      }}
                    resizeMode="contain"      
                /> 
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
   
});