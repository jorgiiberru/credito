import React from 'react'
import { Text, View , TouchableOpacity } from 'react-native'
import { Icon} from 'react-native-elements';
import  call  from 'react-native-phone-call'
import {styles}  from '../../../styles/Estilos';

export default function DatosDeudor(props) {
    const {navigation, route} = props;
    return (
        <View >                
            <DatosSolicitud 
                route      = {route}
                navigation = {navigation}
            />      
        </View>
    )
}

function DatosSolicitud(props) {
    const {navigation, route} = props;
    const { id,code,cod_verif,deuGar,garante,dnombre,dcedula,dtelefono,ddireccion,dreferencia,destado,
            estadoColorDeudor,componDesacDeudor,buttonFormDeudor} = route.params;

    const makeCall = (number) => {
        const args = {
            number: number, 
            prompt: true 
        }
       call(args).catch(console.error)
   };
   const goFormulario = () => {
        navigation.navigate("formulario", {
            id,code,deuGar,garante,dnombre,dcedula,dtelefono,ddireccion,dreferencia,cod_verif,destado
        });         

    };
     
    return(
        <View style={{paddingTop:20}}>
             <View style={{ backgroundColor: estadoColorDeudor,
                            width:'95%',alignSelf:'center',flexDirection:'row',
                            borderTopStartRadius:5, borderTopEndRadius:5}}>
                <Text style={styles.encabezadoTxt}>
                    # {code}
                </Text>
             </View>
             <View style={styles.view_tipo_cli}> 
                <Text style={styles.txt_tipo_cli} >
                            {deuGar} 
                </Text>
            </View>             
            <View style={styles.view_datos}>
                <View style={styles.txt_icon}>
                    <Icon name='user' type='font-awesome' />
                </View>        
                <Text style={styles.txt_atrib}>
                            {dnombre}  
                 </Text>                
             </View>
             <View style={styles.view_datos}>
                <View style={styles.txt_icon}>
                    <Icon name='id-card' type='font-awesome' />
                </View>        
                <Text style={styles.txt_atrib}>
                            {dcedula} 
                </Text>                
             </View>
             <View style={styles.txt_detalle}>
                <View style={styles.txt_det_deu}>
                    <Text style={styles.txt_det}>
                        Dirección:</Text>                  
                    <Text>
                            {ddireccion.toString()}                       
                    </Text>
                    <Text style={styles.txt_det}>
                        Referencia:
                    </Text>                  
                    <Text>
                            {dreferencia.toString()}                       
                    </Text>
                </View>   
             </View>
             <View style={styles.view_datos_call}>
                    <TouchableOpacity

                        style={styles.btn_call} 
                        onPress={()=> makeCall(dtelefono.toString())}>
                        <View >
                            <Icon name='phone' type='font-awesome' color='#fff'/>
                            <Text style={{textAlign:'center',color:'#fff'}}>
                                Llamar
                            </Text>
                        </View> 
                    </TouchableOpacity>
                <Text style={styles.txt_telf}>
                    {dtelefono.toString()} 
                </Text>    
             </View>
             <View style={styles.view_btn_form}>
                    <TouchableOpacity disabled={componDesacDeudor}
                                style={{backgroundColor: buttonFormDeudor,
                                        width:'95%',height:70,borderRadius:10,alignContent:'center',
                                        justifyContent:'center'}}
                                onPress={goFormulario} > 
                            <Icon name='edit' type='font-awesome' color='#ffff'/>  
                            <Text style={styles.txt_llenar_form}>
                                    LLENAR FORMULARIO
                            </Text>                   
                    </TouchableOpacity>  
             </View>
        </View>
    )
}

