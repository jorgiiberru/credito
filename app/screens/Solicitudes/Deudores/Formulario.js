import React, { useState, useEffect,useRef}  from 'react'
import { StyleSheet, Text, View , ScrollView,Switch,TouchableOpacity, 
        ToastAndroid,Linking, Alert ,SafeAreaView,Modal,Image,FlatList,TextInput,Button } from 'react-native'
import { Icon ,ButtonGroup,CheckBox} from 'react-native-elements';
import {Picker} from '@react-native-community/picker';
import * as Location from 'expo-location';
import { Camera } from 'expo-camera';
import {FontAwesome} from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Base64 from 'base-64';
import Loading from "../../../components/Loading";
import { Encabezado } from '../../../components/ComponentesComunes';

global.atob = Base64.encode; 
const data = {
    photo : '',
    searchingPosition : false,
    error : null,
    enableHighAccuracy : false,
    value : 0,
    zona : '',
    estadoVivienda : '',
    tipoVivienda : '',
    vivienda : '',
    tiempoEnAniosVivienda : '',
    situacionVida : '',
    labores : '',
    tiempoEnAniosLabores : '',
    responsable_honesto : false,
    mantiene_otras_deudas : false,
    referencias:{ vecinos : [], familiares : [], amigos : [], jefes : []},
    latitude : '',
    longitude : '',
    problematico : false,
    gpsData: {mocked: false , coords:'', timestamp:''},
    capturedAt :'',
    solicitudCode : 0,
    estadoCivil: {
        esCasado: false,
        laboresConyug: '',
        tiempoEnAniosLaboresCoyug:''
    }
}

const referencias ={
    vecinos:'',
    familiares:'',
    amigos:'',
    jefes:''
}
const pathDefault = 'https://reactjs.org/logo-og.png';
const foto ={
    url:        pathDefault,
    urlBase:    ''
}
const datosObservacion =[
    observacion = '',
    direccion   ='',
    referencia  =''
] 
const options = { quality: 0.5, base64: true };
const comparar = "Seleccione una opción";
export default function Formulario(props) {
    const [isLoading, setIsLoading] = useState(false);
    const {navigation, route} = props;
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [dataSol, setDataSol] = useState(null);
    const [datos, setDato] = useState('');
   useEffect(() => {
        (async () => {
          const jsonValue = await AsyncStorage.getItem('@gh_solicitudes')
           setDataSol(JSON.parse(jsonValue));
            const prueba = jsonValue;
            setDato(JSON.parse(prueba));
            estadoDirrecionCorrecta =false;
            estadoReferenciaCorrecta =false;
            datosObservacion.observacion= ''
            datosObservacion.direccion= ''
            datosObservacion.referencia= ''
         })();
      }, []); 

    useEffect(() => {
        (async () => {
          let { status } = await Location.requestPermissionsAsync();
          if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
          }
        })();
      }, []); 
     
    const guardar = () => {
        
        data.referencias.vecinos =[];
        data.referencias.familiares =[];
        data.referencias.amigos =[];
        data.referencias.jefes =[];

        for (let index = 0; index <  referencias.vecinos.length; index++) {
                data.referencias.vecinos.push(referencias.vecinos[index].valor);
        }
        for (let index = 0; index <  referencias.familiares.length; index++) {
            data.referencias.familiares.push(referencias.familiares[index].valor);
        }
        for (let index = 0; index <  referencias.amigos.length; index++) {
            data.referencias.amigos.push(referencias.amigos[index].valor);
        }       
        for (let index = 0; index <  referencias.jefes.length; index++) {
            data.referencias.jefes.push(referencias.jefes[index].valor);
        }
        if(data.zona!=comparar          
            && data.estadoVivienda!=comparar && data.tipoVivienda!=comparar
            && data.vivienda!=comparar&& data.tiempoEnAniosVivienda!=comparar
            && data.situacionVida!=comparar&& data.labores!=comparar
            && data.tiempoEnAniosLabores!=comparar  && foto.url!=pathDefault  
            ){
            (async () => {                                
                const validarDir = validarChecked(0);
                const validarRef = validarChecked(1);
                if(validarDir && validarRef){ 
                    if(validarEstadoCivil()){
                        console.log(validarEstadoCivil)
                        if(data.estadoCivil.laboresConyug==comparar ||data.estadoCivil.laboresConyug==comparar ){
                            data.estadoCivil.laboresConyug = null;
                            data.estadoCivil.tiempoEnAniosLaboresCoyug = null; 
                        }
                       console.log(data);
                       setIsLoading(true);                    
                       let location= await Location.getCurrentPositionAsync({});
                       setLocation(location);
                       data.solicitudCode = route.params.id;  
                        data.latitude = location.coords.latitude;
                        data.longitude = location.coords.longitude;
                        data.gpsData.coords = location.coords;
                        var hora = new Date(); 
                        var year = hora.getFullYear();
                        var month = hora.getMonth() + 1;
                        var date = hora.getDate();
                        var hours = hora.getHours();
                        var min = hora.getMinutes();
                        var sec = hora.getSeconds(); 
        
                    await AsyncStorage.setItem('@gh_solicitudes',JSON.stringify(datos)); 
                        let list_update = await AsyncStorage.getItem('@gh_solicitudes');             
                        let list_update_temp = JSON.parse(list_update);
                        for (let index = 0; index < list_update_temp.length; index++) {                
                            if(route.params.code==list_update_temp[index].code){                        
                                list_update_temp[index].dobservacion    =   datosObservacion.observacion;
                                list_update_temp[index].ddireccion_cor  =   datosObservacion.direccion;
                                list_update_temp[index].dreferencia_cor =   datosObservacion.referencia;
                                list_update_temp[index].destado='SIN'
                            }         
                        }
                    
                        await AsyncStorage.setItem('@gh_solicitudes',JSON.stringify(list_update_temp));                  
        
                        const listaFormulario = [];
                        const formulario = {
                            usuario_id : route.params.cod_verif,
                            solicitud_id: data.solicitudCode,
                            data :data,
                            fecha_verificacion : year +"-"+month+"-"+date+" "+hours+":"+min+":"+sec,
                            latitud : data.latitude,
                            longitud : data.longitude,
                            tipo: 'd',
                            foto_base: foto.urlBase.base64
                        }; 
                        console.log(data); 
                        let deudor = await AsyncStorage.getItem('@formulario_deudor');             
                        if(deudor!=null){
                            let nuevoform = JSON.parse(deudor);
                            for(let i = 0; i < nuevoform.length; i++){
                                listaFormulario.push(nuevoform[i]);
                            }
                            listaFormulario.push(formulario);
                            await AsyncStorage.setItem('@formulario_deudor',JSON.stringify(listaFormulario)); 
                        }else{
                            listaFormulario.push(formulario);
                            await AsyncStorage.setItem('@formulario_deudor',JSON.stringify( listaFormulario));
                        }   
                        setIsLoading(false); 
                        ToastAndroid.show("Se ha guardado con éxito",1000); 
                        navigation.navigate("listaDeudores");   
                   }else{
                        alert("No ha llenado los datos del Conyugue");
                    }
                }else{
                    alert("No ha validado la dirección y referencia");
                }
           })();
/*            foto.url =pathDefault;           
 */        }else{
            alert("No se puede guardar");    
        } 
     };   
    return (
        <View>
            <ScrollView>
                <Loading isVisible={isLoading} text="Guardando Datos" />
                <DatosSolicitud
                    navigation ={navigation}
                    route ={route}
                /> 
                <DatosCasa      />
                <DatosTrabajo   /> 
                <DatosPersonales  /> 
                <DatosReferencias  /> 
                <VistaFoto />    
                <Observacion />                
                <View style={styles.contenedor}>
                    <View style={styles.containerBtn2}>   
                        <TouchableOpacity 
                        style={{ marginHorizontal:20,marginVertical:20, backgroundColor: '#1EA417',
                                    flex:1,height:70,borderRadius:10,width:'70%'}}
                                    onPress={guardar} > 
                        <Icon name='save' type='font-awesome' color='#ffff' 
                                style={{ marginVertical: 5,marginVertical:5}}/>  
                        <Text style={{ marginHorizontal:5 ,fontSize:15,fontWeight:'bold',color:'#ffff',
                                    textAlign:'center'}} >GUARDAR</Text>                   
                    </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}


function AreaFoto(){
    const camRef = useRef(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [hasPermission, setHasPermission] = useState(null);
    const [ capturedPhoto, setCapturedPhoto] = useState(null);
    const [open, setOpen] = useState(null);
    const [openCamara, setOpenCamara] = useState(camaraa);
    const [isLoading, setIsLoading] = useState(false);

    async function takePicture(){
        if(camRef){
           setIsLoading(true);
           const dataPhoto = await camRef.current.takePictureAsync(options);
           foto.urlBase =dataPhoto;
           setOpen(true);
           data.photo = dataPhoto.uri;
           setIsLoading(false);
        }
      }

    const closeCamera = () => {
        setOpenCamara(false);
     };  

          
    return (
        
        <Modal animationType="slide" transparent ={false} visible={openCamara}> 
            <SafeAreaView style={styles.safecontainer}>
            <Camera 
                style={{flex:1}}
                type={type}
                ref={camRef}
            >
        <View  style={{
          flex:1,
          backgroundColor:'transparent',
          flexDirection:'row'}}>
        </View>
        </Camera>
        <View >     
            <View style={styles.contenedor}>
                <View style={styles.containerBtn}>             
                </View>
                <View style={styles.containerBtn}>      
                    <TouchableOpacity 
                        style={styles.btnCamera}
                        onPress={takePicture}>
                        <FontAwesome name='camera' size={23} color="#fff"/>         
                    </TouchableOpacity>
                </View>
                <View style={styles.containerBtn}>  
                    <TouchableOpacity 
                        style={styles.btnClose}
                        onPress={closeCamera}>
                        <FontAwesome name='close' size={30} color="#fff"/>         
                    </TouchableOpacity>  
                </View>      
            </View>
        </View>
        <View style={{height:80}}></View>
        <Loading isVisible={isLoading} text="Guardando Foto" />

        {capturedPhoto && 
          <Modal 
                animationType="slide" 
                transparent ={false}
                visible={open}          >
            <View 
                style={{flex:1, justifyContent:'center',alignItems:'center',margin:5}}>
                <Image 
                    style ={{width:'100%',height:'100%',}}
                    source={{uri:capturedPhoto}}
                />
            </View>
            <View style={{
                backgroundColor:'#110F0C',width:'100%',height:80,flexDirection:'row'
               }}>
                 <View style={styles.containerBtn}>             
                </View>
                <View style={styles.containerBtn}>      
                    <TouchableOpacity 
                        style={styles.btnCamera}
                        onPress={closeCamera}>
                        <FontAwesome name = "send" size = {40} color = "#fff" />        
                    </TouchableOpacity>
                </View>

                 <View style={styles.containerBtn}>  
                    <TouchableOpacity 
                        style={styles.btnClose}
                        onPress={vaciarFoto}>
                        <FontAwesome name='close' size={30} color="#fff"/>         
                    </TouchableOpacity>  
                </View>                       
            </View>      


          </Modal>
        }
      </SafeAreaView>
    </Modal>
    )

}

function DatosCasa(){
    var zona            = ["Seleccione una opción","URBANA", "PERIFERICA", "RURAL"];
    var estViv          = ["Seleccione una opción","MUY BUENO", "BUENO", "REGULAR","MALO"];
    var tipViv          = ["Seleccione una opción","CASA", "DEPARTAMENTO", "CUARTO INTERNO","CASA DE CAÑA"];  //  TIPO DE VIVIENDA
    var proViv          = ["Seleccione una opción","PROPIA", "ARRENDADA", "FAMILIARES"];                          //  PROPIETARIO DE VIVIENDA
    var timVid         = ["Seleccione una opción","1-4 MESES", "4-8 MESES","8-12 MESES","1- 4 AÑOS", "4-8 AÑOS","MAS DE 8 AÑOS"];    

    const zonaItems = zona.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const estVivItems = estViv.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));
    const tipVivItems = tipViv.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const proVivItems = proViv.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     
    const timVidItems = timVid.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));  
    const [stateZona, setStateZona] = useState("Seleccione una opción");
    const [stateEstViv, setStateEstViv] = useState("Seleccione una opción");
    const [stateTipViv, setStateTipViv] = useState("Seleccione una opción");
    const [stateProViv, setStateProViv] = useState("Seleccione una opción");
    const [stateTimVid, setStateTimVid] = useState("Seleccione una opción");

    data.zona                   = stateZona    
    data.estadoVivienda         = stateEstViv    
    data.tipoVivienda           = stateTipViv    
    data.vivienda               = stateProViv    
    data.tiempoEnAniosVivienda  = stateTimVid    
   
    return (

        <View style={{paddingTop:20}}>
            
        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>
           <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS DE LA CASA
           </Text>
        </View>
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    <Text style={{ fontWeight:'bold', }}>Zona:</Text>                  
                    <Picker
                            selectedValue={stateZona}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateZona(itemValue)
                            }
                        >
                            {zonaItems}
                    </Picker> 
                    <Text style={{ fontWeight:'bold', }}>Estado de la vivienda:</Text>                  
                    <Picker
                        selectedValue={stateEstViv}
                        style={styles.inputAndroid}
                        onValueChange={(itemValue, itemIndex) =>
                        setStateEstViv(itemValue)
                        } 
                    >
                    {estVivItems}
                    </Picker> 
                    <Text style={{ fontWeight:'bold', }}>Tipo de vivienda:</Text>  
                    <Picker
                        selectedValue={stateTipViv}
                        style={styles.inputAndroid}
                        onValueChange={(itemValue, itemIndex) =>
                        setStateTipViv(itemValue)
                    } >
                    {tipVivItems}
                    </Picker>   
                    <Text style={{ fontWeight:'bold', }}>La vivienda es:</Text>  
                    <Picker
                        selectedValue={stateProViv}
                        style={styles.inputAndroid}
                        onValueChange={(itemValue, itemIndex) =>
                        setStateProViv(itemValue)
                    } >
                    {proVivItems}
                    </Picker> 
                    <Text style={{ fontWeight:'bold', }}>Tiempo en años:</Text>                  
                    <Picker
                        selectedValue={stateTimVid}
                        style={styles.inputAndroid}
                        onValueChange={(itemValue, itemIndex) =>
                        setStateTimVid(itemValue)
                    } >
                    {timVidItems}
                </Picker> 
                </View> 
             </View>     
        </View>

    )
}

function DatosTrabajo(){
    var sitVid          = ["Seleccione una opción","BUENA", "REGULAR", "HUMILDE CON PROBABILIDAD DE PAGO","HUMILDE SIN PROBABILIDAD DE PAGO"];       
    var labor           = ["Seleccione una opción","NO TRABAJA","INDEPEDIENTE", "DEPENDIENTE"];
    var timTrab         = ["Seleccione una opción","1-4 MESES", "4-8 MESES","8-12 MESES","1- 4 AÑOS", "4-8 AÑOS","MAS DE 8 AÑOS"];    
    
    const sitVidItems = sitVid.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));
    const laborItems = labor.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const timTrabItems = timTrab.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     

    const [stateSitVid, setStateSitVid] = useState("Seleccione una opción");
    const [stateLabor, setStateLabor] = useState("Seleccione una opción");
    const [stateTimTrab, setStateTimTrab] = useState("Seleccione una opción");

    data.situacionVida          = stateSitVid    
    data.labores                = stateLabor    
    data.tiempoEnAniosLabores   = stateTimTrab    

    return (

        <View style={{paddingTop:20}}>
        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>
           <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS DEL TRABAJO
           </Text>
        </View>
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    <Text style={{ fontWeight:'bold', }}> Situación de vida:</Text>                  
                    <Picker
                            selectedValue={stateSitVid}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateSitVid(itemValue)
                        } >
                        {sitVidItems}
                    </Picker> 
                    <Text style={{ fontWeight:'bold', }}> Labores:</Text>  
                    <Picker
                                selectedValue={stateLabor}
                                style={styles.inputAndroid}
                                onValueChange={(itemValue, itemIndex) =>
                                setStateLabor(itemValue)
                            } >
                            {laborItems}
                            </Picker> 
                    <Text style={{ fontWeight:'bold', }}> Tiempo en año:</Text>                  
              
                    <Picker
                            selectedValue={stateTimTrab}
                            style={styles.inputAndroid}
                            onValueChange={(itemValue, itemIndex) =>
                            setStateTimTrab(itemValue)
                        } >
                        {timTrabItems}
                    </Picker> 
                </View>
        </View>
        </View>

    )
}

function DatosPersonales(){
    const [resHon, setResHon]   = useState(false)
    const [deudas, setDeudas]   = useState(false)
    const [pro, setPro]         = useState(false);
    const [casado, setCasado]   = useState(false);

    var laborConyug           = ["Seleccione una opción","NO TRABAJA","INDEPEDIENTE", "DEPENDIENTE"];
    var timTrabConyug         = ["Seleccione una opción","1-4 MESES", "4-8 MESES","8-12 MESES","1- 4 AÑOS", "4-8 AÑOS","MAS DE 8 AÑOS"];    
   
    const laborConyugItems = laborConyug.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    )); 
    const timTrabConyugItems = timTrabConyug.map(i => (
        <Picker.Item key={i} label={i.toString()} value={i} />
    ));     

    const [stateLaborConyug, setStateLaborConyug] = useState("Seleccione una opción");
    const [stateTimTrabConyug, setStateTimTrabConyug] = useState("Seleccione una opción");

   
    data.responsable_honesto                    = resHon
    data.mantiene_otras_deudas                  = deudas
    data.problematico                           = pro
    data.estadoCivil.esCasado                   = casado   
    data.estadoCivil.laboresConyug              = stateLaborConyug; 
    data.estadoCivil.tiempoEnAniosLaboresCoyug  = stateTimTrabConyug;

    const getCasado = () => {  
        if(casado==true){
           setCasado(false);
        }
        if(casado==false){
            setCasado(true);
        }               
    };
    return(

        <View style={{paddingTop:20}}>
        <View style={{
            backgroundColor:'#0061a7',width:'95%',
            alignSelf:'center',flexDirection:'row',
            borderTopStartRadius:5,borderTopEndRadius:10}}>
           <Text style={{width:'50%',alignSelf:'flex-start',color:'#fff',
                           fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
               DATOS PERSONALES
           </Text>
        </View> 
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'75%',paddingLeft:10,paddingVertical:10}}>   
                        <Text>Responsable /Honesto</Text>
                    </View>   
                    <View style={{width:'25%',alignItems:'center',paddingVertical:10}}>  
                        <Switch
                            style={{color: 'red',alignItems: 'flex-end',}}
                            rankColor ={{false : '#767577' , true: '#06D6A0'}}
                            thumbColor ={resHon ?  '#fff' : '#f4f3f4'}
                            onValueChange={()=> (setResHon((preVal)=>!preVal,data))}
                            value = {resHon}
                            /> 
                    </View>  
                </View>
        </View>  
        <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:1,borderTopWidth:1,
                 borderColor:'#B3BAC4'
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'75%',paddingLeft:10,paddingVertical:10}}>   
                        <Text>Mantiene otras deudas</Text>
                    </View>   
                    <View style={{width:'25%',alignItems:'center',paddingVertical:10}}>  
                        <Switch
                            rankColor ={{false : '#767577' , true: '#06D6A0'}}
                            thumbColor ={deudas ?  '#fff' : '#f4f3f4'}
                            onValueChange={()=> setDeudas((preVal)=>!preVal)}
                            value = {deudas}
                            />  
                    </View>  
                </View>
        </View>

            <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row', borderBottomWidth:1.5,borderColor:'#B3BAC4'   
                    }}>
                    <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                        <View style={{width:'75%',paddingLeft:10,paddingVertical:10}}>   
                            <Text>Problemático</Text>
                        </View>   
                        <View style={{width:'25%',alignItems:'center',paddingVertical:10}}>                         
                            <Switch
                                rankColor ={{false : '#767577' , true: '#06D6A0'}}
                                thumbColor ={pro ?  '#fff' : '#f4f3f4'}
                                onValueChange={()=> (setPro((preVal)=>!preVal))}
                                value = {pro}
                                /> 
                        </View>  
                    </View>
            </View> 
            <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row',
                    }}>
                    <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                        <View style={{width:'75%',paddingLeft:10,paddingVertical:10}}>   
                            <Text>Esta casado </Text>
                        </View>   
                        <View style={{width:'25%',alignItems:'center',paddingVertical:10}}>                         
                            <Switch
                                rankColor ={{false : '#767577' , true: '#06D6A0'}}
                                thumbColor ={casado ?  '#fff' : '#f4f3f4'}
                                onValueChange={()=> (getCasado())}   
                                value = {casado}
                                /> 
                        </View>  
                    </View>
            </View>
                {casado &&
            <View> 
                <Encabezado 
                    txt_info="DATOS DEL CONYUGUE"
                />
                <View style={{
                        backgroundColor:'#fff',width:'95%',
                        alignSelf:'center',flexDirection:'row',
                        }}>
                        <View style={{width:'100%',alignSelf:'center',color:'#fff',
                                        fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                            <Text style={{ fontWeight:'bold', }}> Labores:</Text>                  
                            <Picker
                                    selectedValue={stateLaborConyug}
                                    style={styles.inputAndroid}
                                    onValueChange={(itemValue, itemIndex) =>
                                    setStateLaborConyug(itemValue)
                                } >
                                {laborConyugItems}
                            </Picker>  
                            <Text style={{ fontWeight:'bold', }}> Tiempo en años:</Text>  
                            <Picker
                                        selectedValue={stateTimTrabConyug}
                                        style={styles.inputAndroid}
                                        onValueChange={(itemValue, itemIndex) =>
                                        setStateTimTrabConyug(itemValue)
                                    } >
                                    {timTrabConyugItems}
                            </Picker>                             
                        </View>
                </View>
            </View>
            }        
          

        </View>        


    )

}

function VistaFoto(){
    const [errorMsg, setErrorMsg] = useState(null);
    const [hasPermission, setHasPermission] = useState(null);
    const [camara, setCamara] = useState(null);
    const [imagen, setImagen] = useState('');

    const camRef = useRef(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [open, setOpen] = useState(null);
    const [openCamara, setOpenCamara] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [previsualizar, setPrevisualizar] = useState(false);

    async function takePicture(){
        if(camRef){
           setIsLoading(true);
           const dataPhoto = await camRef.current.takePictureAsync(options);
           foto.urlBase = dataPhoto;
           data.photo = dataPhoto.uri;
           foto.url = dataPhoto.uri
           setImagen(dataPhoto.uri);
           setOpenCamara(false);
           setIsLoading(false);

           
        }
      }

    const closeCamera = () => {
        setOpenCamara(false);
     };  



    useEffect(() => {
        (async () => {
          let { status } = await Location.requestPermissionsAsync();
          if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
          }
        })();
      }, []); 

    const tomarFoto = () => {
        setCamara(null);
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
            setCamara(true);
            if(hasPermission===true){
                setOpenCamara(true);
            }else{
                setOpenCamara(false);
            }
            
        })();
        if (hasPermission === null) {
            setCamara(false);
            return console.log("salio de los permisos");
        }
        if (hasPermission === false) {
            setCamara(false);
            return console.log("No acepto los permisos");
        }  
       
     }; 

     const previsualizarFoto = () => {
        

     }
    
    

    return(

        <View style={{paddingTop:20}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:10}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
                    FOTO DE DOMICILIO
                </Text>
            </View>
            <View style={{backgroundColor:'#171E27',width:'95%', 
                alignSelf:'center',borderBottomWidth:1,
                borderBottomColor:'#B3BAC4'}}> 
                <View style={{width:'100%'}}>
                    <TouchableOpacity onPress={previsualizarFoto}>
                        {
                            imagen ?
                                <Image 
                                    style={{width:'100%', height: 400}}
                                    source={{uri: imagen, cache: "reload"}}
                                    resizeMode="contain" 
                                />  
                            :
                                <Image 
                                    style={{width:'100%', height: 400}}
                                    source={require('../../../../assets/imagen/no-image.png')}
                                    resizeMode="contain"   
                                />   
                        }
                        
                    </TouchableOpacity>
                </View>            

                <View style={{flexDirection: 'row',justifyContent:'center',backgroundColor: '#fff',width:'100%'}}>
                        <View style={{width:'95%'}}>
                            <TouchableOpacity 
                                style={{/*  marginHorizontal:20, */ backgroundColor: '#209BA4',marginVertical:10,
                                        flex:1,height:50,alignItems:'center',justifyContent:'center',borderRadius:10}}
                                        onPress={tomarFoto} > 
                            <View style={{flexDirection:'row'}}>
                                    <Icon name='camera-retro' type='font-awesome' color='#ffff' 
                                    style={{ marginVertical: 5,marginVertical:5}}/> 
                                <Text style={{ marginHorizontal:10 ,marginVertical: 5,fontSize:15,fontWeight:'bold',color:'#ffff',
                                        textAlign:'center',justifyContent:'center',alignItems:'center',alignContent:'center'}} >Tomar Foto</Text>
                            </View>                 
                            </TouchableOpacity>
                        </View>
                            {openCamara  ? 
                                <Modal animationType="slide" transparent ={false} visible={openCamara}> 
                                    <SafeAreaView style={styles.safecontainer}>
                                        <Camera 
                                            style={{flex:1}}
                                            type={type}
                                            ref={camRef}
                                        >
                                            <View  style={{
                                                flex:1,
                                                backgroundColor:'transparent',
                                                flexDirection:'row'}}>
                                            </View>
                                        </Camera>
                                    </SafeAreaView>
                                    <View >     
                                    <View style={styles.contenedor}>
                                        <View style={styles.containerBtn}>             
                                        </View>
                                        <View style={styles.containerBtn}>      
                                            <TouchableOpacity 
                                                style={styles.btnCamera}
                                                onPress={takePicture}>
                                                <FontAwesome name='camera' size={23} color="#fff"/>         
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.containerBtn}>  
                                            <TouchableOpacity 
                                                style={styles.btnClose}
                                                onPress={closeCamera}>
                                                <FontAwesome name='close' size={30} color="#fff"/>         
                                            </TouchableOpacity>  
                                        </View>      
                                    </View>
                                </View>
                                <View style={{height:80}}></View>
                                <Loading isVisible={isLoading} text="Guardando Foto" />
                            </Modal>
                            :   null
                            }  
                </View>  
            </View>   
        </View>
    )
}



function Observacion(){
    const [observacion, setObservacion] = useState('');
    const editarObservacion = (text) => {
        setObservacion(text)
        datosObservacion.observacion = text;
     }; 
    return(
        <View style={{paddingTop:20,paddingBottom:20}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:10}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
                    OBSERVACIÓN
                </Text>
            </View>    
                <View style={{
                        backgroundColor:'#fff',width:'95%',
                        alignSelf:'center',flexDirection:'row',
                    }}>

                        <View style={{width:'100%'}}>
                        <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>Observación:</Text>
                            <TextInput 
                                style={{height:100,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                                width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                                multiline={true}
                                placeholder={'Escriba una observación'}
                                onChangeText={text => editarObservacion(text)}
                                value={observacion}                  
                        />
                        <View>
                            <Text></Text>    
                        </View>   
                    </View>  
                </View>
        </View>       
    )
}

var  estadoDirrecionCorrecta = false;
var  estadoReferenciaCorrecta = false;

function DatosSolicitud(props) {
    const {navigation, route} = props;
    const {code,deuGar,dnombre,dcedula,ddireccion,dreferencia,id} = route.params;
    const [dirCorr, setDirCorr] = useState(false)
    const [direccion, setDireccion] = useState('');
    const [refCorr, setRefCorr] = useState(false)
    const [referencia, setReferencia] = useState('');
    const [stateCasado, setStateCasado] = useState(true)


    const editarDireccion = (text) => {
        setDireccion(text)
        datosObservacion.direccion = text;
     }; 

     const editarReferencia = (text) => {
        setReferencia(text)
        datosObservacion.referencia = text;
     };

     const checkedDireccion = () => {
        if(dirCorr){
            setDirCorr(false);
            estadoDirrecionCorrecta =false;            
        }else{
            datosObservacion.direccion = '';    
            setDireccion('')   
            setDirCorr(true);
            estadoDirrecionCorrecta = true;                
        }       
     };

     const checkedReferencia = () => {
        if(refCorr){
            setRefCorr(false);
            estadoReferenciaCorrecta=false;             
        }else{
            datosObservacion.referencia = '';
            setReferencia('')
            setRefCorr(true);
            estadoReferenciaCorrecta=true;           
        }
     }; 

     
    return(
        <View style={{paddingTop:20}}>
            <View style={{
                 backgroundColor:'#0061a7',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10}}>
                    SOLICITUD # {code.toString()} {id}
                </Text>
             </View>
            <View style={{backgroundColor:'#fff',width:'95%', 
                alignSelf:'center',paddingVertical:4,paddingLeft:10,borderBottomWidth:1,
                borderBottomColor:'#B3BAC4'}}> 
                <Text style={{ fontSize: 15, fontWeight: 'bold',textAlign:'center'}} >
                    {deuGar}
                </Text>
            </View>             
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row'
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5}}>
                    <Icon name='user' type='font-awesome' />
                </View>        
                <Text style={{width:'90%',color:'#000',
                                paddingVertical:5,paddingLeft:10}}>
                    {dnombre} 
                </Text>                
            </View>
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:1,
                    borderColor:'#B3BAC4',
                }}>
                <View style={{width:'10%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5}}>
                    <Icon name='id-card' type='font-awesome' />
                </View>        
                <Text style={{width:'90%',color:'#000',
                                paddingVertical:5,paddingLeft:10}}>
                    {dcedula} 
                </Text>                
             </View>             
            


             <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                  
                }}>
                <View style={{width:'80%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10,}}>
                      <Text style={{ fontWeight:'bold', }}>Dirección:</Text>                  
                      <Text>{ddireccion.toString()}                       
                      </Text>
                </View>        
                <Text style={{width:'20%',color:'#000',paddingVertical:5}}>
                <CheckBox                         
                        checked={dirCorr}
                        checkedColor='#1dd1a1'
                        onPress={checkedDireccion}
                />  
                </Text>                
             </View>

            {dirCorr ? 
                null:
                <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row',
                   }}>

                    <View style={{width:'100%'}}>
                        <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>Corregir la dirección:</Text>
                        <TextInput 
                            style={{height:100,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                            width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                            multiline={true}
                            placeholder={'Ingrese la dirrección correcta'}
                            onChangeText={text => editarDireccion(text)}
                            value={direccion}                  
                        />
                        <View>
                            <Text></Text>    
                        </View>   
                    </View>  
                </View>
            } 

             <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                }}>
               <View style={{width:'80%',alignSelf:'center',color:'#fff',
                                fontWeight:'bold',paddingVertical:5,paddingLeft:10,}}>
                      <Text style={{ fontWeight:'bold', }}>Referencia:</Text>                  
                      <Text>{dreferencia.toString()}                       
                      </Text>
                </View>        
                <Text style={{width:'20%',color:'#000',paddingVertical:5}}>
                <CheckBox                         
                        checked={refCorr}
                        checkedColor='#1dd1a1'
                        onPress={checkedReferencia}
                />  
                </Text>  
             </View>


             {refCorr ? 
                null:
                <View style={{
                    backgroundColor:'#fff',width:'95%',
                    alignSelf:'center',flexDirection:'row',
                   }}>

                    <View style={{width:'100%'}}>
                    <Text style={{ fontWeight:'bold',paddingLeft:10,paddingVertical:5 }}>Corregir la referencia:</Text>
                    <TextInput 
                        style={{height:100,paddingVertical:10,textAlignVertical:'top',alignSelf:'center',
                        width:'90%',backgroundColor:'#f1f3f6',borderRadius:6,paddingHorizontal:10}}
                        multiline={true}
                        placeholder={'Ingrese la referencia correcta'}
                        onChangeText={text => editarReferencia(text)}
                        value={referencia}                  
                    />
                    <View>
                        <Text></Text>    
                    </View>   
                </View>  
            </View>
            }              
  
    </View>
    )
}

const limiteSup = 4;
const limiteInf = 0;

let dataa = [];
const activado = '#ff5c5c';
const desactivado = '#ff5c5c';


function DatosReferencias() {    
    const [fam, setFam] = useState(0);
    const [vec, setVec] = useState(0);
    const [ami, setAmi] = useState(0);
    const [jef, setJef] = useState(0);
    const [stateVec, setStateVec] = useState({data: [],valor: []});
    const [stateFam, setStateFam] = useState({data: []});
    const [stateAmi, setStateAmi] = useState({data: []});
    const [stateJef, setStateJef] = useState({data: []});

    
    const addBtn =(cont, setCont, stateRef) => () =>{
        if(cont < limiteSup){
            setCont(prevState => prevState+1);
            const datos =  { bueno: '', malo:'',btn_bueno:'#fff',btn_malo:'#fff',valor:'',
                             txt_bueno:'#2068CA',txt_malo:'#2068CA',bold_bueno:'normal',bold_malo:'normal'
                             }
            stateRef.data.push(datos);

        }
    }

    const delBtn =(cont, setCont, stateRef) => () =>{
        if(cont  > limiteInf){
            setCont(prevState => prevState-1);  
            stateRef.data.pop();
        }
    }
         
    const rename = (index, buena, mala,stateRef,setStateRef,tipo) => () => {
        const data = stateRef.data;
         if(buena==1){
            data[index].btn_bueno ='#208DCA';
            data[index].btn_malo ='#fff';
            data[index].valor ='Buena';
            data[index].txt_bueno ='#fff';
            data[index].txt_malo ='#208DCA';
            data[index].bold_bueno ='bold';
            data[index].bold_malo ='normal';

        }
        if(mala==1){
          data[index].btn_bueno ='#fff';
          data[index].btn_malo ='#208DCA';
          data[index].valor ='Mala';
          data[index].txt_bueno ='#208DCA';
          data[index].txt_malo ='#fff';
          data[index].bold_bueno ='normal';
          data[index].bold_malo ='bold';
 
        }
        data[index].bueno = buena;
        data[index].malo = mala;
        setStateRef({ data });
        if(tipo=='Vecino'){
            referencias.vecinos = stateRef.data;
        }
        if(tipo=='Familiar'){
            referencias.familiares = stateRef.data;

        }
        if(tipo=='Amigo'){
            referencias.amigos = stateRef.data;

        }
        if(tipo=='Jefe'){
            referencias.jefes = stateRef.data;

        }
    }

        return(
            <View style={{paddingTop:20}}>
            <View style={{
                backgroundColor:'#0061a7',width:'95%',
                alignSelf:'center',flexDirection:'row',
                borderTopStartRadius:5,borderTopEndRadius:10}}>
               <Text style={{width:'100%',alignSelf:'flex-start',color:'#fff',
                               fontWeight:'bold',paddingVertical:10,paddingLeft:10}}>
                   REFERENCIAS DEL ENTREVISTADO
               </Text>
            </View> 
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:0.5,borderColor:'#B3BAC4'
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'30%',paddingLeft:10,paddingVertical:10,justifyContent:'center'}}>   
                        <Text>Vecinos</Text>
                    </View>   
                    <View style={{width:'70%',alignItems:'center',justifyContent:'center',paddingVertical:10,flexDirection:'row'}}>  
                    <TouchableOpacity 
                        onPress={delBtn(vec,setVec,stateVec)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginRight:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:22,textAlign: 'center',textAlignVertical: 'center'}}> - </Text>
                    </TouchableOpacity>
                    <View>
                        <Text style={{borderWidth:0.5,width:35,height:35,textAlign: 'center',textAlignVertical: 'center'}}>
                            {vec}</Text>
                    </View>   
                    <TouchableOpacity 
                        onPress={addBtn(vec,setVec,stateVec)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginLeft:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:20 , textAlign: 'center',textAlignVertical: 'center'}}> + </Text>
                    </TouchableOpacity>  
                    </View>  
                </View>
            </View>  
           
            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',
                }}>
                    {stateVec.data.map(({btn_bueno,btn_malo,txt_bueno,txt_malo,bold_bueno,bold_malo}, index) => {
                        return(
                        <View key={index}>
                            <View style={{flexDirection:'row',paddingVertical:5}} >
                            <View style={{width:'30%',justifyContent:'center'}}>
                                <Text style={{marginLeft:10}}>Vecino {index+1 } :  </Text>
                            </View>
                            <View 
                                style={{width:'70%',flexDirection:'row',height:40,
                                       justifyContent:'center'
                                    }}>
                                
                                <TouchableOpacity  style={{width:'40%',justifyContent:'center',
                                    alignItems:'center',borderWidth:1,borderColor:'#208DCA',
                                    backgroundColor:btn_bueno,borderTopLeftRadius:10,borderBottomLeftRadius:10}}
                                    onPress={rename(index,1,0,stateVec,setStateVec,"Vecino")}
                                >
                                    <Text  style={{color:txt_bueno,fontWeight:bold_bueno}}>Buena</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  
                                    style={{width:'40%',height:'auto',justifyContent:'center',alignItems:'center',
                                    borderWidth:1,borderColor:'#208DCA',borderTopRightRadius:10,borderBottomRightRadius:10,
                                    borderStartWidth:0.5,backgroundColor:btn_malo}}
                                    onPress={rename(index,0,1,stateVec,setStateVec,"Vecino")}
                                    >
                                    <Text  style={{color:txt_malo,fontWeight:bold_malo}}>Mala</Text>
                                    

                                </TouchableOpacity>    
                                                            
                            </View>                
                            </View>
                        </View>
                        );
                    })}
                </View>   


            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:0.3,borderColor:'#B3BAC4',borderTopWidth:0.3
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'30%',paddingLeft:10,paddingVertical:10,justifyContent:'center'}}>   
                        <Text>Familiares</Text>
                    </View>   
                    <View style={{width:'70%',alignItems:'center',justifyContent:'center',paddingVertical:10,flexDirection:'row'}}>  
                    <TouchableOpacity 
                        onPress={delBtn(fam,setFam,stateFam)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginRight:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:22,textAlign: 'center',textAlignVertical: 'center'}}> - </Text>
                    </TouchableOpacity>
                    <View>
                        <Text style={{borderWidth:0.5,width:35,height:35,textAlign: 'center',textAlignVertical: 'center'}}>
                            {fam}</Text>
                    </View>   
                    <TouchableOpacity 
                        onPress={addBtn(fam,setFam,stateFam)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginLeft:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:20 , textAlign: 'center',textAlignVertical: 'center'}}> + </Text>
                    </TouchableOpacity>  
                    </View>  
                </View>
            </View>  


            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',
                }}>
                    {stateFam.data.map(({btn_bueno,btn_malo,txt_bueno,txt_malo,bold_bueno,bold_malo}, index) => {
                        return(
                        <View key={index}>
                            <View style={{flexDirection:'row',paddingVertical:5}} >
                            <View style={{width:'30%',justifyContent:'center'}}>
                                <Text style={{marginLeft:10}}>Familiar {index+1 } :  </Text>
                            </View>
                            <View 
                                style={{width:'70%',flexDirection:'row',height:40,
                                       justifyContent:'center'
                                    }}>
                                
                                <TouchableOpacity  style={{width:'40%',justifyContent:'center',
                                    alignItems:'center',borderWidth:1,borderColor:'#208DCA',
                                    backgroundColor:btn_bueno,borderTopLeftRadius:10,borderBottomLeftRadius:10}}
                                    onPress={rename(index,1,0,stateFam,setStateFam,"Familiar")}
                                >
                                    <Text  style={{color:txt_bueno,fontWeight:bold_bueno}}>Buena</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  
                                    style={{width:'40%',height:'auto',justifyContent:'center',alignItems:'center',
                                    borderWidth:1,borderColor:'#208DCA',borderTopRightRadius:10,borderBottomRightRadius:10,
                                    borderStartWidth:0.5,backgroundColor:btn_malo}}
                                    onPress={rename(index,0,1,stateFam,setStateFam,"Familiar")}
                                    >
                                    <Text  style={{color:txt_malo,fontWeight:bold_malo}}>Mala</Text>
                                    

                                </TouchableOpacity>    
                                                            
                            </View>                
                            </View>
                        </View>
                        );
                    })}
                </View>   

                
                <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:0.3,borderColor:'#B3BAC4',borderTopWidth:0.3
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'30%',paddingLeft:10,paddingVertical:10,justifyContent:'center'}}>   
                        <Text>Amigos</Text>
                    </View>   
                    <View style={{width:'70%',alignItems:'center',justifyContent:'center',paddingVertical:10,flexDirection:'row'}}>  
                    <TouchableOpacity 
                        onPress={delBtn(ami,setAmi,stateAmi)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginRight:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:22,textAlign: 'center',textAlignVertical: 'center'}}> - </Text>
                    </TouchableOpacity>
                    <View>
                        <Text style={{borderWidth:0.5,width:35,height:35,textAlign: 'center',textAlignVertical: 'center'}}>
                            {ami}</Text>
                    </View>   
                    <TouchableOpacity 
                        onPress={addBtn(ami,setAmi,stateAmi)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginLeft:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:20 , textAlign: 'center',textAlignVertical: 'center'}}> + </Text>
                    </TouchableOpacity>  
                    </View>  
                </View>
            </View>  


            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',
                }}>
                    {stateAmi.data.map(({btn_bueno,btn_malo,txt_bueno,txt_malo,bold_bueno,bold_malo}, index) => {
                        return(
                        <View key={index}>
                            <View style={{flexDirection:'row',paddingVertical:5}} >
                            <View style={{width:'30%',justifyContent:'center'}}>
                                <Text style={{marginLeft:10}}>Amigo {index+1 } :  </Text>
                            </View>
                            <View 
                                style={{width:'70%',flexDirection:'row',height:40,
                                       justifyContent:'center'
                                    }}>
                                
                                <TouchableOpacity  style={{width:'40%',justifyContent:'center',
                                    alignItems:'center',borderWidth:1,borderColor:'#208DCA',
                                    backgroundColor:btn_bueno,borderTopLeftRadius:10,borderBottomLeftRadius:10}}
                                    onPress={rename(index,1,0,stateAmi,setStateAmi,"Amigo")}
                                >
                                    <Text  style={{color:txt_bueno,fontWeight:bold_bueno}}>Buena</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  
                                    style={{width:'40%',height:'auto',justifyContent:'center',alignItems:'center',
                                    borderWidth:1,borderColor:'#208DCA',borderTopRightRadius:10,borderBottomRightRadius:10,
                                    borderStartWidth:0.5,backgroundColor:btn_malo}}
                                    onPress={rename(index,0,1,stateAmi,setStateAmi,"Amigo")}
                                    >
                                    <Text  style={{color:txt_malo,fontWeight:bold_malo}}>Mala</Text>
                                    

                                </TouchableOpacity>    
                                                            
                            </View>                
                            </View>
                        </View>
                        );
                    })}
                </View>   

                <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',flexDirection:'row',
                 borderBottomWidth:0.3,borderColor:'#B3BAC4',borderTopWidth:0.3
                }}>
                <View style={{width:'100%',backgroundColor:'#fff',flexDirection:'row'}}>
                    <View style={{width:'30%',paddingLeft:10,paddingVertical:10,justifyContent:'center'}}>   
                        <Text>Jefe</Text>
                    </View>   
                    <View style={{width:'70%',alignItems:'center',justifyContent:'center',paddingVertical:10,flexDirection:'row'}}>  
                    <TouchableOpacity 
                        onPress={delBtn(jef,setJef,stateJef)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginRight:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:22,textAlign: 'center',textAlignVertical: 'center'}}> - </Text>
                    </TouchableOpacity>
                    <View>
                        <Text style={{borderWidth:0.5,width:35,height:35,textAlign: 'center',textAlignVertical: 'center'}}>
                            {jef}</Text>
                    </View>   
                    <TouchableOpacity 
                        onPress={addBtn(jef,setJef,stateJef)}
                        style={{backgroundColor:'#0061a7',
                        width:35,height:35,marginLeft:'10%',borderRadius:10}}>
                        <Text style={{color:'#fff',fontWeight:'bold',fontSize:20 , textAlign: 'center',textAlignVertical: 'center'}}> + </Text>
                    </TouchableOpacity>  
                    </View>  
                </View>
            </View>  


            <View style={{
                 backgroundColor:'#fff',width:'95%',
                 alignSelf:'center',
                }}>
                    {stateJef.data.map(({btn_bueno,btn_malo,txt_bueno,txt_malo,bold_bueno,bold_malo}, index) => {
                        return(
                        <View key={index}>
                            <View style={{flexDirection:'row',paddingVertical:5}} >
                            <View style={{width:'30%',justifyContent:'center'}}>
                                <Text style={{marginLeft:10}}>Jefe {index+1 } :  </Text>
                            </View>
                            <View 
                                style={{width:'70%',flexDirection:'row',height:40,
                                       justifyContent:'center'
                                    }}>
                                
                                <TouchableOpacity  style={{width:'40%',justifyContent:'center',
                                    alignItems:'center',borderWidth:1,borderColor:'#208DCA',
                                    backgroundColor:btn_bueno,borderTopLeftRadius:10,borderBottomLeftRadius:10}}
                                    onPress={rename(index,1,0,stateJef,setStateJef,"Jefe")}
                                >
                                    <Text  style={{color:txt_bueno,fontWeight:bold_bueno}}>Buena</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  
                                    style={{width:'40%',height:'auto',justifyContent:'center',alignItems:'center',
                                    borderWidth:1,borderColor:'#208DCA',borderTopRightRadius:10,borderBottomRightRadius:10,
                                    borderStartWidth:0.5,backgroundColor:btn_malo}}
                                    onPress={rename(index,0,1,stateJef,setStateJef,"Jefe")}
                                    >
                                    <Text  style={{color:txt_malo,fontWeight:bold_malo}}>Mala</Text>
                                    

                                </TouchableOpacity>    
                                                            
                            </View>                
                            </View>
                        </View>
                        );
                    })}
                </View> 
            </View> 

    )
}


function validarChecked(tipo){
    if(tipo==0){       
        if(!estadoDirrecionCorrecta){           
            if(datosObservacion.direccion!=''){
                console.log("No verificado, pero lleno");            
               return true;
            }else{
               console.log("No verificado");            
               return false; 
            }            
        }else{
            console.log("verificado");
            return true;
        }  
    }
    if(tipo==1){       
        if(!estadoReferenciaCorrecta){
            if(datosObservacion.referencia!=''){
                console.log("No verificado referencia, pero lleno");            
               return true;
            }else{
               console.log("No verificado");            
               return false; 
            }    
        }else{
            console.log("verificado");
            return true;
        }  
    }  
}

function validarEstadoCivil(){
    console.log(data.estadoCivil.esCasado);
    console.log(data.estadoCivil.tiempoEnAniosLaboresCoyug);
    console.log(data.estadoCivil.laboresConyug);

    if(data.estadoCivil.esCasado){
        if( (data.estadoCivil.tiempoEnAniosLaboresCoyug!=comparar && 
            data.estadoCivil.laboresConyug!=comparar) ){
            console.log("Me case y elige;")
            return true;
        }else{
            console.log("casado pero no elige opciones")
            return false;
        }
    }else{ 
        console.log("No me case ni elige")
      /*   data.estadoCivil.laboresConyug ="";
        data.estadoCivil.tiempoEnAniosLaboresCoyug =""; */
        return true;
    }
}

const styles = StyleSheet.create({
    safecontainer: {
        flex: 1,
        justifyContent: 'center',
      }, 
      containerBtn:{
        flex:1,backgroundColor:'#120D0D',flexDirection: 'row',height:80,alignItems:'center',justifyContent: 'center',
      },
      containerBtn2:{
        flex:1,backgroundColor:'transparent',flexDirection: 'row',height:80,alignItems:'center',justifyContent: 'center',
      },  
      containerBtn3:{
        flex:1,backgroundColor:'transparent',flexDirection: 'row',
        width:'50%',height:80,alignItems:'center',justifyContent: 'center',
      },       
      
      btnCamera:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#898989',
        margin:5,
        borderRadius:70,
        height:70,
        width:70
      },
      btnClose:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#F2162F',
        margin:5,
        borderRadius:60,
        height:60,
        width:60
      },
      btnUndo:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#C2C4C1',
        margin:5,
        borderRadius:60,
        height:60,
        width:60
      },      
    inputAndroid: {
        fontSize: 18,
        borderColor: '#000',
        borderWidth:1,

        color: 'black',
      },
    blockButton: {
        marginLeft:20,
        marginRight:20,       
        display : 'flex',
        alignItems : 'center',
        paddingVertical: 14,
        backgroundColor : '#EE5A24',
        borderRadius : 10,
        elevation :2,
        shadowColor : '#000',
        shadowOffset:{
            width:2,
            height:2,
        },
        shadowOpacity: 0.25,
        shadowRadius :3.5,    
    },    
    buttonText :{
        color :'#fff',
        fontWeight:'bold',
        fontSize: 20
    },

    containerCard: {
        borderRadius: 20,
        borderWidth: 0,        
    },
    headerCard: {
        borderWidth: 0,        
        flex: 1,
        alignItems: 'center',
        paddingTop: 2,
        backgroundColor: '#0061a7',
    },    
    cardContent:{
        marginTop: 10,
        marginLeft:20,
        marginRight:20,
      },
      subheading: {
        fontSize: 15,    
        fontWeight: '900',
        color: '#fff',
        fontWeight: 'bold'
      },
      cardHeader: {
        backgroundColor: '#0061a7',
        
      },
      footer: {
        width: '100%',
        paddingVertical: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      contenedor: {
        flex: 1,
        flexDirection: 'row',
        
      },
      textCmb:{
        paddingLeft:20,
        fontWeight:'bold',
      },
      inputCmb:{
          paddingStart:30
        },

})



