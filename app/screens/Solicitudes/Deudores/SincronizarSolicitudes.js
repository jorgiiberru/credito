import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'; 
import ProgressCircle from 'react-native-progress-circle';
import { Button } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {URL_FORMULARIOS_SINC,URL_SOLICITUDES_SINC} from '../../../api/Storage'
import ProbarConexionNet from '../../../components/ProbarConexionNet';
import { Alert } from 'react-native';
import Loading from '../../../components/Loading';

export default function SincronizarSolicitudes() {
    const [porcentaje, setPorcentaje] = useState(0); 
    const [totalSolicitud, setTotalSolicitud] = useState(0); 
    let temporal_formularios_solicitud_enviar = [];
    const [isLoading, setIsLoading] = useState(false);


    useEffect(() => {
        (async () => {
            let cont = 0;
            const store_deudor = await AsyncStorage.getItem('@gh_solicitudes');
            let temp_deudor = JSON.parse(store_deudor);
            console.log(temp_deudor+"------------>");      
            if(temp_deudor!=null){                
                for (let i = 0; i < temp_deudor.length; i++) {
                    if(temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='SIN' || 
                       temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='NA'){
                            console.log("Para guardarse-"+temp_deudor[i].id);
                            cont ++;
                    }
                }
                setTotalSolicitud(cont);                   
            }else{
                console.log("Estoy Vacio")
            }
         })();
      }, []); 

    const sincronizar = async() =>{
        try { 
            let estadoConexion = await ProbarConexionNet.conexion(); 
            setIsLoading(true);
            
            if(estadoConexion){ 
                const store_deudor = await AsyncStorage.getItem('@gh_solicitudes');
                let temp_deudor = JSON.parse(store_deudor);
                const store_formulario = await AsyncStorage.getItem('@formulario_deudor');
                let temp_form = JSON.parse(store_formulario);
                const store_form_garant = await AsyncStorage.getItem('@formulario_garante');
                let temp_form_garant = JSON.parse(store_form_garant);
                const store_terminadas = await AsyncStorage.getItem('@solicitud_finalizadas');                
                let listaTerminadas = []
                let temp_terminadas = JSON.parse(store_terminadas);    
                let temporal_formularios_deudores_enviar = [];
                let temporal_formularios_garantes_enviar = [];
                let temporal_formularios_solicitud_enviar = [];
            
                if(temp_form!=null){
                    for (let i = 0; i < temp_deudor.length; i++) {
                        if(temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='SIN' || 
                        temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='NA'){
                                console.log("Para guardarse-"+temp_deudor[i].id);
                                temporal_formularios_solicitud_enviar.push(temp_deudor[i]);
                        }
                    }
                }
                if(temp_form!=null){
                    for (let i = 0; i < temporal_formularios_solicitud_enviar.length; i++) {
                        for (let j = 0; j < temp_form.length; j++) {
                            if(temp_form[j].solicitud_id==temporal_formularios_solicitud_enviar[i].id){
                                temporal_formularios_deudores_enviar.push(temp_form[j]);
                                console.log("Formulario del Deudor"+"----->"+temp_form[j].solicitud_id+" dCoincide")
                            }
                        }
                    }
                }
                if(temp_form_garant!=null){
                    for (let i = 0; i < temporal_formularios_solicitud_enviar.length; i++) {                    
                        for (let j = 0; j < temp_form_garant.length; j++) {    
                            if(temp_form_garant[j].solicitud_id==temporal_formularios_solicitud_enviar[i].id){
                                temporal_formularios_garantes_enviar.push(temp_form_garant[j]);
                                console.log("Formulario del Garante----->"+temp_form_garant[j].solicitud_id+" gCoincide")
                            }
                        }
        
                    }
                } 
                 if(totalSolicitud!=0){
                    if(porcentaje!=100){
                            if(temporal_formularios_solicitud_enviar!=null){
                                if(temporal_formularios_deudores_enviar!=null){
                                    for(let i = 0; i < temporal_formularios_deudores_enviar.length; i++){
                                        fetch(URL_FORMULARIOS_SINC, {
                                            method: 'POST',
                                            headers: {
                                            Accept: 'application/json',
                                                    'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify({
                                                data                : JSON.stringify(temporal_formularios_deudores_enviar[i].data),
                                                usuario_id	        : temporal_formularios_deudores_enviar[i].usuario_id,
                                                solicitud_id	    : temporal_formularios_deudores_enviar[i].solicitud_id,
                                                fecha_verificacion	: temporal_formularios_deudores_enviar[i].fecha_verificacion,
                                                latitud             : temporal_formularios_deudores_enviar[i].latitud,
                                                longitud            : temporal_formularios_deudores_enviar[i].longitud, 
                                                tipo                : temporal_formularios_deudores_enviar[i].tipo,
                                                foto_base           : temporal_formularios_deudores_enviar[i].foto_base,
                                            })                                                                       
                                        });
                                    }                                
                                }
                                if(temporal_formularios_garantes_enviar!=null){
                                    for(let i = 0; i < temporal_formularios_garantes_enviar.length; i++){
                                        fetch(URL_FORMULARIOS_SINC, {
                                            method: 'POST',
                                            headers: {
                                            Accept: 'application/json',
                                                    'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify({
                                                data                : JSON.stringify(temporal_formularios_garantes_enviar[i].data),
                                                usuario_id	        : temporal_formularios_garantes_enviar[i].usuario_id,
                                                solicitud_id	    : temporal_formularios_garantes_enviar[i].solicitud_id,
                                                fecha_verificacion	: temporal_formularios_garantes_enviar[i].fecha_verificacion,
                                                latitud             : temporal_formularios_garantes_enviar[i].latitud,
                                                longitud            : temporal_formularios_garantes_enviar[i].longitud, 
                                                tipo                : temporal_formularios_garantes_enviar[i].tipo,
                                                foto_base           : temporal_formularios_garantes_enviar[i].foto_base,
                                            })                                                                       
                                        });
                                       
                                    }      
                                }
                                for (let i = 0; i < temp_deudor.length; i++) {
                                    if(temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='SIN' || 
                                       temp_deudor[i].destado=='SIN' && temp_deudor[i].gestado=='NA'){
                                        if(temp_deudor[i].destado=='SIN'){
                                            temp_deudor[i].destado = 'FI';                                                                      
                                        }
                                        if(temp_deudor[i].gestado=='SIN'){
                                            temp_deudor[i].gestado = 'FI';                                                                      
                                        }
                                        fetch((URL_SOLICITUDES_SINC+temp_deudor[i].id),{
                                            method: 'POST',
                                            headers: {
                                                Accept: 'application/json',
                                                'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify({
                                                destado:        temp_deudor[i].destado,
                                                gestado:        temp_deudor[i].gestado,
                                                ddireccion_cor: temp_deudor[i].ddireccion_cor,
                                                dobservacion:   temp_deudor[i].dobservacion,
                                                gdireccion_cor: temp_deudor[i].gdireccion_cor,
                                                gobservacion:   temp_deudor[i].gobservacion,
                                                dreferencia_cor:   temp_deudor[i].dreferencia,  
                                                                                
                                                })
                                            })  
                                            listaTerminadas.push(temp_deudor[i]);
                                    }
                                }
                                if(temp_terminadas != null){
                                    for (let i = 0; i < listaTerminadas.length; i++) {
                                        temp_terminadas.unshift(listaTerminadas[i]);
                                    }
                                    await AsyncStorage.setItem('@solicitud_finalizadas', JSON.stringify(temp_terminadas));
                                }else{
                                    console.log("Guardadas finalizadas")
                                    await AsyncStorage.setItem('@solicitud_finalizadas', JSON.stringify(listaTerminadas));
                                    const solic = await AsyncStorage.getItem('@solicitud_finalizadas'); 
                                }
                                setPorcentaje(prevCount => prevCount + 100);
                                setTotalSolicitud(0);
                                await AsyncStorage.setItem('@gh_solicitudes', JSON.stringify(temp_deudor)) 
    
                            }else{
                                alert("No se puede realizar esta acción")
                            }
                    }
                }else{
                    alert("No hay solicitudes pendientes por Sincronizar");
                }   

            }else{
                Alert.alert("No tiene Internet","Intete mas tarde sincronizar")
            }



           




        } catch(e) {
          console.log(e);
          // error reading value
        }
        setIsLoading(false);

    }
    

    return (
        <View style={{backgroundColor: "#fff"}}>
            <Loading isVisible={isLoading} text="Guardando Datos" />
            <View  style= {styles.body}>
            <View style={{marginTop:10,borderColor:'#D7DFE6',borderWidth:0.4}}>
                <Text style={{marginVertical:10,marginStart:10,fontWeight:'bold'}}>
                    Pendientes por Sincronizar : {totalSolicitud}
                </Text>
            </View>
            <View 
                style={{alignItems:'center',marginVertical:20,borderWidth:0.4,borderColor:'#D7DFE6',paddingVertical:15}}>
                <ProgressCircle
                    percent={porcentaje}
                    radius={100}
                    borderWidth={30}
                    color="#3399FF"
                    shadowColor="#D7DFE6"
                    bgColor="#fff"
                >
                    <Text style={{ fontSize: 18 }}>{porcentaje} %</Text>
                </ProgressCircle>
            </View>
             <Button
                    icon={
                        <FontAwesome name="cloud-upload" size={24} color="#fff" />
                    }
                    title="  Sincronizar"
                    onPress={sincronizar}
                />
        </View>
        </View>    
    )
}

const styles = StyleSheet.create({
    body: {
        height: "auto",
        width: "90%",
        marginHorizontal:10,
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center',
        backgroundColor: "#fff"
    },
    btnWithIcon:{
        display:'flex',
        flexDirection:'row',
        borderRadius:5
    }
});