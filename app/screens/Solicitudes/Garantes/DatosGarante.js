import React from 'react'
import { StyleSheet, Text, View , TouchableOpacity } from 'react-native'
import { Icon} from 'react-native-elements';
import  call  from 'react-native-phone-call'
import {styles}  from '../../../styles/Estilos';

export default function DatosGarante(props) {
    const {navigation, route} = props;
    return (
        <View >                
            <DatosSolicitud 
                route      = {route}
                navigation = {navigation}
            />      
        </View>
    )
}

function DatosSolicitud(props) {
    const {navigation, route} = props;
    const {id,code,deuGar,gcedula,gnombre,gestado,gdireccion,greferencia,
        gtelefono,destadoEdit,cod_verif,colorBtn,colorDeudor,
        estadoColorGarant,componDesacGarant,buttonFormGarant
    } = route.params;

    const makeCall = (number) => {
        const args = {
            number: number, 
            prompt: true 
        }
       call(args).catch(console.error)
   };

   const goFormulario = () => {
        navigation.navigate("formulario-garante", {
            id,code,deuGar,gcedula,gnombre,gestado,gdireccion,greferencia,
            gtelefono,destadoEdit,cod_verif,colorBtn,colorDeudor
        });         

    };
     
    return(
        <View style={{paddingTop:20}}>
             <View style={{ backgroundColor: estadoColorGarant,
                            width:'95%',alignSelf:'center',flexDirection:'row',
                            borderTopStartRadius:5, borderTopEndRadius:5}}>
                <Text style={styles.encabezadoTxt}>
                    # {code}
                </Text>
             </View>
             <View style={styles.view_tipo_cli}> 
                <Text style={styles.txt_tipo_cli} >
                     {deuGar} 
                </Text>
            </View>             
            <View style={styles.view_datos}>
                <View style={styles.txt_icon}>
                    <Icon name='user' type='font-awesome' />
                </View>        
                <Text style={styles.txt_atrib}>
                     {gnombre}  
                 </Text>                
             </View>
             <View style={styles.view_datos}>
                <View style={styles.txt_icon}>
                    <Icon name='id-card' type='font-awesome' />
                </View>        
                <Text style={styles.txt_atrib}>
                     {gcedula} 
                </Text>                
             </View>
             <View style={styles.txt_detalle}>
                <View style={styles.txt_det_deu}>
                    <Text style={styles.txt_det}>
                        Dirección:</Text>                  
                    <Text>
                        {gdireccion.toString()}                       
                    </Text>
                    <Text style={styles.txt_det}>
                        Referencia:
                    </Text>                  
                    <Text>
                        {greferencia.toString()}                       
                    </Text>
                </View>   
             </View>
             <View style={styles.view_datos_call}>
                    <TouchableOpacity style={styles.btn_call} 
                        disabled={componDesacGarant}
                        onPress={()=> makeCall(gtelefono.toString())}>
                        <View >
                            <Icon name='phone' type='font-awesome' color='#fff'/>
                            <Text style={{textAlign:'center',color:'#fff'}}>Llamar</Text>
                        </View> 
                    </TouchableOpacity>
                <Text style={styles.txt_telf}>
                    {gtelefono.toString()} 
                </Text>    
             </View>
             <View style={styles.view_btn_form}>
                    <TouchableOpacity 
                        disabled={componDesacGarant}
                        style={{backgroundColor: buttonFormGarant,
                                        width:'95%',height:70,borderRadius:10,alignContent:'center',justifyContent:'center'}}
                                onPress={goFormulario} > 
                            <Icon name='edit' type='font-awesome' color='#ffff'/>  
                            <Text style={styles.txt_llenar_form}>
                                    LLENAR FORMULARIO
                            </Text>                   
                    </TouchableOpacity>  
             </View>
        </View>
    )
}
