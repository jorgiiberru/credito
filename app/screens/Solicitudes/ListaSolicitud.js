import React, { useState, useCallback, useEffect } from 'react';
import {Text, View, FlatList,  TouchableOpacity, ToastAndroid} from 'react-native';
import {useNavigation} from "@react-navigation/native";
import { Icon } from 'react-native-elements';
import {color_fin,color_pen,color_syn,color_desa,color_form} from '../../styles/Colores'
import {stylesList}  from '../../styles/Estilos';

export default function ListaSolicitud(props) {    
    const {solicitudes} = props;
    const navigation = useNavigation();
     return (
        <View>
              <FlatList
                    data={solicitudes}
                    renderItem={(solicitud) => 
                        <GetSolicitudes
                            solicitud={solicitud}
                            navigation={navigation}
                        />}
                    keyExtractor={(item, index) => index.toString()}
                /> 
        </View>
    )
}


function GetSolicitudes(props) {
    const {solicitud,navigation} = props;
    const {destado,gestado} = solicitud.item;

    let estadoColorDeudor = color_pen;
    let estadoColorGarant = color_pen;

    let buttonFormDeudor = color_form;
    let buttonFormGarant = color_form;

    let componDesacDeudor = false;
    let componDesacGarant = false;

    if(destado=='IN'){
        estadoColorDeudor = color_pen;
        buttonFormDeudor  = color_form;
        componDesacDeudor = false;
    }
    if(destado=='SIN'){
        estadoColorDeudor = color_syn;
        buttonFormDeudor  = color_desa;
        componDesacDeudor = true;

    }
    if(destado=='FI'){
        estadoColorDeudor = color_fin;
        buttonFormDeudor  = color_desa;
        componDesacDeudor = true;

    }   
    if(gestado=='IN'){
        estadoColorGarant = color_pen;
        buttonFormGarant  = color_form;
        componDesacGarant = false;

    }
    if(gestado=='SIN'){
        estadoColorGarant = color_syn;
        buttonFormGarant  = color_desa;
        componDesacGarant = true;

    }
    if(gestado=='FI'){
        estadoColorGarant = color_fin;
        buttonFormGarant  = color_desa;
        componDesacGarant = true;

    } 
    return (
        <View>
            <CardSolicitudes 
                solicitud           ={solicitud}
                navigation          ={navigation}
                estadoColorDeudor   ={estadoColorDeudor}
                estadoColorGarant   ={estadoColorGarant}
                componDesacDeudor   ={componDesacDeudor}
                componDesacGarant   ={componDesacGarant}
                buttonFormDeudor    ={buttonFormDeudor}
                buttonFormGarant    ={buttonFormGarant}
            /> 
        </View>                      
    )
}


function CardSolicitudes(props){
    const   {   solicitud,navigation,
                estadoColorDeudor,componDesacDeudor,buttonFormDeudor,
                estadoColorGarant,componDesacGarant,buttonFormGarant
            } = props;

    const {dcedula,dnombre,destado,ddireccion,dreferencia,dtelefono,code,id,cod_verif,
           gcedula,gnombre,gestado,gdireccion,greferencia,gtelefono,agencia} = solicitud.item;

    const visualizar =  (tipoDocumento) => {  
        navigation.navigate("visualizar",{tipoDocumento,code});  
 
    } 
    const verDeudor = () => {
        const deuGar = 'DEUDOR';    
        navigation.navigate("datos-deudor", {
            code,deuGar,dcedula,dnombre,destado,ddireccion,dreferencia,dtelefono,cod_verif,
            estadoColorDeudor,componDesacDeudor,buttonFormDeudor,id
        });
     };

     const verGarante = () => {
        const deuGar = 'GARANTE';
        navigation.navigate("datosGarante", {
        code,deuGar,gcedula,gnombre,gestado,gdireccion,greferencia,gtelefono,cod_verif,
        estadoColorGarant,componDesacGarant,buttonFormGarant,id
    });  
};
     return(
         <View style={{paddingTop:10}}>
             <View style={{ backgroundColor:estadoColorDeudor,
                            width:'95%',alignSelf:'center',flexDirection:'row',
                            borderTopStartRadius:5,borderTopEndRadius:5}}>
                <Text style={stylesList.txt_code}>
                        # {code.toString()} 
                </Text>
                <Text style={stylesList.txt_agencia}>
                        {agencia}
                </Text>                
             </View>
             <View style={stylesList.view_description}> 
                <Text style={stylesList.txt_nombre} >
                        {dnombre.toString()}
                </Text>
                <Text style={{ fontSize: 11 }}>
                        {ddireccion.toString()}</Text>
             </View>

            <View style={stylesList.view_buttons}>
                <View style={stylesList.view_btn}>
                    <TouchableOpacity  onPress={verDeudor}>
                        <Icon   color={estadoColorDeudor} 
                                name='user' type='font-awesome' size={18} />
                        <Text  style={stylesList.txt_btn}>Deudor</Text>
                    </TouchableOpacity>
                </View> 
                {gcedula && 
                    <View style={stylesList.view_btn}>
                        <TouchableOpacity  onPress={verGarante}>
                            <Icon   color={estadoColorGarant} 
                                    name='users' type='font-awesome' size={18}/>
                            <Text  style={stylesList.txt_btn}>Garante</Text>
                        </TouchableOpacity>
                    </View> 
                } 
                <View style={stylesList.view_btn}>
                    <TouchableOpacity  onPress={()=>visualizar(30)}>
                        <Icon name='map' type='font-awesome' size={17}/>
                        <Text  style={stylesList.txt_btn}>Croquis</Text>
                    </TouchableOpacity>
                </View> 
                <View style={stylesList.view_btn}>
                    <TouchableOpacity  onPress={()=>visualizar(1)}>
                        <Icon name='id-card' type='font-awesome' size={19}/>
                        <Text  style={stylesList.txt_btn}>Cédula</Text>
                    </TouchableOpacity>
                </View> 

                                                        
            </View>
         </View>
     )
}


