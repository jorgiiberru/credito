import React, { useState, useCallback ,useEffect} from 'react';
import { StyleSheet, Text, View,  TouchableOpacity} from 'react-native';
import ListaSolicitud from './ListaSolicitud'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ScrollView } from 'react-native';
import {Ionicons, SimpleLineIcons   } from '@expo/vector-icons';
import { Button } from "react-native-elements";

import { StatusBar } from 'expo-status-bar';
import {  Menu, Divider, Provider } from 'react-native-paper';
import {URL_CARGA_SOLICITUD} from '../../api/Storage'
import {useNavigation} from "@react-navigation/native";
import Loading from "../../components/Loading";
import Modal from '../../components/Modal';
import { ToastAndroid } from 'react-native';
import SolicitudesApi from '../../api/SolicitudesApi';
import SolicitudAsyncStorage from '../../utils/storage/SolicitudAsyncStorage';
import ProbarConexionNet from '../../components/ProbarConexionNet';
import { Alert } from 'react-native';
import LocalStorageUser from '../../api/LocalStorageUser';

export default function Solicitud({navigation}) {
    const navigation2 = useNavigation();
    const [store, setStore] = useState([]); 
    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);
    const soliTempGuard = [];
    const [isLoading, setIsLoading] = useState(false);
    const [showModal, setShowModal] = useState(false);



    useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {      
        (async () => {        
          user = { id: ''}
          let user_logueado = await LocalStorageUser.getDatosUser('@usuario_logueado');
          let dato = user_logueado;
          user.id =dato.scode;
          await fetch(URL_CARGA_SOLICITUD, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                        'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  cod_verif       :  user.id ,
              })
          }).then((respuesta)=>respuesta.json())
            .then( async ( responseJson)=>{
  
              const consulta =  JSON.stringify(responseJson);            
              const respaldoSolicitudes = await AsyncStorage.getItem('@gh_solicitudes');
                if(respaldoSolicitudes!=null){
                  let resp = JSON.parse(respaldoSolicitudes);
                  console.log("Id Guardados");
                  for(let i = 0; i < resp.length;i++){
                    if(resp[i].destado=='SIN' || resp[i].gestado=='SIN'){
                      console.log(resp[i].id+"- deudor: "+resp[i].destado +" - garante: "+ resp[i].gestado);
                      soliTempGuard.push(resp[i]);
                    }
                  } 
                }  
  
              if(consulta=='[]' || consulta=='undefined'){
                ToastAndroid.show("No hay data nueva", 1000);
              }            
              console.log("Id Descargados");
              const jsonValue = JSON.stringify(responseJson);
              await AsyncStorage.setItem('@gh_solicitudes', jsonValue);
              const dataGet = await AsyncStorage.getItem('@gh_solicitudes')
              const datos = JSON.parse(dataGet);
              
              for(let i = 0; i < datos.length;i++){
                console.log(datos[i].id+"-"+datos[i].destado);
                for(let j = 0; j < soliTempGuard.length;j++){
                   if(soliTempGuard[j].id==datos[i].id){
                      datos[i]=soliTempGuard[j];
                      console.log("Son iguales" + j +"-"+i);
                   }
                }
              } 
             const datosMerge = JSON.stringify(datos);
             await AsyncStorage.setItem('@gh_solicitudes', datosMerge); 
             const dataFinal = JSON.parse(await AsyncStorage.getItem('@gh_solicitudes'));
  
             console.log("Id Descargados");
              for(let i = 0; i < datos.length;i++){
                console.log(datos[i].id+"- deudor: "+datos[i].destado +" - garante: "+ datos[i].gestado); 
              }
              setStore(dataFinal); 
              setIsLoading(false);         
              }).catch((error)=>{
                ToastAndroid.show("Ha surgido un error", 1000);
            }); 
        })();
        setVisible(false);       
      });
      return unsubscribe;
    }, [navigation]);

    const syncSolicitud=()=>{
      setVisible(false); 
      navigation2.navigate("sync-solicitudes");
    }
    
    const salir=()=>{
        setVisible(false);
        setShowModal(true);
    }  
    const regresar=()=>{
      setVisible(false);
      setShowModal(false);
      navigation2.navigate("menu-principal");
    }    
    const cerrarSesion = ()=>{
        (async () => {         
           const store_deudor = await AsyncStorage.getItem('@gh_solicitudes');
           const dataStore = JSON.parse(store_deudor);           
           let cont = 0;
           if(store_deudor!=null){
              for (let l = 0; l < dataStore.length; l++) {
                if(dataStore[l].destado == 'SIN' || dataStore[l].gestado == 'SIN' ){
                    cont = cont + 1;
                   
                }
              }
           }
           console.log(cont);
           if(cont==0){
              await AsyncStorage.removeItem('@usuario_logueado');
              await AsyncStorage.removeItem('@gh_solicitud_descargas');
              await AsyncStorage.removeItem('@gh_solicitudes');          
              setShowModal(false);            
              navigation2.navigate("login");
           }else{
            setShowModal(false);
            alert("No puede cerrar sesión, sin antes sincronizar");            
            navigation2.navigate("sync-solicitudes");           
          }

         })();
    } 

    const cancel=()=>{
        setVisible(false);
        setShowModal(false);
    }
 
    const listarSolicitudes=()=>{
      setVisible(false); 
      navigation2.navigate("list-solicitudes");
    }

    const cargar = async() => {
      setVisible(false); 
      const solicitudes = await SolicitudesApi.get('@usuario_logueado');
      if(solicitudes =='[]' || solicitudes=='undefined'){
          console.log("Me cargo con lo descargado")
          ToastAndroid.show("No hay data nueva", 1000);
      }else{
        let burn = false;
        if(burn){
            console.log("Estoy lleno");
            console.log("me descargo , me comparo y me cargo")
        }else{           
            await SolicitudAsyncStorage.setStoreSolicitud(solicitudes);
            const dataFinal = JSON.parse(await SolicitudAsyncStorage.get());
         /*    console.log(dataFinal) */
            setStore(dataFinal);
            console.log("Me cargue con lo descargado")
        }
        
      /*   const respaldo = await SolicitudAsyncStorage.respaldar('@gh_solicitudes');
        console.log(respaldo); */
       /*  if(respaldo!=null){
           console.log("Ando con Dato " + respaldo);
        }else{
         /*  const dataFinal = await SolicitudAsyncStorage.setStoreSolicitud('@gh_solicitudes',solicitudes);
          console.log(dataFinal);
          console.log("Estoy vacio,debo ser la data descargado"); }*/
               
      }
    }
                
    let user = {id:''}       
    const cargarDatos = async()=>{
      let  estadoConexion = await ProbarConexionNet.conexion();      
      if(estadoConexion){        
        (async () => {                    
          user = { id: ''}
          let user_logueado = await LocalStorageUser.getDatosUser('@usuario_logueado');
          let dato = user_logueado;
          user.id =dato.scode;
          setIsLoading(true);
          await fetch(URL_CARGA_SOLICITUD, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                        'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  cod_verif       :  user.id ,
              })
          }).then((respuesta)=>respuesta.json())
            .then( async ( responseJson)=>{

              const consulta =  JSON.stringify(responseJson);            
              const respaldoSolicitudes = await AsyncStorage.getItem('@gh_solicitudes');
                if(respaldoSolicitudes!=null){
                  let resp = JSON.parse(respaldoSolicitudes);
                  console.log("Id Guardados");
                  for(let i = 0; i < resp.length;i++){
                    if(resp[i].destado=='SIN' || resp[i].gestado=='SIN'){
                      console.log(resp[i].id+"- deudor: "+resp[i].destado +" - garante: "+ resp[i].gestado);
                      soliTempGuard.push(resp[i]);
                    }
                  } 
                }  

              if(consulta=='[]' || consulta=='undefined'){
                ToastAndroid.show("No hay data nueva", 1000);
              }            
              console.log("Id Descargados");
              const jsonValue = JSON.stringify(responseJson);
              await AsyncStorage.setItem('@gh_solicitudes', jsonValue);
              const dataGet = await AsyncStorage.getItem('@gh_solicitudes')
              const datos = JSON.parse(dataGet);
              
              for(let i = 0; i < datos.length;i++){
                console.log(datos[i].id+"-"+datos[i].destado);
                for(let j = 0; j < soliTempGuard.length;j++){
                  if(soliTempGuard[j].id==datos[i].id){
                      datos[i]=soliTempGuard[j];
                      console.log("Son iguales" + j +"-"+i);
                  }
                }
              } 
            const datosMerge = JSON.stringify(datos);
            await AsyncStorage.setItem('@gh_solicitudes', datosMerge); 
            const dataFinal = JSON.parse(await AsyncStorage.getItem('@gh_solicitudes'));

            console.log("Id Descargados");
              for(let i = 0; i < datos.length;i++){
                console.log(datos[i].id+"- deudor: "+datos[i].destado +" - garante: "+ datos[i].gestado); 
              }
              setStore(dataFinal); 
                      
              }).catch((error)=>{
                ToastAndroid.show("Ha surgido un error", 1000);
            });                      
            setIsLoading(false); 
        })();
      }else{
        Alert.alert("No tiene internet","¡Intente mas tarde!");        
      }
        setVisible(false);   
    };
    console.log ('Hola')

    return (
      <Provider>
        <View>
            <Text> </Text>
            <StatusBar barStyle = "dark-content" hidden = {false} 
            backgroundColor = "#e42320" translucent = {true}/>
        </View>

        <Modal 
                isVisible={showModal}
                setIsVisible={setShowModal}
            >
                <View style={{height:130}}>
                    <Text style={{fontWeight:'bold',paddingTop:5,paddingBottom:10}}>¿Desea Cerrar sesión?</Text> 
                    <Text style={{paddingTop:2,paddingBottom:30}}>
                        Revise si tiene datos sin Sincronizar</Text> 
                    <View style={{flexDirection:'row-reverse'}}>
                        <View style={{width:'30%'}}>
                            <Button
                                title="Salir"
                                onPress={cerrarSesion}
                                buttonStyle={{backgroundColor:'#E62E42'}}
                            />
                        </View>
                        <View style={{width:'5%'}}>
                        </View>
                        <View style={{width:'30%'}}>
                            <Button
                                title="Cancelar"
                                onPress={cancel}
                            />
                        </View>                       
                    </View>                   
                </View>
            </Modal>
  
        <View style={{ flexDirection: 'row'}}>
          <View style={styles.boxStackOne} > 
            <TouchableOpacity onPress={regresar}>
                <Ionicons name="arrow-back" size={24} color="white"  />
            </TouchableOpacity>        
          </View>          
          <View style={styles.boxStack} >
            <Text style={styles.textStack}> 
              SOLICITUDES PENDIENTES 
            </Text>
          </View>
        <View style={styles.boxStack2} >              
          <Menu
              visible={visible}
              onDismiss={closeMenu}
              anchor={
                    <TouchableOpacity onPress={openMenu}>
                      <SimpleLineIcons 
                        name="options-vertical" size={18} 
                        color="#ffff" 
                      />
                  </TouchableOpacity>
                }>
                <Menu.Item icon="cellphone-arrow-down" onPress={cargarDatos} title="Recargar Solicitudes" />
                <Menu.Item icon="autorenew" onPress={syncSolicitud} title="Sincronizar Solicitudes" />
                <Menu.Item icon="account-multiple-check" onPress={listarSolicitudes} title="Solicitudes Realizadas" />
                <Divider />
                <Menu.Item icon="exit-to-app"  onPress={salir} title="Salir" />
          </Menu>    
        </View>
      </View>
            <ScrollView>              
              {store  ? 
                <ListaSolicitud
                  solicitudes={store}
                /> 
              :null}
            </ScrollView>
            <Loading isVisible={isLoading} text="Cargando Nuevas solicitudes" />
      </Provider>   
    )
}











const styles = StyleSheet.create({
  boxStack: { width: '70%', height: 60, backgroundColor: '#e42320', justifyContent: 'center' },
  boxStackOne: { width: '15%', height: 60, backgroundColor: '#e42320', 
  justifyContent: 'center',alignItems:'center' },

  textStack: {fontSize: 22, textAlign: 'center', color: '#fff',fontSize:14,fontWeight:'bold'},
  boxStack2:{ width: '15%', height: 60, backgroundColor: '#e42320', 
  justifyContent: 'center',alignSelf:'center' },
  btnIcon:{ backgroundColor:'#e42320',height:50,width:200,}
});
