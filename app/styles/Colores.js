
export const color_pen = '#0061a7';
export const color_syn = '#FF5733';
export const color_fin = '#239B56';

export const color_desa= '#B3BAC4';

export const color_form = '#148CB2';

export const color_azul = '#0061a7';
export const color_rojo = '#e42320';
export const color_blanco = '#fff';


export const color_fondo = '#F6F8FB';
export const color_icon     = '#2B3A4F';
export const color_confirm = '#07B639';

export const color_fon_foto = '#171E27';

