
import { StyleSheet } from 'react-native'
import { color_azul, color_blanco, color_fondo, color_fon_foto, color_form, color_rojo } from './Colores';




export const card_style = StyleSheet.create({
    card_header_one:{ 
        backgroundColor:color_azul,width:'95%',
        alignSelf:'center',flexDirection:'row',
        borderTopStartRadius:5,borderTopEndRadius:5
    },
    card_header_two:{ 
        backgroundColor:color_form,width:'95%',
        alignSelf:'center',flexDirection:'row',
        borderTopStartRadius:5,borderTopEndRadius:5
    },    
    card_text_one:{
        width:'100%',alignSelf:'flex-start',color:'#fff',
        fontWeight:'bold',paddingVertical:5,paddingLeft:10
    },
    card_body_search:{
        backgroundColor:color_blanco,width:'95%', alignSelf:'center',
        paddingVertical:15,flexDirection:'row'
    },
    card_body_search_cmb:{
        backgroundColor:color_blanco,width:'95%', alignSelf:'center',
        
    }

   
});

export const styles = StyleSheet.create({
    txt_llenar_form:{ 
        marginHorizontal:5 ,
        fontSize:15,
        fontWeight:'bold',
        color:'#ffff',
        textAlign:'center'
    },
    encabezadoCard:{
        backgroundColor: '#239B56',
        width:'95%',
        alignSelf:'center',
        flexDirection:'row',
        borderTopStartRadius:5,
        borderTopEndRadius:5
    },
    encabezadoTxt:{
        width:'100%',
        alignSelf:'flex-start',
        color:'#fff',
        fontWeight:'bold',
        paddingVertical:5,
        paddingLeft:10            
    },
    view_tipo_cli:{
        backgroundColor:'#fff',
        width:'95%', 
        alignSelf:'center',
        paddingVertical:4,
        paddingLeft:10,
        borderBottomWidth:1,
        borderBottomColor:'#B3BAC4'
    },
    txt_tipo_cli:{ 
        fontSize: 15, 
        fontWeight: 'bold',
        textAlign:'center'
    },
    view_datos:{
        backgroundColor:'#fff',
        width:'95%',
        alignSelf:'center',
        flexDirection:'row'   
    },
    txt_icon:{
        width:'10%',
        paddingLeft:10,
        alignSelf:'center',
        color:'#fff',
        fontWeight:'bold',
        paddingVertical:5,
        backgroundColor:'#fff'

    },
    txt_atrib:{
        width:'90%',
        color:'#000',
        paddingVertical:5,
        paddingLeft:10
    },
    txt_det:{
        fontWeight:'bold',
        paddingTop:15 
    },
    txt_det_deu:{
        width:'100%',
        alignSelf:'center',
        color:'#fff',
        fontWeight:'bold',
        paddingLeft:10
    },
    txt_detalle:{
        backgroundColor:'#fff',
        width:'95%',
        alignSelf:'center',
        flexDirection:'row',
        borderTopWidth:1,
        borderColor:'#B3BAC4'
    },
    view_datos_call:{
        backgroundColor:'#fff',
        width:'95%',
        alignSelf:'center',
        flexDirection:'row',
        paddingVertical:10,
        paddingLeft:10,
        borderTopWidth:1,
        borderColor:'#B3BAC4'       
    },
    btn_call :{
        width:'25%',
        alignSelf:'center',
        color:'#fff',
        fontWeight:'bold',
        paddingVertical:5,
        backgroundColor:'#1BB214',
        borderRadius:5,
    },
    txt_telf:{
        width:'70%',
        color:'#000',
        paddingLeft:20,
        fontWeight:'bold',
        fontSize:22,
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center'
    },
    view_btn_form:{
        backgroundColor:'#fff',
        width:'95%',
        alignSelf:'center',
        flexDirection:'row',
        paddingVertical:10,
        borderTopWidth:1,
        borderColor:'#B3BAC4',
        justifyContent:'center'
    },    
})

export const stylesList = StyleSheet.create({
    txt_code:{
        width:'50%',alignSelf:'flex-start',color:'#fff',
        fontWeight:'bold',paddingVertical:2,paddingLeft:10
    },
    txt_agencia:{
        width:'50%',textAlign:'right',color:'#fff',
        fontWeight:'bold',paddingVertical:2,paddingRight:10
    },
    view_div_prin:{
        backgroundColor:'#fff',width:'95%', alignSelf:'center',
        paddingVertical:4,paddingLeft:10,flexDirection:'row'
    },
    view_div_one:{
        backgroundColor:'#fff',width:'80%',
        paddingVertical:4
    },
    view_div_two:{
        backgroundColor:'#fff',width:'20%',
        paddingVertical:4
    },
    view_description:{
        backgroundColor:'#fff',width:'95%', alignSelf:'center',
        paddingVertical:4,paddingLeft:10
    },
    view_description_two:{
        backgroundColor:'#fff',width:'95%', alignSelf:'center',
        paddingVertical:4,paddingLeft:10,borderBottomEndRadius:5,
        borderBottomStartRadius:5
    },
    view_buttons:{
        backgroundColor:'#fff',width:'95%',
        alignSelf:'center',flexDirection:'row-reverse',
        borderTopWidth:0.5,borderTopColor:'#B3BAC4',
    },
    view_buttons2:{
        backgroundColor:'#fff',width:'95%',
        alignSelf:'center',flexDirection:'row-reverse',
        borderTopWidth:0.5,borderTopColor:'#B3BAC4',paddingTop:20
    },
    view_btn:{
        backgroundColor:'#fff',width:'20%',alignSelf:'flex-start',
        color:'#fff', fontWeight:'bold',paddingVertical:2
    },
    txt_btn:{
        textAlign:'center',fontSize:10
    },
    txt_nombre:{ 
        fontSize: 13, fontWeight: 'bold'
    },
    view_izq_75:{
        backgroundColor:'#fff',width:'75%',alignSelf:'center',
        color:'#fff', fontWeight:'bold',paddingVertical:2
    },    
    fab: {
        position: 'absolute',
        margin: 20,
        right: 0,
        bottom: 0,
        backgroundColor: color_rojo,
        width:55,
        height:55,
        borderRadius:55,
        justifyContent:'center',
        alignItems:'center'
    },  
    btn_float_map:{
        position: 'absolute',
        margin: 20,
        right: 0,
        bottom: 0,
        backgroundColor: color_azul,
        width:'90%',
        height:40,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'  
    },
    view_celda_25:{
        width:'25%',flexDirection:'column'
    },
    view_celda_30:{
        width:'30%',flexDirection:'column'
    },
    view_celda_35:{
        width:'35%',flexDirection:'column'
    },
    view_celda_20:{
        width:'20%',flexDirection:'column'
    },
    view_celda_40:{
        width:'40%',flexDirection:'column'
    },
    view_celda_50:{
        width:'50%',flexDirection:'column'
    },
    view_celda_60:{
        width:'60%',flexDirection:'column'
    },
    txt_celda:{
       textAlign:'center',fontWeight:'bold',borderBottomWidth:0.5,borderColor:'#B3BAC4'
    }
})


export const stylesRetiro = StyleSheet.create({
    fondo_foto:{
        backgroundColor:color_fon_foto,
        width:'95%', 
        alignSelf:'center',
        borderBottomColor:'#B3BAC4'
    },
    view_tom_foto:{
        backgroundColor:color_form,
        width:'95%',
        alignSelf:'center',
        flexDirection:'row',
        justifyContent:'center',
        height:50,
        alignItems:'center'},
    txt_tom_foto:{ textAlign:'center',fontSize:15,fontWeight:'bold',color:color_blanco}
});


export const stylesAreaFoto = StyleSheet.create({
    safecontainer: {
        flex: 1,
        justifyContent: 'center',
      }, 
      containerBtn:{
        flex:1,backgroundColor:'#120D0D',flexDirection: 'row',height:80,alignItems:'center',justifyContent: 'center',
      },
      containerBtn2:{
        flex:1,backgroundColor:'transparent',flexDirection: 'row',height:80,alignItems:'center',justifyContent: 'center',
      },  
      containerBtn3:{
        flex:1,backgroundColor:'transparent',flexDirection: 'row',
        width:'50%',height:80,alignItems:'center',justifyContent: 'center',
      },       
      
      btnCamera:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#898989',
        margin:5,
        borderRadius:70,
        height:70,
        width:70
      },
      btnClose:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#F2162F',
        margin:5,
        borderRadius:60,
        height:60,
        width:60
      },
      btnUndo:{
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#C2C4C1',
        margin:5,
        borderRadius:60,
        height:60,
        width:60
      },      
    inputAndroid: {
        fontSize: 18,
        borderColor: '#000',
        borderWidth:1,

        color: 'black',
      },
    blockButton: {
        marginLeft:20,
        marginRight:20,       
        display : 'flex',
        alignItems : 'center',
        paddingVertical: 14,
        backgroundColor : '#EE5A24',
        borderRadius : 10,
        elevation :2,
        shadowColor : '#000',
        shadowOffset:{
            width:2,
            height:2,
        },
        shadowOpacity: 0.25,
        shadowRadius :3.5,    
    },    
    buttonText :{
        color :'#fff',
        fontWeight:'bold',
        fontSize: 20
    },

    containerCard: {
        borderRadius: 20,
        borderWidth: 0,        
    },
    headerCard: {
        borderWidth: 0,        
        flex: 1,
        alignItems: 'center',
        paddingTop: 2,
        backgroundColor: '#0061a7',
    },    
    cardContent:{
        marginTop: 10,
        marginLeft:20,
        marginRight:20,
      },
      subheading: {
        fontSize: 15,    
        fontWeight: '900',
        color: '#fff',
        fontWeight: 'bold'
      },
      cardHeader: {
        backgroundColor: '#0061a7',
        
      },
      footer: {
        width: '100%',
        paddingVertical: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      contenedor: {
        flex: 1,
        flexDirection: 'row',
        
      },
      textCmb:{
        paddingLeft:20,
        fontWeight:'bold',
      },
      inputCmb:{
          paddingStart:30
        },

})
