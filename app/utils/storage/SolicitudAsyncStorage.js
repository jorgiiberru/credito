import AsyncStorage from "@react-native-async-storage/async-storage";
const SolicitudAsyncStorage = {  
    get: () => {
        return ( async () => {
            return  AsyncStorage.getItem('@gh_solicitudes');
        })();     
    },
    respaldar: key => {         
        return ( async () => {
            const respaldoSolicitudes = await JSON.parse(AsyncStorage.getItem(key));   
            let result;
            console.log(respaldoSolicitudes);
            if(respaldoSolicitudes!=null){
                return result = respaldo;
            }else{
                return result = null;
            }
        })();        
    },
    setStoreSolicitud: (value) => {
        return  AsyncStorage.setItem('@gh_solicitudes', value);
    },

}

export default SolicitudAsyncStorage;