-- --------------------------------------------------------
-- Host:                         157.245.219.93
-- Versión del servidor:         8.0.21 - Source distribution
-- SO del servidor:              Linux
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para test_gpi
CREATE DATABASE IF NOT EXISTS `test_gpi` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `test_gpi`;

-- Volcando estructura para tabla test_gpi.agencia
CREATE TABLE IF NOT EXISTS `agencia` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `ciudad` smallint unsigned DEFAULT NULL,
  `tipo` enum('V','B','M') NOT NULL DEFAULT 'V',
  `direccion` varchar(300) NOT NULL,
  `codigo` varchar(3) DEFAULT NULL,
  `nombre` varchar(30) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `es_bodega` tinyint(1) NOT NULL DEFAULT '0',
  `scode` smallint unsigned NOT NULL DEFAULT '0',
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_AG001` (`ciudad`),
  CONSTRAINT `FK_AG001` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.aud_cli_calif
CREATE TABLE IF NOT EXISTS `aud_cli_calif` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cliente` int DEFAULT NULL,
  `calificacion` char(3) DEFAULT NULL,
  `concepto` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.banco_cta
CREATE TABLE IF NOT EXISTS `banco_cta` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `banco` int unsigned NOT NULL,
  `tipo` enum('A','C','T') NOT NULL DEFAULT 'C',
  `numero` varchar(20) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `saldo` double NOT NULL DEFAULT '0',
  `ctrl_cheques` tinyint(1) NOT NULL DEFAULT '0',
  `ini_chk` int DEFAULT NULL,
  `fin_chk` int DEFAULT NULL,
  `no_actual` int DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` int DEFAULT NULL,
  `f_actu` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BANK_CUENTA_NUMERO` (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.banco_deposito
CREATE TABLE IF NOT EXISTS `banco_deposito` (
  `id` int NOT NULL AUTO_INCREMENT,
  `agencia` smallint unsigned NOT NULL,
  `cajero` mediumint unsigned NOT NULL,
  `cuenta` smallint unsigned DEFAULT NULL,
  `fecha` date NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `monto` double NOT NULL,
  `etd` enum('IN','VL','AN','UT') NOT NULL DEFAULT 'IN',
  `etc` enum('IN','CT') NOT NULL DEFAULT 'IN',
  `obs` varchar(255) DEFAULT NULL,
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_BANCO_DEPOSITO_AGENCIA` (`agencia`),
  CONSTRAINT `FK_BANCO_DEPOSITO_AGENCIA` FOREIGN KEY (`agencia`) REFERENCES `agencia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.banco_tc
CREATE TABLE IF NOT EXISTS `banco_tc` (
  `id` int NOT NULL AUTO_INCREMENT,
  `banco` int unsigned NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `saldo` double NOT NULL DEFAULT '0',
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  `u_actu` int DEFAULT NULL,
  `f_actu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.barrio
CREATE TABLE IF NOT EXISTS `barrio` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `zona` mediumint unsigned DEFAULT NULL,
  `ciudad` smallint unsigned DEFAULT NULL,
  `nombre` varchar(60) NOT NULL,
  `u_actu` mediumint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_BARRIO_ZONA` (`zona`),
  KEY `FK_BARRIO_CIU` (`ciudad`),
  CONSTRAINT `FK_BARRIO_CIU` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`id`),
  CONSTRAINT `FK_BARRIO_ZONA` FOREIGN KEY (`zona`) REFERENCES `zona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `linea` smallint unsigned DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `padre` smallint unsigned DEFAULT NULL,
  `nivel` tinyint unsigned NOT NULL DEFAULT '1',
  `pme` tinyint unsigned NOT NULL DEFAULT '6',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` smallint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CTG_PADRE` (`padre`),
  KEY `FK_CTG_LINEA` (`linea`),
  CONSTRAINT `FK_CTG_LINEA` FOREIGN KEY (`linea`) REFERENCES `linea` (`id`),
  CONSTRAINT `FK_CTG_PADRE_SELF` FOREIGN KEY (`padre`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_banco_audit
CREATE TABLE IF NOT EXISTS `cb_banco_audit` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `query_sql` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `result` text,
  `query` text,
  `ip` varchar(20) DEFAULT NULL,
  `path` varchar(150) NOT NULL DEFAULT '',
  `etd` tinyint NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_billeton
CREATE TABLE IF NOT EXISTS `cb_billeton` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cliente` int unsigned NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` double NOT NULL DEFAULT '0',
  `venta` int unsigned DEFAULT NULL,
  `endoso` int unsigned DEFAULT NULL,
  `f_endoso` date DEFAULT NULL,
  `etd` enum('VG','UT','CD') NOT NULL DEFAULT 'VG',
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_calendario
CREATE TABLE IF NOT EXISTS `cb_calendario` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `gracia` tinyint(1) NOT NULL DEFAULT '0',
  `u_crea` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `obs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_cobros
CREATE TABLE IF NOT EXISTS `cb_cobros` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pagare` int unsigned NOT NULL,
  `f_pago` tinyint unsigned NOT NULL DEFAULT '1',
  `fecha` date NOT NULL,
  `monto` double NOT NULL,
  `m_condona` double NOT NULL DEFAULT '0',
  `multa` double NOT NULL DEFAULT '0',
  `banco` int DEFAULT NULL,
  `cuenta` int DEFAULT NULL,
  `tarjeta` int DEFAULT NULL,
  `plazo` smallint unsigned DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '0',
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  `obs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PAGAREDET_PAGARE` (`pagare`),
  CONSTRAINT `FK_PAGAREDET_PAGARE` FOREIGN KEY (`pagare`) REFERENCES `cb_pagare` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_detalle_orden_retiro
CREATE TABLE IF NOT EXISTS `cb_detalle_orden_retiro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `orden_retiro` int unsigned NOT NULL,
  `producto` int NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `serie` int NOT NULL,
  `kardex` int NOT NULL,
  `estado` enum('NOAP','IN','RE','NORET','REOTRO') NOT NULL DEFAULT 'NOAP',
  PRIMARY KEY (`id`),
  KEY `orden_retiro` (`orden_retiro`),
  CONSTRAINT `FK_ORDEN_RETIRO` FOREIGN KEY (`orden_retiro`) REFERENCES `cb_orden_retiro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_equifax
CREATE TABLE IF NOT EXISTS `cb_equifax` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tid` char(1) NOT NULL DEFAULT 'C',
  `sujeto` int unsigned DEFAULT NULL,
  `nid` varchar(20) NOT NULL DEFAULT '',
  `dets` json DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_crea` int unsigned DEFAULT NULL,
  `agencia` smallint unsigned DEFAULT NULL,
  `score` mediumint DEFAULT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `celular` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `nota` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18064 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_equifax_log
CREATE TABLE IF NOT EXISTS `cb_equifax_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tid` char(1) NOT NULL DEFAULT 'C',
  `sujeto` int unsigned DEFAULT NULL,
  `nid` varchar(20) NOT NULL DEFAULT '',
  `equifax` int unsigned NOT NULL DEFAULT '0',
  `empleado` mediumint unsigned DEFAULT NULL,
  `agencia` smallint unsigned DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `celular` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_EQUIFAX_LOG_EQX` (`equifax`),
  KEY `FK_EQUIFAX_LOG_EMPLEADO` (`empleado`),
  CONSTRAINT `FK_EQUIFAX_LOG_EMPLEADO` FOREIGN KEY (`empleado`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK_EQUIFAX_LOG_EQX` FOREIGN KEY (`equifax`) REFERENCES `cb_equifax` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29136 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_novedades
CREATE TABLE IF NOT EXISTS `cb_novedades` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cliente` int unsigned NOT NULL COMMENT 'id sujeto de gestion, original',
  `respon` mediumint unsigned NOT NULL,
  `agencia` smallint unsigned NOT NULL,
  `tipo` smallint unsigned NOT NULL,
  `gtipo` enum('RET','REF','COB','PAG','DEV') NOT NULL DEFAULT 'PAG',
  `fecha` datetime NOT NULL,
  `f_promesa` date DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `monto` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `t_mobi` enum('MOBILE','NO_MOBILE','XX') NOT NULL DEFAULT 'XX',
  `t_cli` enum('DEUDOR','GARANTE') DEFAULT 'DEUDOR',
  `ets` enum('IN','ER','SN') NOT NULL COMMENT 'IN',
  `obs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_oficios
CREATE TABLE IF NOT EXISTS `cb_oficios` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('COMERCIO_GENERAL','INDUSTRIA_MANUFACTURERA','AGRICULTURA','OFICIOS_MENORES','COMERCIO_MENOR','MINERIAN','CONSTRUCCION','TRANSPORTE','NO_APLICA') NOT NULL DEFAULT 'COMERCIO_GENERAL',
  `nombre` varchar(80) NOT NULL DEFAULT '',
  `ingreso` smallint unsigned NOT NULL DEFAULT '0',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_oficios_COPIA
CREATE TABLE IF NOT EXISTS `cb_oficios_COPIA` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('CMR','IMA','AGR','OFM','CME','MNR','CNS','TRN','NA') NOT NULL DEFAULT 'CMR',
  `nombre` varchar(80) NOT NULL DEFAULT '',
  `ingreso` smallint unsigned NOT NULL DEFAULT '0',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_orden_retiro
CREATE TABLE IF NOT EXISTS `cb_orden_retiro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `factura_id` int unsigned DEFAULT NULL,
  `nombre_cliente` varchar(255) NOT NULL DEFAULT 'k',
  `calific` varchar(11) NOT NULL DEFAULT 'k',
  `agencia` varchar(255) NOT NULL DEFAULT 'k',
  `zona` varchar(255) NOT NULL DEFAULT 'k',
  `direccion` varchar(255) NOT NULL DEFAULT 'k',
  `fecha_emision` datetime DEFAULT NULL,
  `fecha_aprobacion_cartera` datetime DEFAULT NULL,
  `observacion` text,
  `etd_cartera` enum('P','A','R') NOT NULL DEFAULT 'P',
  `observacion_cartera` varchar(255) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `factura_id` (`factura_id`),
  CONSTRAINT `FK_VENTA` FOREIGN KEY (`factura_id`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_pagare
CREATE TABLE IF NOT EXISTS `cb_pagare` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('CD','EF','ND') NOT NULL DEFAULT 'CD',
  `venta` int unsigned NOT NULL,
  `cliente` int unsigned DEFAULT NULL,
  `numero` tinyint unsigned NOT NULL DEFAULT '1',
  `fecha` date NOT NULL,
  `monto` double NOT NULL DEFAULT '0',
  `multa` double NOT NULL DEFAULT '0',
  `abono` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `d_atraso` smallint NOT NULL DEFAULT '0',
  `etd` tinyint(1) NOT NULL DEFAULT '0',
  `f_upago` datetime DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PAGARE` (`venta`,`numero`),
  KEY `IDX_PAGARE` (`venta`),
  KEY `FK_PAGARE_CLIENTE` (`cliente`),
  CONSTRAINT `FK_PAGARE_CLIENTE` FOREIGN KEY (`cliente`) REFERENCES `sujeto` (`id`),
  CONSTRAINT `FK_PAGARE_VENTA` FOREIGN KEY (`venta`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_paleta
CREATE TABLE IF NOT EXISTS `cb_paleta` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('RET','REF','COB','PAG','DEV') NOT NULL DEFAULT 'PAG',
  `dato` enum('FPV','FAV','FPP','DOC','DYV','MDF','MDE','SIN') NOT NULL DEFAULT 'FPV',
  `nombre` varchar(80) DEFAULT NULL,
  `scode` smallint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_plan_trabajo
CREATE TABLE IF NOT EXISTS `cb_plan_trabajo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cli_id` int NOT NULL,
  `cli_ced` varchar(13) NOT NULL DEFAULT '',
  `cli_nomb` varchar(300) NOT NULL DEFAULT '',
  `cli_dir` varchar(200) NOT NULL DEFAULT '',
  `cli_lat` double NOT NULL DEFAULT '0',
  `cli_log` double NOT NULL DEFAULT '0',
  `emp_id` int NOT NULL,
  `emp_code` int NOT NULL,
  `age_id` int NOT NULL,
  `age_nomb` varchar(100) NOT NULL DEFAULT '',
  `saldo` double NOT NULL DEFAULT '0',
  `estado` enum('IN','FI') DEFAULT NULL,
  `fecha_visita` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_produbanco
CREATE TABLE IF NOT EXISTS `cb_produbanco` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nid` varchar(13) NOT NULL DEFAULT '',
  `valor` float DEFAULT '0',
  `id_item` int unsigned DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `frev` datetime DEFAULT NULL,
  `det` varchar(120) DEFAULT NULL,
  `verificado` char(2) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_region
CREATE TABLE IF NOT EXISTS `cb_region` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `ciudades` varchar(100) DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_retiro
CREATE TABLE IF NOT EXISTS `cb_retiro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `kardex_id` int unsigned NOT NULL,
  `tipo` enum('TEMPORAL','DEFINITIVO') NOT NULL DEFAULT 'TEMPORAL',
  `fecha_emision` datetime NOT NULL,
  `valoracion` double NOT NULL DEFAULT '100',
  `observacion` text,
  `nombre_cliente` varchar(255) DEFAULT NULL,
  `agencia` varchar(11) DEFAULT NULL,
  `factura_id` int unsigned NOT NULL,
  `imagen` text,
  `estado` tinyint NOT NULL DEFAULT '1',
  `estado_cartera` enum('P','A','R') NOT NULL DEFAULT 'P',
  PRIMARY KEY (`id`),
  KEY `FK_cb_retiro_kardex` (`kardex_id`),
  KEY `FK_cb_retiro_venta` (`factura_id`),
  CONSTRAINT `FK_cb_retiro_kardex` FOREIGN KEY (`kardex_id`) REFERENCES `kardex` (`id`),
  CONSTRAINT `FK_cb_retiro_venta` FOREIGN KEY (`factura_id`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_rxa
CREATE TABLE IF NOT EXISTS `cb_rxa` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agencia` smallint unsigned NOT NULL,
  `zona` mediumint unsigned NOT NULL,
  `ciudad` smallint unsigned NOT NULL,
  `recaudador` mediumint unsigned NOT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_ZONA_AGENCIA` (`agencia`,`zona`),
  KEY `FK_RXA_EMPLEADO` (`recaudador`),
  CONSTRAINT `FK_RXA_AGENCIA` FOREIGN KEY (`agencia`) REFERENCES `agencia` (`id`),
  CONSTRAINT `FK_RXA_EMPLEADO` FOREIGN KEY (`recaudador`) REFERENCES `empleado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_rxz
CREATE TABLE IF NOT EXISTS `cb_rxz` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `zona` mediumint unsigned NOT NULL,
  `ciudad` smallint unsigned NOT NULL,
  `recaudador` mediumint unsigned NOT NULL,
  `tipo_recaudador` enum('R','V') NOT NULL DEFAULT 'R',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` date DEFAULT NULL,
  `f_actu` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zona_recaudador_tipo_recaudador` (`zona`,`recaudador`,`tipo_recaudador`),
  KEY `recaudador` (`recaudador`),
  CONSTRAINT `FK_RXZ_EMPLEADO` FOREIGN KEY (`recaudador`) REFERENCES `empleado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_sbaco
CREATE TABLE IF NOT EXISTS `cb_sbaco` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tid` char(1) NOT NULL DEFAULT 'C',
  `sujeto` int unsigned DEFAULT NULL,
  `nid` char(10) NOT NULL DEFAULT '',
  `dets` json DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_crea` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nid` (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=6996 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cb_sbaco_log
CREATE TABLE IF NOT EXISTS `cb_sbaco_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tid` char(1) NOT NULL DEFAULT 'C',
  `sujeto` int unsigned DEFAULT NULL,
  `nid` char(10) NOT NULL DEFAULT '',
  `sbaco` int unsigned NOT NULL DEFAULT '0',
  `empleado` mediumint unsigned DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_SBACO_LOG_EQX` (`sbaco`),
  KEY `FK_SBACO_LOG_EMPLEADO` (`empleado`),
  CONSTRAINT `FK_SBACO_LOG_EMPLEADO` FOREIGN KEY (`empleado`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK_SBACO_LOG_EQX` FOREIGN KEY (`sbaco`) REFERENCES `cb_sbaco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10663 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ciudad
CREATE TABLE IF NOT EXISTS `ciudad` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `padre` smallint unsigned DEFAULT NULL,
  `nivel` tinyint unsigned NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `etd` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CIU_CODIGO` (`codigo`),
  KEY `IDX_CIU_PADRE` (`padre`),
  CONSTRAINT `FK_CIU_PADRE_SELF` FOREIGN KEY (`padre`) REFERENCES `ciudad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.cliente_salarios
CREATE TABLE IF NOT EXISTS `cliente_salarios` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `estado` int unsigned NOT NULL DEFAULT '1',
  `salario` double NOT NULL DEFAULT '0',
  `laboral_id` int NOT NULL,
  `f_crea` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.compra
CREATE TABLE IF NOT EXISTS `compra` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('CM','CG','ES','GS') NOT NULL DEFAULT 'CM',
  `tdoc` enum('FE','GR','MN') NOT NULL DEFAULT 'FE',
  `idf` int unsigned DEFAULT NULL,
  `pedido` int unsigned DEFAULT NULL,
  `agencia` smallint unsigned NOT NULL DEFAULT '1',
  `proveedor` int unsigned NOT NULL,
  `nro` varchar(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `st_0` double NOT NULL DEFAULT '0',
  `st_iva` double NOT NULL DEFAULT '0',
  `ice` double NOT NULL DEFAULT '0',
  `iva` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `etd` enum('IN','KD') NOT NULL DEFAULT 'IN',
  `etc` enum('IN','CT','XC','ER') NOT NULL DEFAULT 'IN',
  `plz_pago` date DEFAULT NULL,
  `p_fiscal` date DEFAULT NULL,
  `t_ret_renta` double NOT NULL DEFAULT '0',
  `t_ret_iva` double NOT NULL DEFAULT '0',
  `t_ret_isd` double NOT NULL DEFAULT '0',
  `t_con_ret` double NOT NULL DEFAULT '0',
  `aut_ret` varchar(49) DEFAULT NULL,
  `no_ret` varchar(20) DEFAULT NULL,
  `obs` varchar(255) DEFAULT NULL,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_reg` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_COMPRA_AGENCIA` (`agencia`),
  KEY `IDX_COMPRA_PROVEEDOR` (`proveedor`),
  CONSTRAINT `FK_COMPRA_PROVEEDOR` FOREIGN KEY (`proveedor`) REFERENCES `sujeto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.compra_det
CREATE TABLE IF NOT EXISTS `compra_det` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `idf` int unsigned DEFAULT NULL,
  `bos` enum('B','S') NOT NULL DEFAULT 'B',
  `compra` int unsigned NOT NULL DEFAULT '0',
  `producto` int unsigned DEFAULT NULL,
  `codigoPrincipal` varchar(25) DEFAULT NULL,
  `cantidad` double NOT NULL DEFAULT '0',
  `precioUnitario` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `valor` double NOT NULL DEFAULT '0',
  `codigo` char(2) DEFAULT NULL,
  `tarifa` double NOT NULL DEFAULT '0',
  `baseImponible` double NOT NULL DEFAULT '0',
  `codigoPorcentaje` char(5) DEFAULT NULL,
  `ice` double NOT NULL DEFAULT '0',
  `descripcion` varchar(250) NOT NULL DEFAULT '',
  `ptsi` double NOT NULL DEFAULT '0',
  `ajuste` double NOT NULL DEFAULT '0',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iadi` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_COMPRA_DET` (`compra`),
  CONSTRAINT `FK_COMPRA_DET_COMPRA` FOREIGN KEY (`compra`) REFERENCES `compra` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.compra_retencion
CREATE TABLE IF NOT EXISTS `compra_retencion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `compra` int unsigned NOT NULL DEFAULT '0',
  `proveedor` int unsigned DEFAULT NULL,
  `nro` char(17) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `t_ret_renta` double NOT NULL DEFAULT '0',
  `t_ret_iva` double NOT NULL DEFAULT '0',
  `cod_s` char(4) NOT NULL DEFAULT '01',
  `fac` varchar(20) DEFAULT NULL,
  `etd` enum('IN','OK','XW','ER') NOT NULL DEFAULT 'IN',
  `ets` char(1) NOT NULL DEFAULT 'I',
  `cax` char(49) DEFAULT NULL,
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dets` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_COMPRA_RETENCION_NRO` (`nro`),
  UNIQUE KEY `UNQ_COMPRA_RETENCION_CAX` (`cax`),
  KEY `FK_COMPRA_RETENCION_COMPRA` (`compra`),
  CONSTRAINT `FK_COMPRA_RETENCION_COMPRA` FOREIGN KEY (`compra`) REFERENCES `compra` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.copy_wb_compras
CREATE TABLE IF NOT EXISTS `copy_wb_compras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('ef','tc') NOT NULL DEFAULT 'ef',
  `email` varchar(80) NOT NULL DEFAULT '',
  `celular` char(10) NOT NULL DEFAULT '',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_ati` datetime DEFAULT NULL,
  `deta` json DEFAULT NULL,
  `detalle_pago` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_libro
CREATE TABLE IF NOT EXISTS `ct_libro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tmovi` smallint unsigned DEFAULT NULL,
  `fecha` date NOT NULL,
  `idd` int unsigned DEFAULT NULL,
  `descripcion` varchar(255) NOT NULL,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_LIBRO_TMOVI` (`tmovi`),
  CONSTRAINT `FK_LIBRO_TMOVI` FOREIGN KEY (`tmovi`) REFERENCES `ct_tmovi` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_libro_det
CREATE TABLE IF NOT EXISTS `ct_libro_det` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `libro` int unsigned DEFAULT NULL,
  `cuenta` mediumint unsigned DEFAULT NULL,
  `tbaux` enum('N','S','B','P','A') NOT NULL DEFAULT 'N',
  `idaux` int unsigned DEFAULT NULL,
  `debe` double NOT NULL DEFAULT '0',
  `haber` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_LIBRODET_LIBRE` (`libro`),
  KEY `IDX_LIBRODET_CUENTA` (`cuenta`),
  CONSTRAINT `FK_LBDET_LB` FOREIGN KEY (`libro`) REFERENCES `ct_libro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_pcuentas
CREATE TABLE IF NOT EXISTS `ct_pcuentas` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `empresa` tinyint unsigned DEFAULT '1',
  `padre` mediumint unsigned DEFAULT NULL,
  `nombre` varchar(100) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `nivel` tinyint unsigned NOT NULL,
  `movi` tinyint unsigned NOT NULL,
  `grupo` tinyint unsigned NOT NULL,
  `tbaux` enum('N','S','B','P','A') NOT NULL DEFAULT 'N',
  `saldo` double NOT NULL DEFAULT '0',
  `saldob` double NOT NULL DEFAULT '0',
  `u_crea` mediumint unsigned DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PCUENTAS_CODIGO` (`codigo`),
  KEY `IDX_PCUENTAS_EMPRESA` (`padre`),
  CONSTRAINT `FK_PCUENTAS004` FOREIGN KEY (`padre`) REFERENCES `ct_pcuentas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=585 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_periodos
CREATE TABLE IF NOT EXISTS `ct_periodos` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `codigo` mediumint unsigned NOT NULL,
  `f_ini` date NOT NULL,
  `f_fin` date NOT NULL,
  `etd` enum('IN','AB','CE') NOT NULL DEFAULT 'IN',
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PERIODO_CODIGO` (`codigo`),
  UNIQUE KEY `f_ini` (`f_ini`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_saldos
CREATE TABLE IF NOT EXISTS `ct_saldos` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cuenta` mediumint unsigned DEFAULT NULL,
  `tbaux` enum('N','S','B','P','A') NOT NULL DEFAULT 'N',
  `idaux` int unsigned DEFAULT NULL,
  `fecha` date NOT NULL,
  `debe` double NOT NULL DEFAULT '0',
  `haber` double NOT NULL DEFAULT '0',
  `saldo` double NOT NULL DEFAULT '0',
  `deudor` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_SALDOS_FECHA` (`fecha`),
  KEY `FK_SALDOS_CUENTA` (`cuenta`),
  CONSTRAINT `FK_SALDOS_CUENTA` FOREIGN KEY (`cuenta`) REFERENCES `ct_pcuentas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_saldos_ini
CREATE TABLE IF NOT EXISTS `ct_saldos_ini` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cuenta` mediumint unsigned DEFAULT NULL,
  `tbaux` enum('N','S','B','P','A') NOT NULL DEFAULT 'N',
  `idaux` int unsigned DEFAULT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `monto` double NOT NULL,
  `cierre` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_SALDOSINI_CUENTA` (`cuenta`),
  CONSTRAINT `FK_SALDOSINI_CUENTA` FOREIGN KEY (`cuenta`) REFERENCES `ct_pcuentas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ct_tmovi
CREATE TABLE IF NOT EXISTS `ct_tmovi` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `det` json DEFAULT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.empleado
CREATE TABLE IF NOT EXISTS `empleado` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `sujeto` int unsigned DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `tid` char(1) DEFAULT NULL,
  `cedula` varchar(15) DEFAULT NULL,
  `cuenta` varchar(15) DEFAULT NULL,
  `cargas` tinyint unsigned DEFAULT '0',
  `agencia` int DEFAULT '0',
  `ingreso` date DEFAULT NULL,
  `salida` date DEFAULT NULL,
  `tipo` tinyint unsigned NOT NULL DEFAULT '1',
  `roles` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `sueldo` float NOT NULL DEFAULT '0',
  `mas` json NOT NULL,
  `password` varchar(120) NOT NULL,
  `uvisita` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CNF_EMPLEADO` (`username`),
  UNIQUE KEY `UNIQ_EMPLEADO_CEDULA` (`cedula`)
) ENGINE=InnoDB AUTO_INCREMENT=1113 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.empresa
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `razon` varchar(300) NOT NULL,
  `ruc` varchar(13) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `contabilidad` char(2) NOT NULL DEFAULT 'SI',
  `cespecial` varchar(5) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `contador` varchar(120) DEFAULT NULL,
  `rep_reserva` varchar(120) DEFAULT NULL,
  `rep_legal` varchar(120) DEFAULT NULL,
  `clave_firma` varchar(100) DEFAULT NULL,
  `expira_firma` datetime DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_EMPRESA_IDENTIFICACION` (`ruc`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_01
CREATE TABLE IF NOT EXISTS `fe_01` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `idc` int unsigned NOT NULL DEFAULT '0',
  `agencia` smallint unsigned NOT NULL DEFAULT '1',
  `caja` smallint unsigned NOT NULL DEFAULT '1',
  `tipo` enum('COMPRA','GASTO','ACTIVO','NO_DEFINIDO') NOT NULL DEFAULT 'NO_DEFINIDO',
  `loga` enum('SI','NO','PA') NOT NULL DEFAULT 'NO',
  `etd` enum('IN','DT','AP','VF') NOT NULL DEFAULT 'IN',
  `ets` enum('IN','SN','ER') NOT NULL DEFAULT 'IN',
  `proveedor` int unsigned DEFAULT NULL,
  `eruc` char(13) NOT NULL DEFAULT '',
  `erazon` varchar(250) NOT NULL DEFAULT '',
  `cid` char(13) NOT NULL DEFAULT '',
  `nro` char(17) DEFAULT NULL,
  `cax` char(49) NOT NULL DEFAULT '',
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `ice` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgasto` smallint unsigned DEFAULT NULL,
  `xml` json DEFAULT NULL,
  `f_pago` tinyint unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_FE_FACTURA_CAX` (`cax`)
) ENGINE=InnoDB AUTO_INCREMENT=13279 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_01_det
CREATE TABLE IF NOT EXISTS `fe_01_det` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `idf` int unsigned NOT NULL DEFAULT '0',
  `producto` int unsigned DEFAULT NULL,
  `codigoPrincipal` varchar(25) DEFAULT NULL,
  `cantidad` double NOT NULL DEFAULT '0',
  `precioUnitario` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `codigo` char(2) DEFAULT NULL,
  `valor` double NOT NULL DEFAULT '0',
  `tarifa` double NOT NULL DEFAULT '0',
  `baseImponible` double NOT NULL DEFAULT '0',
  `ptsi` double NOT NULL DEFAULT '0',
  `codigoPorcentaje` char(2) DEFAULT NULL,
  `ice` double NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `etd` enum('I','C','P') NOT NULL DEFAULT 'I',
  `etl` enum('I','K','P') NOT NULL DEFAULT 'I',
  `cax` char(49) NOT NULL DEFAULT '',
  `serie` enum('S','N','M','C','D','R') DEFAULT NULL,
  `bos` enum('B','S') DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `obs` varchar(255) DEFAULT NULL,
  `iadi` json DEFAULT NULL,
  `codigoAuxiliar` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_FE01_DET_FACTURA` (`idf`),
  CONSTRAINT `FK_FE01_DET_FACTURA` FOREIGN KEY (`idf`) REFERENCES `fe_01` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2402 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_04
CREATE TABLE IF NOT EXISTS `fe_04` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idc` int unsigned NOT NULL DEFAULT '0',
  `agencia` smallint unsigned NOT NULL DEFAULT '1',
  `eruc` char(13) NOT NULL DEFAULT '',
  `proveedor` int unsigned DEFAULT NULL,
  `erazon` varchar(250) NOT NULL DEFAULT '',
  `cid` char(13) NOT NULL DEFAULT '',
  `nro` char(17) DEFAULT NULL,
  `cax` char(49) NOT NULL DEFAULT '',
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `fac` varchar(20) DEFAULT NULL,
  `cod_s` char(4) DEFAULT NULL,
  `etd` enum('IN','OK','PK','XW','ER','EE') NOT NULL DEFAULT 'IN',
  `ets` char(1) NOT NULL DEFAULT 'I',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `loga` enum('SI','NO','PA') NOT NULL DEFAULT 'NO',
  `tipo` enum('COMPRA','GASTO','ACTIVO','NO_DEFINIDO') NOT NULL DEFAULT 'NO_DEFINIDO',
  `xml` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_FE_NCREDITO_CAX` (`cax`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_06
CREATE TABLE IF NOT EXISTS `fe_06` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idc` int NOT NULL DEFAULT '0',
  `proveedor` mediumint unsigned DEFAULT NULL,
  `eruc` char(13) NOT NULL DEFAULT '',
  `erazon` varchar(250) NOT NULL DEFAULT '',
  `cid` char(13) NOT NULL DEFAULT '',
  `nro` char(17) DEFAULT NULL,
  `cax` char(49) NOT NULL DEFAULT '',
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `total` double NOT NULL DEFAULT '0',
  `fac` varchar(20) DEFAULT NULL,
  `cod_s` char(4) DEFAULT NULL,
  `fpf` date DEFAULT NULL,
  `etd` enum('IN','DT','AP','VF') NOT NULL DEFAULT 'IN',
  `ets` enum('IN','SN','ER') NOT NULL DEFAULT 'IN',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fcont` datetime DEFAULT NULL,
  `fapp` datetime DEFAULT NULL,
  `loga` enum('SI','NO','PA') NOT NULL DEFAULT 'NO',
  `tipo` enum('COMPRA','CONSIGNACION','GASTO','ACTIVO','NO_DEFINIDO') NOT NULL DEFAULT 'NO_DEFINIDO',
  `xml` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_FE_GUIA_CAX` (`cax`),
  KEY `IDX_FE_GUIA_CAX` (`cax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_07
CREATE TABLE IF NOT EXISTS `fe_07` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idc` int NOT NULL DEFAULT '0',
  `agencia` smallint unsigned NOT NULL DEFAULT '1',
  `eruc` char(13) NOT NULL DEFAULT '',
  `proveedor` int unsigned DEFAULT NULL,
  `erazon` varchar(250) NOT NULL DEFAULT '',
  `cid` char(13) NOT NULL DEFAULT '',
  `nro` char(17) DEFAULT NULL,
  `cax` char(49) NOT NULL DEFAULT '',
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `total` double NOT NULL DEFAULT '0',
  `fac` varchar(20) DEFAULT NULL,
  `cod_s` char(4) DEFAULT NULL,
  `dpf` char(7) DEFAULT '',
  `fpf` date DEFAULT NULL,
  `etd` enum('IN','OK','PK','XW','ER','EE') NOT NULL DEFAULT 'IN',
  `ets` char(1) NOT NULL DEFAULT 'I',
  `cpp` char(1) NOT NULL DEFAULT '',
  `usc` varchar(20) DEFAULT NULL,
  `usp` varchar(20) DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fcont` datetime DEFAULT NULL,
  `fapp` datetime DEFAULT NULL,
  `scont` date DEFAULT NULL,
  `sapp` date DEFAULT NULL,
  `loga` enum('SI','NO','PA') NOT NULL DEFAULT 'NO',
  `tipo` enum('COMPRA','GASTO','ACTIVO','NO_DEFINIDO') NOT NULL DEFAULT 'NO_DEFINIDO',
  `xml` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_FE_RETENCION_CAX` (`cax`),
  KEY `IDX_FE_RETENCION_CAX` (`cax`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.fe_carga
CREATE TABLE IF NOT EXISTS `fe_carga` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cod` enum('01','04','06','07') NOT NULL DEFAULT '01',
  `eruc` char(13) NOT NULL DEFAULT '',
  `erazon` varchar(250) NOT NULL DEFAULT '',
  `cid` char(13) NOT NULL DEFAULT '',
  `crazon` varchar(250) NOT NULL DEFAULT '',
  `nro` char(17) DEFAULT NULL,
  `cax` char(49) NOT NULL DEFAULT '',
  `nax` char(49) DEFAULT NULL,
  `fas` char(25) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `fac` char(17) DEFAULT NULL,
  `cod_s` char(4) DEFAULT NULL,
  `dpf` char(7) DEFAULT NULL,
  `etd` enum('LD','IN','OK','PK','XW','ER','EE','IP') NOT NULL DEFAULT 'LD',
  `etr` char(2) NOT NULL DEFAULT '00',
  `wer` tinyint unsigned DEFAULT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fupd` datetime DEFAULT NULL,
  `msj` varchar(250) DEFAULT NULL,
  `xml` json DEFAULT NULL,
  `ver` char(1) DEFAULT NULL,
  `dets` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_FE_CARGA_CAX` (`cax`),
  KEY `idx_cax` (`cax`)
) ENGINE=InnoDB AUTO_INCREMENT=8805 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_alicuota
CREATE TABLE IF NOT EXISTS `f_alicuota` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `num` tinyint NOT NULL DEFAULT '0',
  `valor` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `vori` double NOT NULL DEFAULT '0',
  `idalidesc` tinyint DEFAULT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_eme
CREATE TABLE IF NOT EXISTS `f_eme` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `n_ef` double NOT NULL,
  `n_tc` double NOT NULL,
  `n_cd` double NOT NULL,
  `e_ef` double NOT NULL,
  `e_tc` double NOT NULL,
  `e_cd` double NOT NULL,
  `p_ef` double NOT NULL,
  `p_tc` double NOT NULL,
  `p_cd` double NOT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  `scode` smallint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `EME_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_estado
CREATE TABLE IF NOT EXISTS `f_estado` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `code` char(2) NOT NULL DEFAULT '',
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `descuento` tinyint unsigned DEFAULT '0',
  `u_actu` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cnf_estado_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_forma_pago
CREATE TABLE IF NOT EXISTS `f_forma_pago` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `codigo` char(2) DEFAULT NULL,
  `nombre` varchar(120) NOT NULL,
  `sri` varchar(10) NOT NULL,
  `anticipo` enum('SI','NO') NOT NULL DEFAULT 'SI',
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  `gasto` enum('SI','NO') DEFAULT 'NO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_impuesto_producto
CREATE TABLE IF NOT EXISTS `f_impuesto_producto` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `sri` varchar(10) NOT NULL,
  `etd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_impuesto_retencion
CREATE TABLE IF NOT EXISTS `f_impuesto_retencion` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `sri` varchar(10) NOT NULL,
  `etd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_parametro
CREATE TABLE IF NOT EXISTS `f_parametro` (
  `codigo` char(5) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `valor` double NOT NULL DEFAULT '0',
  `tipo` char(2) NOT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_promocion
CREATE TABLE IF NOT EXISTS `f_promocion` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `p_uno` double NOT NULL DEFAULT '0',
  `p_adi` double NOT NULL DEFAULT '0',
  `m_ini` tinyint unsigned NOT NULL DEFAULT '1',
  `m_fin` tinyint unsigned NOT NULL DEFAULT '1',
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_rentabilidad
CREATE TABLE IF NOT EXISTS `f_rentabilidad` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  `porce` tinyint unsigned NOT NULL DEFAULT '1',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `EME_RENTA_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_tarifa_producto
CREATE TABLE IF NOT EXISTS `f_tarifa_producto` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `impuesto` smallint unsigned DEFAULT NULL,
  `nombre` varchar(500) NOT NULL,
  `sri` varchar(10) NOT NULL,
  `tarifa` double NOT NULL,
  `etd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CNF_TRF_PRD` (`impuesto`),
  CONSTRAINT `FK_CNF_TRF_PRD` FOREIGN KEY (`impuesto`) REFERENCES `f_impuesto_producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_tarifa_retencion
CREATE TABLE IF NOT EXISTS `f_tarifa_retencion` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `impuesto` smallint unsigned DEFAULT NULL,
  `nombre` varchar(500) NOT NULL,
  `sri` varchar(10) NOT NULL,
  `porcentaje` varchar(10) NOT NULL,
  `etd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_TARIFA_RETENCION` (`impuesto`),
  CONSTRAINT `FK_TARIFA_RETE` FOREIGN KEY (`impuesto`) REFERENCES `f_impuesto_retencion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.f_tipos
CREATE TABLE IF NOT EXISTS `f_tipos` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `grupo` enum('EDU','LAB','TRA','VIV','CON','SEG','OCU','TEZ','PLZ','COL','ND') NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto
CREATE TABLE IF NOT EXISTS `gasto` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tdoc` enum('FE','MN') NOT NULL DEFAULT 'FE',
  `idf` int unsigned DEFAULT NULL,
  `agencia` smallint unsigned NOT NULL DEFAULT '1',
  `proveedor` int unsigned NOT NULL,
  `tgasto` smallint unsigned NOT NULL,
  `fecha` date NOT NULL,
  `base` double NOT NULL DEFAULT '0',
  `st_0` double NOT NULL DEFAULT '0',
  `st_iva` double NOT NULL DEFAULT '0',
  `ice` double NOT NULL DEFAULT '0',
  `iva` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `t_ori` double unsigned NOT NULL,
  `docu` enum('FACTURA','NOTA_VENTA','VALE') NOT NULL DEFAULT 'FACTURA',
  `nro` char(17) NOT NULL DEFAULT '',
  `motivo` smallint NOT NULL DEFAULT '0',
  `deducible` enum('SI','NO') DEFAULT 'SI',
  `f_pago` tinyint unsigned DEFAULT '0',
  `etd` enum('IN','DT') NOT NULL DEFAULT 'IN',
  `etc` enum('IN','CT','XC','ER') NOT NULL DEFAULT 'IN',
  `rateo` int NOT NULL DEFAULT '0',
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_reg` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `det` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CP_GASTO_AGENCIA` (`agencia`),
  KEY `IDX_CP_GASTO_PROVEEDOR` (`proveedor`),
  CONSTRAINT `FK_CP_GASTO_PROVEEDOR` FOREIGN KEY (`proveedor`) REFERENCES `sujeto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto_motivo
CREATE TABLE IF NOT EXISTS `gasto_motivo` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL DEFAULT '',
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  `tipo` enum('NORMAL','VIAJE') NOT NULL DEFAULT 'NORMAL',
  `u_crea` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto_prorrateo
CREATE TABLE IF NOT EXISTS `gasto_prorrateo` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tdoc` enum('FE','MN') NOT NULL DEFAULT 'FE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto_proveedor
CREATE TABLE IF NOT EXISTS `gasto_proveedor` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `proveedor` int unsigned DEFAULT NULL,
  `gasto` int unsigned DEFAULT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PXGAS` (`proveedor`,`gasto`)
) ENGINE=InnoDB AUTO_INCREMENT=1021 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto_tipo
CREATE TABLE IF NOT EXISTS `gasto_tipo` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL DEFAULT '',
  `bos` enum('B','S') NOT NULL DEFAULT 'B',
  `tipo` enum('G','L','F','B','M') DEFAULT 'G',
  `aprueba` tinyint unsigned NOT NULL DEFAULT '0',
  `c_ivap` mediumint unsigned NOT NULL DEFAULT '0',
  `c_riva` mediumint unsigned NOT NULL DEFAULT '0',
  `c_rrenta` mediumint unsigned NOT NULL DEFAULT '0',
  `c_vta` mediumint unsigned NOT NULL DEFAULT '0',
  `c_prv` mediumint unsigned NOT NULL DEFAULT '0',
  `iva` smallint unsigned NOT NULL DEFAULT '0',
  `renta` smallint unsigned NOT NULL DEFAULT '0',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_crea` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gasto_xmotivo
CREATE TABLE IF NOT EXISTS `gasto_xmotivo` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `motivo` smallint unsigned DEFAULT NULL,
  `gasto` int unsigned DEFAULT NULL,
  `etd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_GAS_TIPO_MOTIVO` (`motivo`,`gasto`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gha_formulario
CREATE TABLE IF NOT EXISTS `gha_formulario` (
  `id` int NOT NULL AUTO_INCREMENT,
  `usuario_id` int DEFAULT NULL,
  `solicitud_id` int DEFAULT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fecha_verificacion` datetime NOT NULL,
  `latitud` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `longitud` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `foto_base` mediumblob,
  PRIMARY KEY (`id`),
  KEY `IDX_24D6FBDDB38439E` (`usuario_id`),
  KEY `IDX_24D6FBD1CB9D6E4` (`solicitud_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33200 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gha_solicitud
CREATE TABLE IF NOT EXISTS `gha_solicitud` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` int NOT NULL,
  `cod_verif` int DEFAULT NULL,
  `dnombre` varchar(150) NOT NULL,
  `dcedula` varchar(15) NOT NULL,
  `ddireccion` varchar(200) NOT NULL,
  `dreferencia` varchar(250) DEFAULT NULL,
  `dtelefono` varchar(50) NOT NULL,
  `gnombre` varchar(150) DEFAULT NULL,
  `gcedula` varchar(15) DEFAULT NULL,
  `gdireccion` varchar(200) DEFAULT NULL,
  `greferencia` varchar(250) DEFAULT NULL,
  `gtelefono` varchar(50) DEFAULT NULL,
  `destado` varchar(2) NOT NULL,
  `gestado` varchar(2) NOT NULL,
  `dfecha` datetime DEFAULT NULL,
  `gfecha` datetime DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cod_ven` int NOT NULL DEFAULT '0',
  `cod_agen` int NOT NULL DEFAULT '0',
  `etd` varchar(2) NOT NULL,
  `lista` char(12) DEFAULT NULL,
  `mapa` char(1) DEFAULT 'I',
  `fins` datetime DEFAULT NULL,
  `fupd` datetime DEFAULT NULL,
  `idsujeto` int DEFAULT NULL,
  `agencia` varchar(50) DEFAULT NULL,
  `zona` varchar(80) DEFAULT NULL,
  `eto` char(1) NOT NULL DEFAULT 'I',
  `idapno` int DEFAULT NULL,
  `fecapno` datetime DEFAULT NULL,
  `f_map` datetime DEFAULT NULL,
  `f_doc` datetime DEFAULT NULL,
  `f_val` datetime DEFAULT NULL,
  `iddel` int DEFAULT NULL,
  `f_del` datetime DEFAULT NULL,
  `u_map` int unsigned DEFAULT NULL,
  `u_doc` int unsigned DEFAULT NULL,
  `u_val` int unsigned DEFAULT NULL,
  `lapso` int unsigned DEFAULT NULL,
  `obs` text,
  `nota` text,
  `aud` enum('S','N') DEFAULT 'N',
  `dvgeo` enum('SI','NO','XX') NOT NULL DEFAULT 'XX',
  `gvgeo` enum('SI','NO','NA','XX') NOT NULL DEFAULT 'XX',
  `copia` enum('IN','AP','XX') NOT NULL DEFAULT 'XX',
  `ncode` int unsigned DEFAULT NULL,
  `dscore` mediumint DEFAULT NULL,
  `gscore` mediumint DEFAULT NULL,
  `etp` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `u_pre` int unsigned DEFAULT NULL,
  `f_pre` datetime DEFAULT NULL,
  `stp` enum('IN','OK','ER','XX') NOT NULL DEFAULT 'IN',
  `sbaco` int DEFAULT NULL,
  `dobservacion` varchar(400) DEFAULT NULL,
  `gobservacion` varchar(400) DEFAULT NULL,
  `ddireccion_cor` varchar(200) DEFAULT NULL,
  `gdireccion_cor` varchar(200) DEFAULT NULL,
  `dreferencia_cor` varchar(200) DEFAULT NULL,
  `greferencia_cor` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_VERIF` (`cod_verif`),
  KEY `IDX_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=30678 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gh_suser
CREATE TABLE IF NOT EXISTS `gh_suser` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(60) NOT NULL,
  `roles` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `idsujeto` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.gh_usuario
CREATE TABLE IF NOT EXISTS `gh_usuario` (
  `id` int NOT NULL,
  `nombre` varchar(120) DEFAULT NULL,
  `cod_cargo` int NOT NULL,
  `tipou` varchar(80) NOT NULL,
  `clave` varchar(120) NOT NULL,
  `cod_agen` int NOT NULL,
  `cod_jef_a` int DEFAULT NULL,
  `cod_zona` int DEFAULT NULL,
  `zona` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.kardex
CREATE TABLE IF NOT EXISTS `kardex` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tdoc` enum('F','G','M','L') NOT NULL DEFAULT 'F',
  `idf` int unsigned DEFAULT NULL,
  `producto` int unsigned DEFAULT NULL,
  `tipo` enum('CM','CG','ES') NOT NULL DEFAULT 'CM',
  `proveedor` mediumint unsigned DEFAULT NULL,
  `ubica` smallint unsigned DEFAULT '6',
  `ubicacion_logica` smallint unsigned DEFAULT '6',
  `tserie` enum('S','N','M','C','D','R') NOT NULL DEFAULT 'S',
  `serie` varchar(50) DEFAULT NULL,
  `estado` tinyint unsigned DEFAULT '1',
  `etl` enum('IN','CM','VL','VF') NOT NULL DEFAULT 'IN',
  `etv` enum('I','D','S','P','V') NOT NULL DEFAULT 'I',
  `etu` enum('B','A','T','C') NOT NULL DEFAULT 'A',
  `num` tinyint unsigned NOT NULL DEFAULT '1',
  `costo` double NOT NULL DEFAULT '0',
  `pvp` double NOT NULL DEFAULT '0',
  `venta` int unsigned DEFAULT NULL,
  `idg` int unsigned DEFAULT NULL,
  `idm` int unsigned DEFAULT NULL,
  `color` smallint unsigned NOT NULL DEFAULT '1',
  `f_reg` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `det` text,
  `mas` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_KARDEX_SERIE` (`serie`),
  KEY `IDX_KDX_PRD` (`producto`),
  KEY `IDX_KDX_FE` (`idf`),
  KEY `IDX_KDX_SERIE` (`serie`),
  CONSTRAINT `FK_KDX_PROD` FOREIGN KEY (`producto`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=896 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.kardexc
CREATE TABLE IF NOT EXISTS `kardexc` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `producto` int unsigned NOT NULL,
  `tipo` enum('IN','CM','VT') NOT NULL DEFAULT 'CM',
  `cantidad` smallint unsigned DEFAULT '0',
  `c_unitario` double NOT NULL,
  `c_total` double NOT NULL,
  `cantidad_saldo` double NOT NULL,
  `c_promedio` double NOT NULL,
  `c_total_saldo` double NOT NULL,
  `actual` tinyint unsigned NOT NULL DEFAULT '1',
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `obs` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_KDXC` (`producto`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.linea
CREATE TABLE IF NOT EXISTS `linea` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `pdr` double NOT NULL DEFAULT '30',
  `scode` smallint unsigned NOT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.marca
CREATE TABLE IF NOT EXISTS `marca` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `scode` smallint DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.motos
CREATE TABLE IF NOT EXISTS `motos` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `cod` char(2) NOT NULL DEFAULT '01',
  `cax` varchar(49) NOT NULL,
  `eruc` varchar(20) DEFAULT NULL,
  `erazon` varchar(80) DEFAULT NULL,
  `nro` varchar(20) DEFAULT NULL,
  `cid` varchar(20) DEFAULT NULL,
  `femi` date DEFAULT NULL,
  `xml` mediumtext,
  `CLASE` varchar(20) DEFAULT NULL,
  `SUBCLASE` varchar(20) DEFAULT NULL,
  `MARCA` varchar(20) DEFAULT NULL,
  `MODELO` varchar(20) DEFAULT NULL,
  `PESO` varchar(20) DEFAULT NULL,
  `CHASIS` varchar(20) DEFAULT NULL,
  `ANO` varchar(4) DEFAULT NULL,
  `COLOR1` varchar(20) DEFAULT NULL,
  `COLOR2` varchar(20) DEFAULT NULL,
  `MOTOR` varchar(20) DEFAULT NULL,
  `CILINDRAJE` varchar(20) DEFAULT NULL,
  `COMBUSTIBLE` varchar(20) DEFAULT NULL,
  `PASAJEROS` varchar(4) DEFAULT NULL,
  `CARROCERIA` varchar(20) DEFAULT NULL,
  `CPN` varchar(20) DEFAULT NULL,
  `TONELAJE` varchar(20) DEFAULT NULL,
  `CAP_CARGA` varchar(20) DEFAULT NULL,
  `PAIS` varchar(20) DEFAULT NULL,
  `etd` char(2) NOT NULL DEFAULT 'IN',
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `total` double DEFAULT '0',
  `base` double DEFAULT '0',
  `descuento` double DEFAULT '0',
  `dato` varchar(255) DEFAULT NULL,
  `n_repertorio` mediumint DEFAULT NULL,
  `f_inscripcion` date DEFAULT NULL,
  `n_inscripcion` mediumint DEFAULT NULL,
  `nid_comprador` varchar(15) DEFAULT NULL,
  `nom_comprador` varchar(100) DEFAULT NULL,
  `vendedor` varchar(50) DEFAULT '0791751543001:GRANHOGAR S.A GRAHOGSA',
  `placa` varchar(10) DEFAULT NULL,
  `f_emision` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cax_cod` (`cax`,`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=1189 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.novedad
CREATE TABLE IF NOT EXISTS `novedad` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tabla` varchar(20) NOT NULL,
  `dato` varchar(60) NOT NULL,
  `nuevo` int unsigned DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '0',
  `u_crea` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.pais
CREATE TABLE IF NOT EXISTS `pais` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.parroquia
CREATE TABLE IF NOT EXISTS `parroquia` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `ciudad` smallint unsigned DEFAULT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PARR_CODIGO` (`codigo`),
  KEY `FK_PARR_CIU` (`ciudad`),
  CONSTRAINT `FK_PARR_CIU` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1388 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.pedido
CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `proveedor` int unsigned NOT NULL,
  `detalle` json DEFAULT NULL,
  `etd` enum('I','P','T') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'I',
  `f_des` date DEFAULT NULL,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_reg` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_PEDIDO_PROVEEDOR` (`proveedor`),
  CONSTRAINT `FK_PEDIDO_PROVEEDOR` FOREIGN KEY (`proveedor`) REFERENCES `sujeto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.producto
CREATE TABLE IF NOT EXISTS `producto` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL,
  `scode` mediumint unsigned DEFAULT NULL,
  `marca` smallint unsigned DEFAULT NULL,
  `linea` smallint unsigned DEFAULT NULL,
  `categoria` smallint unsigned DEFAULT '0',
  `ivav` enum('S','N') NOT NULL DEFAULT 'S',
  `ivac` enum('S','N') NOT NULL DEFAULT 'S',
  `iva` smallint unsigned NOT NULL DEFAULT '2',
  `iva_c` smallint unsigned NOT NULL DEFAULT '2',
  `voc` enum('V','C','A','R','G','L','F') NOT NULL DEFAULT 'V',
  `bos` enum('B','S') NOT NULL DEFAULT 'B',
  `tps` tinyint unsigned NOT NULL DEFAULT '0',
  `c_prm` double NOT NULL DEFAULT '0',
  `c_ppc` double NOT NULL DEFAULT '0',
  `c_act` double NOT NULL DEFAULT '0',
  `pvp` double NOT NULL DEFAULT '0',
  `p_ret` tinyint unsigned DEFAULT '0',
  `eme` tinyint unsigned DEFAULT '0',
  `ats` char(5) DEFAULT NULL,
  `tv_cd` enum('S','N') DEFAULT 'S',
  `tv_tc` enum('S','N') DEFAULT 'S',
  `tv_ef` enum('S','N') DEFAULT 'S',
  `plz_min` tinyint unsigned DEFAULT '1',
  `plz_max` tinyint unsigned DEFAULT '24',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `serie` enum('S','N','M','C','D','R') DEFAULT 'S',
  `t_promo` enum('M','N','G','X') NOT NULL DEFAULT 'X',
  `ctg_promo` varchar(100) DEFAULT NULL,
  `cod_promo` varchar(150) DEFAULT NULL,
  `peso` double DEFAULT NULL,
  `minimo` mediumint unsigned NOT NULL DEFAULT '1',
  `stockf` mediumint unsigned NOT NULL DEFAULT '0',
  `stockd` mediumint unsigned NOT NULL DEFAULT '0',
  `stock_mach1` mediumint unsigned NOT NULL DEFAULT '0',
  `cuenta_vta` mediumint unsigned NOT NULL DEFAULT '0',
  `cuenta_adm` mediumint unsigned NOT NULL DEFAULT '0',
  `cuenta_fnz` mediumint unsigned NOT NULL DEFAULT '0',
  `finanza` tinyint unsigned NOT NULL DEFAULT '0',
  `proveedores` varchar(100) DEFAULT NULL,
  `renta` smallint unsigned NOT NULL DEFAULT '0',
  `piva` tinyint unsigned NOT NULL DEFAULT '12',
  `piva_c` tinyint unsigned NOT NULL DEFAULT '12',
  `obs` longtext,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `u_actu` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `w_img` tinyint unsigned NOT NULL DEFAULT '0',
  `w_det` json DEFAULT NULL,
  `jsaldo` json DEFAULT NULL,
  `is_ret` enum('SI','NO','XX') NOT NULL DEFAULT 'XX',
  `w_tpo` enum('PUBLICAR','NO_PUBLICAR','PROMO') NOT NULL DEFAULT 'NO_PUBLICAR',
  `url_video` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prd_codigo_unique` (`scode`),
  KEY `FK_PROD_IVA` (`iva`)
) ENGINE=InnoDB AUTO_INCREMENT=6912 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ptoemi
CREATE TABLE IF NOT EXISTS `ptoemi` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `agencia` smallint unsigned DEFAULT NULL,
  `es_fe` enum('SI','NO') NOT NULL DEFAULT 'SI',
  `estab` varchar(3) DEFAULT NULL,
  `codigo` varchar(3) NOT NULL,
  `fa_aut` char(10) DEFAULT NULL,
  `fa_ini` date DEFAULT NULL,
  `fa_cad` date DEFAULT NULL,
  `fa_seq` mediumint unsigned NOT NULL DEFAULT '0',
  `nc_aut` char(10) DEFAULT NULL,
  `nc_ini` date DEFAULT NULL,
  `nc_cad` date DEFAULT NULL,
  `nc_seq` mediumint unsigned NOT NULL DEFAULT '0',
  `re_aut` char(10) DEFAULT NULL,
  `re_ini` date DEFAULT NULL,
  `re_cad` date DEFAULT NULL,
  `re_seq` mediumint unsigned NOT NULL DEFAULT '0',
  `gu_aut` char(10) DEFAULT NULL,
  `gu_ini` date DEFAULT NULL,
  `gu_cad` date DEFAULT NULL,
  `gu_seq` mediumint unsigned NOT NULL DEFAULT '0',
  `nd_aut` char(10) DEFAULT NULL,
  `nd_ini` date DEFAULT NULL,
  `nd_cad` date DEFAULT NULL,
  `nd_seq` mediumint unsigned NOT NULL DEFAULT '0',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_PUNTO_COD` (`agencia`,`codigo`),
  KEY `IDX_PUNTO_AGENCIA` (`agencia`),
  CONSTRAINT `FK_PUNTO_AGE` FOREIGN KEY (`agencia`) REFERENCES `agencia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.pxp
CREATE TABLE IF NOT EXISTS `pxp` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `producto` int unsigned DEFAULT NULL,
  `proveedor` mediumint unsigned DEFAULT NULL,
  `kpi` varchar(30) DEFAULT NULL,
  `nombre` text,
  `nombrep` text,
  `serie` enum('S','N','M','C','D','R') DEFAULT NULL,
  `bos` enum('B','S') DEFAULT NULL,
  `f_uc` date DEFAULT NULL,
  `num_uc` smallint unsigned DEFAULT NULL,
  `costo_uc` double DEFAULT NULL,
  `ice_uc` double NOT NULL DEFAULT '0',
  `desc_uc` double NOT NULL DEFAULT '0',
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PROD_PRV_KPRI_UNQ` (`proveedor`,`kpi`),
  KEY `IDX_PRD_PRV_PRODUCTO` (`producto`),
  KEY `IDX_PRD_PRV_PROVEEDOR` (`proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.ref_refinanciar
CREATE TABLE IF NOT EXISTS `ref_refinanciar` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int unsigned NOT NULL,
  `porcentaje_abono_refinanciar` double NOT NULL,
  `abono_refinanciar` double NOT NULL,
  `saldo_refinanciar` double NOT NULL,
  `num_cuotas_refinanciar` int NOT NULL,
  `valor_cuotas_refinanciar` double NOT NULL DEFAULT '0',
  `total_refinanciar` double NOT NULL DEFAULT '0',
  `fecha_refinanciar` date NOT NULL,
  `observacion_refinanciar` text NOT NULL,
  `fecha_refinanciar_cartera` date DEFAULT NULL,
  `observacion_refinanciar_cartera` text,
  `etd_refinanciar_cartera` enum('P','A','R') DEFAULT 'P',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.re_detalle_retiro
CREATE TABLE IF NOT EXISTS `re_detalle_retiro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `orden_retiro` int unsigned NOT NULL,
  `producto` int DEFAULT NULL,
  `nombre` varchar(255) DEFAULT '',
  `serie` varchar(255) DEFAULT NULL,
  `kardex` int DEFAULT NULL,
  `etd_retiro` enum('RE','NORE','REOTRO') DEFAULT 'NORE',
  `imagen_frontal` mediumblob,
  `imagen_posterior` mediumblob,
  `imagen_lateral_derecho` mediumblob,
  `imagen_lateral_izquierdo` mediumblob,
  `etd_verificacion_detalle` enum('B','R','M') DEFAULT NULL,
  `costo_propuesto` double DEFAULT NULL,
  `precio_referencial_propuesto` double DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `precio_referencial` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orden_retiro` (`orden_retiro`),
  CONSTRAINT `FK_RE_ORDEN_RETIRO` FOREIGN KEY (`orden_retiro`) REFERENCES `re_retiro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.re_detalle_retiro_otro
CREATE TABLE IF NOT EXISTS `re_detalle_retiro_otro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `orden_retiro` int unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `serie` varchar(255) NOT NULL DEFAULT '',
  `imagen_frontal` mediumblob NOT NULL,
  `imagen_posterior` mediumblob NOT NULL,
  `imagen_lateral_izq` mediumblob NOT NULL,
  `imagen_lateral_der` mediumblob NOT NULL,
  `etd_verificacion` enum('B','R','M') DEFAULT NULL,
  `producto` int DEFAULT NULL,
  `kardex` int DEFAULT NULL,
  `costo_propuesto` double DEFAULT NULL,
  `precio_referencial_propuesto` double DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `precio_referencial` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orden_retiro` (`orden_retiro`),
  CONSTRAINT `FK_DETALLE_RETIRO_OTRO` FOREIGN KEY (`orden_retiro`) REFERENCES `re_retiro` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.re_retiro
CREATE TABLE IF NOT EXISTS `re_retiro` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agencia` varchar(50) NOT NULL,
  `calific_cliente` varchar(50) NOT NULL,
  `id_cliente` int NOT NULL,
  `nombre_cliente` varchar(255) NOT NULL,
  `cedula_cliente` varchar(50) NOT NULL,
  `direccion_cliente` varchar(50) NOT NULL,
  `vivienda_cliente` varchar(50) NOT NULL,
  `telefono_cliente` varchar(50) NOT NULL,
  `email_cliente` varchar(50) DEFAULT NULL,
  `garante_cliente` enum('SI','NO') NOT NULL,
  `ingresos_cliente` double NOT NULL DEFAULT '0',
  `recaudador_id` int NOT NULL,
  `zona_cliente` varchar(50) NOT NULL,
  `latitud_cliente` double NOT NULL,
  `longitud_cliente` double NOT NULL,
  `factura_id` int NOT NULL DEFAULT '0',
  `fecha_venta_factura` datetime NOT NULL,
  `entrada_factura` double NOT NULL DEFAULT '0',
  `cuotas_factura` int NOT NULL DEFAULT '0',
  `valor_cuota_factura` double NOT NULL,
  `total_factura` double NOT NULL,
  `fecha_ultimo_abono_factura` datetime NOT NULL,
  `valor_ultimo_abono_factura` double NOT NULL DEFAULT '0',
  `fecha_orden_retiro` datetime NOT NULL,
  `observacion_orden_retiro` text NOT NULL,
  `fecha_orden_retiro_cartera` datetime DEFAULT NULL,
  `observacion_orden_retiro_cartera` text,
  `etd_orden_retiro_cartera` enum('P','A','R') DEFAULT 'P',
  `fecha_retiro` datetime DEFAULT NULL,
  `observacion_retiro` text,
  `etd_retiro` enum('IN','FI') DEFAULT 'IN',
  `fecha_retiro_cartera` datetime DEFAULT NULL,
  `observacion_retiro_cartera` text,
  `etd_retiro_cartera` enum('P','A','R') DEFAULT 'P',
  `fecha_verificacion` datetime DEFAULT NULL,
  `observacion_verificacion` text,
  `etd_verificacion` enum('IN','FI') DEFAULT 'IN',
  `valor_cancelado_factura` double DEFAULT NULL,
  `valor_porcancelar_factura` double DEFAULT NULL,
  `valor_vencido_factura` double DEFAULT NULL,
  `valor_total_factura` double DEFAULT NULL,
  `meses_vencidos_factura` varchar(255) DEFAULT NULL,
  `fecha_verificacion_cartera` datetime DEFAULT NULL,
  `observacion_verificacion_cartera` text,
  `etd_verificacion_cartera` enum('P','A','R') DEFAULT 'P',
  `saldopropuesto_usoydeterioro` double DEFAULT NULL,
  `saldopropuesto_prod_noretirados` double DEFAULT NULL,
  `saldopropuesto_total` double DEFAULT NULL,
  `fecha_monetizacion` datetime DEFAULT NULL,
  `observacion_monetizacion` text,
  `etd_monetizacion` enum('IN','FI') DEFAULT 'IN',
  `saldo_usoydeterioro` double DEFAULT NULL,
  `saldo_prod_noretirado` double DEFAULT NULL,
  `saldo_total` double DEFAULT NULL,
  `fecha_fijacion_precio` datetime DEFAULT NULL,
  `observacion_fijacion_precio` text,
  `etd_fijacion_precio` enum('IN','FI') DEFAULT 'IN',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.secuenciales
CREATE TABLE IF NOT EXISTS `secuenciales` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `secuencial` int unsigned NOT NULL DEFAULT '0',
  `estado` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  KEY `Índice 1` (`id`),
  KEY `Índice 2` (`nombre`,`estado`),
  KEY `Índice 3` (`nombre`,`secuencial`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.sujeto
CREATE TABLE IF NOT EXISTS `sujeto` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `t_nid` enum('C','R','P') NOT NULL DEFAULT 'C',
  `t_per` enum('N','J','P') NOT NULL DEFAULT 'N',
  `cedula` char(10) DEFAULT NULL,
  `ruc` char(13) DEFAULT NULL,
  `pasaporte` varchar(20) DEFAULT NULL,
  `nombre` varchar(120) DEFAULT NULL,
  `apellido` varchar(120) DEFAULT NULL,
  `ssri` varchar(120) DEFAULT NULL,
  `es_cli` tinyint unsigned NOT NULL DEFAULT '0',
  `es_prv` tinyint unsigned NOT NULL DEFAULT '0',
  `es_emp` tinyint unsigned NOT NULL DEFAULT '0',
  `es_soc` tinyint unsigned NOT NULL DEFAULT '0',
  `telefono` varchar(20) DEFAULT NULL,
  `celular` char(15) DEFAULT NULL,
  `celular2` varchar(15) DEFAULT '',
  `smail` varchar(80) DEFAULT NULL,
  `sciudad` smallint unsigned DEFAULT NULL,
  `calific` char(3) DEFAULT NULL,
  `calif` enum('A','AA','B','C','D','E','E1','E2','N','X') NOT NULL DEFAULT 'X',
  `calburo` smallint unsigned DEFAULT NULL,
  `ssaldo` double NOT NULL DEFAULT '0',
  `scredito` double NOT NULL DEFAULT '0',
  `f_aper` date DEFAULT NULL,
  `f_ucom` date DEFAULT NULL,
  `f_nac` date DEFAULT NULL,
  `e_civil` enum('C','S','V','U','H','D','X') NOT NULL DEFAULT 'X',
  `sexo` enum('M','F','X') NOT NULL DEFAULT 'X',
  `cargas` tinyint unsigned NOT NULL DEFAULT '0',
  `ingresos` double unsigned NOT NULL DEFAULT '0',
  `pais` smallint unsigned DEFAULT NULL,
  `ciudad` smallint unsigned DEFAULT NULL,
  `parroquia` mediumint unsigned DEFAULT NULL,
  `zona` mediumint unsigned DEFAULT NULL,
  `barrio` mediumint unsigned DEFAULT NULL,
  `t_viv` mediumint unsigned NOT NULL DEFAULT '1',
  `nivel` mediumint unsigned NOT NULL DEFAULT '1',
  `tiempov` tinyint unsigned NOT NULL DEFAULT '0',
  `dir1` varchar(200) DEFAULT NULL,
  `dir2` text,
  `dir3` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `t_docex` smallint unsigned DEFAULT NULL,
  `diasplz` smallint unsigned DEFAULT NULL,
  `cupo` double NOT NULL DEFAULT '0',
  `cheque` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `credito` enum('SI','NO') NOT NULL DEFAULT 'SI',
  `vip` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `t_empre` enum('P','S','N','T','X') NOT NULL DEFAULT 'X',
  `t_prv` enum('MK','BN','SV','CM','RM','NT','BC','BK','TC','BT','XX') NOT NULL DEFAULT 'XX',
  `conta` enum('NO','SI') NOT NULL DEFAULT 'NO',
  `espe` enum('NO','SI','RI') NOT NULL DEFAULT 'NO',
  `lucro` enum('NO','SI') NOT NULL DEFAULT 'SI',
  `scode` int unsigned DEFAULT NULL,
  `idsujeto` mediumint unsigned DEFAULT NULL,
  `cyg_cedula` char(10) DEFAULT NULL,
  `cyg_nombre` varchar(150) DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `u_actu` mediumint unsigned DEFAULT '1',
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `subtipo` enum('NOR','SUP','SBA','SEP') NOT NULL DEFAULT 'NOR',
  `ncomercial` varchar(60) NOT NULL DEFAULT '',
  `jlabora` json DEFAULT NULL,
  `jrefer` json DEFAULT NULL,
  `cyg_ingreso` double NOT NULL DEFAULT '0',
  `cyg_dir_trab` varchar(100) DEFAULT '',
  `cyg_tel_trab` varchar(12) DEFAULT '',
  `cyg_emp_trab` varchar(60) DEFAULT '',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `otro_ingreso` double DEFAULT '0',
  `prop_tviv` varchar(120) DEFAULT NULL,
  `cyg_fechaingreso` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_SUJETO_CEDULA` (`cedula`),
  KEY `IDX_SUJETO_RUC` (`ruc`),
  KEY `IDX_SUJETO_PASAPORTE` (`pasaporte`)
) ENGINE=InnoDB AUTO_INCREMENT=87618 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.th_geo
CREATE TABLE IF NOT EXISTS `th_geo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data` varchar(88) DEFAULT NULL,
  `cedula` char(13) NOT NULL DEFAULT '',
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.th_registro
CREATE TABLE IF NOT EXISTS `th_registro` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `cedula` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `edad` tinyint(1) NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sarea` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `scargo` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ciudad` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `empleo` char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `cv` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `etd` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'IN',
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `fcrea` datetime DEFAULT CURRENT_TIMESTAMP,
  `visto` char(2) DEFAULT 'NO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3805 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tlog
CREATE TABLE IF NOT EXISTS `tlog` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('GRNT','INSR','UPDT','DLTE') NOT NULL,
  `tbl` varchar(25) DEFAULT NULL,
  `idu` int unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `dato` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1597 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tp_proveedor
CREATE TABLE IF NOT EXISTS `tp_proveedor` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `code` char(1) NOT NULL DEFAULT 'M',
  `nombre` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `TIPO_PROVEEDOR_UNQ` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_camion
CREATE TABLE IF NOT EXISTS `tr_camion` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `matricula` varchar(50) NOT NULL DEFAULT '0',
  `tipo` enum('INTERNO','EXTERNO') NOT NULL DEFAULT 'INTERNO',
  `marca` varchar(50) DEFAULT NULL,
  `tonelaje` double unsigned NOT NULL DEFAULT '0',
  `num_ejes` smallint unsigned NOT NULL DEFAULT '0',
  `anio` smallint unsigned NOT NULL DEFAULT '0',
  `estado` enum('ACTIVO','REPARACION','DESUSO') DEFAULT 'ACTIVO',
  `observacion` text,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_despacho
CREATE TABLE IF NOT EXISTS `tr_despacho` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `kardex_id` int unsigned NOT NULL,
  `venta_id` int unsigned NOT NULL,
  `type` enum('CLIENTE','LOGISTICA') NOT NULL DEFAULT 'CLIENTE',
  `state` int NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `Índice 1` (`id`),
  KEY `FK1` (`kardex_id`),
  KEY `FK_tr_despacho_venta` (`venta_id`),
  CONSTRAINT `FK1` FOREIGN KEY (`kardex_id`) REFERENCES `kardex` (`id`),
  CONSTRAINT `FK_tr_despacho_venta` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_dt_embarque_det_expr
CREATE TABLE IF NOT EXISTS `tr_dt_embarque_det_expr` (
  `id` int unsigned DEFAULT NULL,
  `express_id` int unsigned NOT NULL,
  `kardex_id` int unsigned DEFAULT NULL,
  `embarque_id` int unsigned NOT NULL,
  `estado` enum('NO PROCESADO','EMBARCADO','ENVIADO','ENTREGADO','ELIMINADO') NOT NULL DEFAULT 'NO PROCESADO',
  `observacion` text,
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `Índice 1` (`id`),
  KEY `FK_dt_express_embarque_tr_express_det` (`express_id`),
  KEY `FK_dt_express_embarque_tr_embarque` (`embarque_id`),
  KEY `FK_tr_dt_embarque_det_expr_kardex` (`kardex_id`),
  CONSTRAINT `FK_dt_express_embarque_tr_embarque` FOREIGN KEY (`embarque_id`) REFERENCES `tr_embarque` (`id`),
  CONSTRAINT `FK_dt_express_embarque_tr_express_det` FOREIGN KEY (`express_id`) REFERENCES `tr_express_det` (`id`),
  CONSTRAINT `FK_tr_dt_embarque_det_expr_kardex` FOREIGN KEY (`kardex_id`) REFERENCES `kardex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_dt_express_empleado
CREATE TABLE IF NOT EXISTS `tr_dt_express_empleado` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `empleado_id` mediumint unsigned NOT NULL DEFAULT '0',
  `express_id` int unsigned NOT NULL DEFAULT '0',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `Índice 1` (`id`),
  KEY `FK__empleadod` (`empleado_id`),
  KEY `FK__tr_expressd` (`express_id`),
  CONSTRAINT `FK__empleadod` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK__tr_expressd` FOREIGN KEY (`express_id`) REFERENCES `tr_express` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_embarque
CREATE TABLE IF NOT EXISTS `tr_embarque` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `fecha_salida` date DEFAULT NULL,
  `conductor_id` mediumint unsigned NOT NULL,
  `vehiculo_id` int unsigned NOT NULL DEFAULT '0',
  `puntos_recorrido` text NOT NULL,
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` enum('PENDIENTE','EMBARCANDO','EN TRANSCURSO','FINALIZADO','ELIMINADO') NOT NULL DEFAULT 'PENDIENTE',
  `fecha_embarcado` timestamp NULL DEFAULT NULL,
  `fecha_translado` timestamp NULL DEFAULT NULL,
  `fecha_finalizado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  KEY `Índice 1` (`id`),
  KEY `FK__empleado_conductor` (`conductor_id`),
  KEY `FK__tr_camion` (`vehiculo_id`),
  CONSTRAINT `FK__empleado_conductor` FOREIGN KEY (`conductor_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK__tr_camion` FOREIGN KEY (`vehiculo_id`) REFERENCES `tr_camion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_express
CREATE TABLE IF NOT EXISTS `tr_express` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_respuesta` timestamp NULL DEFAULT NULL,
  `fecha_eleccion_transporte` timestamp NULL DEFAULT NULL,
  `fecha_logistica` timestamp NULL DEFAULT NULL,
  `fecha_inicio_envio` timestamp NULL DEFAULT NULL,
  `fecha_llegada_envio` timestamp NULL DEFAULT NULL,
  `tipo_transporte` enum('INDIVIDUAL','GESTIONADO','NO PROCESADO') NOT NULL DEFAULT 'NO PROCESADO',
  `gestionado_obs` text,
  `origen` smallint unsigned NOT NULL,
  `destino` smallint unsigned NOT NULL,
  `estado_recepcion` enum('PENDIENTE','APROBADO','RECHAZADO','SIN VERIFICAR','VERIFICADO') NOT NULL DEFAULT 'PENDIENTE',
  `observacion_estado` text,
  `estado` tinyint unsigned NOT NULL DEFAULT '1',
  `user_solicitud` mediumint unsigned NOT NULL,
  `user_respuesta` mediumint unsigned DEFAULT NULL,
  `user_logistica` mediumint unsigned DEFAULT NULL,
  `especi_transporte` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tr_express_empleado` (`user_solicitud`),
  KEY `FK_tr_express_empleado_2` (`user_respuesta`),
  KEY `FK_tr_express_agencia` (`origen`),
  KEY `FK_tr_express_agencia_2` (`destino`),
  KEY `FK_tr_express_empleado_3` (`user_logistica`),
  CONSTRAINT `FK_tr_express_agencia` FOREIGN KEY (`origen`) REFERENCES `agencia` (`id`),
  CONSTRAINT `FK_tr_express_agencia_2` FOREIGN KEY (`destino`) REFERENCES `agencia` (`id`),
  CONSTRAINT `FK_tr_express_empleado` FOREIGN KEY (`user_solicitud`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK_tr_express_empleado_2` FOREIGN KEY (`user_respuesta`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK_tr_express_empleado_3` FOREIGN KEY (`user_logistica`) REFERENCES `empleado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_express_det
CREATE TABLE IF NOT EXISTS `tr_express_det` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `kardex_id` int unsigned NOT NULL,
  `express_id` int unsigned NOT NULL,
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_enviado` timestamp NULL DEFAULT NULL,
  `fecha_entregado` timestamp NULL DEFAULT NULL,
  `fecha_eliminado` timestamp NULL DEFAULT NULL,
  `estado` enum('NO PROCESADO','ENVIADO','ENTREGADO','ELIMINADO','VERIFICADO','NO TRANSPORTADO') NOT NULL DEFAULT 'NO PROCESADO',
  `observacion` text,
  PRIMARY KEY (`id`),
  KEY `FK_express_detalle_kardex` (`kardex_id`),
  KEY `FK_express_id` (`express_id`),
  CONSTRAINT `FK_express_detalle_kardex` FOREIGN KEY (`kardex_id`) REFERENCES `kardex` (`id`),
  CONSTRAINT `FK_express_id` FOREIGN KEY (`express_id`) REFERENCES `tr_express` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_express_recorrido
CREATE TABLE IF NOT EXISTS `tr_express_recorrido` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tr_express_id` int unsigned NOT NULL DEFAULT '0',
  `tr_recorrido_id` int unsigned NOT NULL DEFAULT '0',
  `estado` int NOT NULL DEFAULT '1',
  KEY `Índice 1` (`id`),
  KEY `FK__tr_express` (`tr_express_id`),
  KEY `FK__tr_recorrido_transporte` (`tr_recorrido_id`),
  CONSTRAINT `FK__tr_express` FOREIGN KEY (`tr_express_id`) REFERENCES `tr_express` (`id`),
  CONSTRAINT `FK__tr_recorrido_transporte` FOREIGN KEY (`tr_recorrido_id`) REFERENCES `tr_recorrido_transporte` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_persona_transporte
CREATE TABLE IF NOT EXISTS `tr_persona_transporte` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `vehiculo_id` int unsigned DEFAULT NULL,
  `express_id` int unsigned NOT NULL,
  `estado` tinyint unsigned NOT NULL DEFAULT '1',
  `nivel_confianza` mediumint unsigned NOT NULL DEFAULT '1',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `Key` (`id`),
  KEY `fkveh` (`vehiculo_id`),
  KEY `FK_tr_persona_transporte_tr_express` (`express_id`),
  KEY `Índice 4` (`cedula`),
  CONSTRAINT `FK_tr_persona_transporte_tr_express` FOREIGN KEY (`express_id`) REFERENCES `tr_express` (`id`),
  CONSTRAINT `fkveh` FOREIGN KEY (`vehiculo_id`) REFERENCES `tr_camion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.tr_recorrido_transporte
CREATE TABLE IF NOT EXISTS `tr_recorrido_transporte` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `camion_id` int unsigned NOT NULL,
  `conductor_id` mediumint unsigned NOT NULL DEFAULT '0',
  `fecha_salida` timestamp NULL DEFAULT NULL,
  `fecha_llegada` timestamp NULL DEFAULT NULL,
  `punto_partida` text,
  `punto_destino` text,
  `observacion` text,
  `estado` enum('INICIO','PROCESO','INCONVENIENTE','TERMINADO','HISTORIAL') NOT NULL DEFAULT 'INICIO',
  KEY `Índice 1` (`id`),
  KEY `FK__empleado` (`conductor_id`),
  KEY `FK_tr_recorrido_transporte_tr_camion` (`camion_id`),
  CONSTRAINT `FK__empleado` FOREIGN KEY (`conductor_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `FK_tr_recorrido_transporte_tr_camion` FOREIGN KEY (`camion_id`) REFERENCES `tr_camion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta
CREATE TABLE IF NOT EXISTS `venta` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `femi` date NOT NULL,
  `tipo` enum('CD','EF','TC') NOT NULL DEFAULT 'EF',
  `t_doc` enum('F','N') NOT NULL DEFAULT 'F',
  `agencia` smallint unsigned DEFAULT NULL,
  `cliente` int unsigned DEFAULT NULL,
  `garante` int unsigned DEFAULT NULL,
  `parentesco_gar_a_cli` enum('ABUELO','PADRE','MADRE','HERMANO','TIO','SOBRINO','NIETO','AMIGO','CONOCIDO','NO DEFINIDO') NOT NULL DEFAULT 'NO DEFINIDO',
  `t_cli` enum('NN','EE','PP') NOT NULL DEFAULT 'NN',
  `etd` enum('IN','SL','GH','AP','RE','FA','AN','EL','VL','PDE','ELI') NOT NULL,
  `estado_despacho` enum('NO DEFINIDO','DESPACHADO','LOGISTICA') NOT NULL DEFAULT 'NO DEFINIDO',
  `estab` varchar(3) DEFAULT NULL,
  `ptoemi` varchar(3) DEFAULT NULL,
  `seq` mediumint unsigned DEFAULT NULL,
  `numero_solicitud` mediumint unsigned DEFAULT NULL,
  `base` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `base_iva0` double NOT NULL DEFAULT '0',
  `base_iva` double NOT NULL DEFAULT '0',
  `m_iva` double NOT NULL DEFAULT '0',
  `m_ice` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `t_ret_renta` double NOT NULL DEFAULT '0',
  `t_ret_iva` double NOT NULL DEFAULT '0',
  `t_con_ret` double NOT NULL DEFAULT '0',
  `f_nax` datetime DEFAULT NULL,
  `cax` varchar(49) DEFAULT NULL,
  `direccion_despacho` tinytext,
  `nax` varchar(49) DEFAULT NULL,
  `ref_pago` varchar(50) DEFAULT NULL,
  `plz_pago` date DEFAULT NULL,
  `etc` enum('IN','CT','ER') NOT NULL DEFAULT 'IN',
  `xml` json DEFAULT NULL,
  `t_nid` enum('C','R','P') DEFAULT NULL,
  `no_retencion` varchar(20) DEFAULT NULL,
  `aut_retencion` varchar(50) DEFAULT NULL,
  `guia` varchar(20) DEFAULT NULL,
  `mas` varchar(255) DEFAULT NULL,
  `vendedor` mediumint unsigned DEFAULT NULL,
  `cajero` mediumint unsigned DEFAULT NULL,
  `u_crea` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `m_cuota` double DEFAULT NULL,
  `num_cuotas` int DEFAULT NULL,
  `entrada` double NOT NULL DEFAULT '0',
  `estado_credito` enum('NO_APLICA','INICIADO','PROCESO','FINALIZADO') NOT NULL DEFAULT 'NO_APLICA',
  PRIMARY KEY (`id`),
  UNIQUE KEY `docventa_unique` (`t_doc`,`estab`,`ptoemi`,`seq`),
  KEY `IDX_VTA_CLI` (`cliente`),
  KEY `IDX_VTA_FEMI` (`femi`),
  KEY `FK_VTA_SERIE` (`agencia`),
  KEY `FK_VTA_VENDE` (`vendedor`),
  CONSTRAINT `FK_VTA_SERIE` FOREIGN KEY (`agencia`) REFERENCES `agencia` (`id`),
  CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `sujeto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta_anticipo
CREATE TABLE IF NOT EXISTS `venta_anticipo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cliente` int NOT NULL,
  `agencia` smallint unsigned DEFAULT NULL,
  `cajero` mediumint unsigned DEFAULT NULL,
  `f_pago` tinyint unsigned DEFAULT NULL,
  `banco` int DEFAULT NULL,
  `cuenta` int DEFAULT NULL,
  `tarjeta` int DEFAULT NULL,
  `plazo` smallint unsigned DEFAULT NULL,
  `fecha` date NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `f_giro` date DEFAULT NULL,
  `girador` varchar(60) DEFAULT NULL,
  `monto` double NOT NULL,
  `saldo` double NOT NULL DEFAULT '0',
  `recargo` double NOT NULL DEFAULT '0',
  `etd` enum('IN','VL','SD','UT','DV','DP','XU') NOT NULL DEFAULT 'IN',
  `etc` enum('I','C','S','D','IN','CT') NOT NULL DEFAULT 'IN',
  `motivo` enum('NA','FL','DC') NOT NULL DEFAULT 'NA',
  `obs` varchar(255) DEFAULT NULL,
  `u_crea` int DEFAULT NULL,
  `u_actu` int DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta_desc_extra
CREATE TABLE IF NOT EXISTS `venta_desc_extra` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `factura_id` int unsigned NOT NULL DEFAULT '0',
  `estado` enum('PD','AP','RE') NOT NULL DEFAULT 'PD',
  `f_crea` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `f_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `Índice 1` (`id`),
  KEY `FK_venta_desc_extra_venta` (`factura_id`),
  CONSTRAINT `FK_venta_desc_extra_venta` FOREIGN KEY (`factura_id`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta_det
CREATE TABLE IF NOT EXISTS `venta_det` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `venta` int unsigned DEFAULT NULL,
  `producto` int unsigned DEFAULT NULL,
  `series` json DEFAULT NULL,
  `descripcion` varchar(255) NOT NULL,
  `kardexs` json NOT NULL,
  `cantidad` mediumint unsigned NOT NULL DEFAULT '1',
  `pvp` double NOT NULL,
  `precio` double NOT NULL,
  `ispromo` enum('si','no') NOT NULL DEFAULT 'no',
  `referencia_product` mediumint unsigned DEFAULT NULL,
  `valor_entrada` double DEFAULT NULL,
  `cuota_mensual` double DEFAULT NULL,
  `numero_cuotas` double DEFAULT NULL,
  `descuento` double NOT NULL DEFAULT '0',
  `restante_sin_regalo` double NOT NULL DEFAULT '0',
  `valor_de_regalos` double NOT NULL DEFAULT '0',
  `base` double DEFAULT NULL,
  `t_iva` enum('S','N') DEFAULT NULL,
  `piva` tinyint unsigned DEFAULT NULL,
  `m_iva` double NOT NULL DEFAULT '0',
  `m_ice` double DEFAULT '0',
  `datos` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DET_VTA_PRD` (`producto`),
  KEY `FK_DET_VTA_VENT` (`venta`),
  CONSTRAINT `FK_DET_VTA_PRD` FOREIGN KEY (`producto`) REFERENCES `producto` (`id`),
  CONSTRAINT `FK_DET_VTA_VENT` FOREIGN KEY (`venta`) REFERENCES `venta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=455 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta_gestion
CREATE TABLE IF NOT EXISTS `venta_gestion` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cedula` char(10) DEFAULT NULL,
  `nombres` varchar(150) NOT NULL DEFAULT '',
  `direccion` varchar(150) NOT NULL DEFAULT '',
  `producto` varchar(80) NOT NULL DEFAULT '',
  `telefono` varchar(15) NOT NULL DEFAULT '',
  `agenda` date DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `t_mobi` enum('MOBILE','NO_MOBILE','XX','MÓVIL') DEFAULT 'XX',
  `respon` mediumint unsigned DEFAULT NULL,
  `agencia` smallint unsigned DEFAULT NULL,
  `ftran` datetime DEFAULT CURRENT_TIMESTAMP,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `email` varchar(80) DEFAULT NULL,
  `tipo` enum('NUEVA','SEGUIMIENTO','CIERRE') NOT NULL DEFAULT 'NUEVA',
  `idref` int unsigned DEFAULT NULL,
  `interes` enum('ALTO','MEDIO','BAJO','NA') NOT NULL DEFAULT 'ALTO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19209 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.venta_proforma
CREATE TABLE IF NOT EXISTS `venta_proforma` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `femi` date NOT NULL,
  `tipo` enum('CD','EF','TC') NOT NULL DEFAULT 'EF',
  `agencia` smallint unsigned DEFAULT NULL,
  `cliente` int unsigned DEFAULT NULL,
  `t_cli` enum('NN','EE','PP') NOT NULL DEFAULT 'NN',
  `etd` enum('IN','EL','VL') NOT NULL DEFAULT 'IN',
  `base` double NOT NULL DEFAULT '0',
  `descuento` double NOT NULL DEFAULT '0',
  `base_iva0` double NOT NULL DEFAULT '0',
  `base_iva` double NOT NULL DEFAULT '0',
  `m_iva` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `t_nid` enum('C','R','P') DEFAULT NULL,
  `xml` json DEFAULT NULL,
  `vendedor` mediumint unsigned DEFAULT NULL,
  `f_crea` datetime DEFAULT CURRENT_TIMESTAMP,
  `f_actu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_VTA_PROF_CLI` (`cliente`),
  KEY `IDX_VTA_PROF_FEMI` (`femi`),
  KEY `FK_VTA_PROF_AGENCIA` (`agencia`),
  KEY `FK_VTA_VENDE` (`vendedor`),
  CONSTRAINT `VENTA_PROFORMA_CLIENTE` FOREIGN KEY (`cliente`) REFERENCES `sujeto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_cliente
CREATE TABLE IF NOT EXISTS `wb_cliente` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `apellido` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `tid` char(1) NOT NULL DEFAULT 'C',
  `cedula` varchar(13) NOT NULL,
  `celular` char(10) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `f_crea` date DEFAULT NULL,
  `uvisita` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_WBCLI_EMAIL` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_compras
CREATE TABLE IF NOT EXISTS `wb_compras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('ef','tc') NOT NULL DEFAULT 'ef',
  `email` varchar(80) NOT NULL DEFAULT '',
  `celular` char(10) NOT NULL DEFAULT '',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_ati` datetime DEFAULT NULL,
  `deta` json DEFAULT NULL,
  `detalle_pago` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_crediya
CREATE TABLE IF NOT EXISTS `wb_crediya` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cedula` char(10) NOT NULL DEFAULT '',
  `nombre` varchar(180) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `f_ati` datetime DEFAULT NULL,
  `deta` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_fb_imagenes
CREATE TABLE IF NOT EXISTS `wb_fb_imagenes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `descripcion` varchar(150) NOT NULL DEFAULT '0',
  `version` int unsigned NOT NULL DEFAULT '0',
  `imagen` longtext NOT NULL,
  `estado` tinyint unsigned NOT NULL DEFAULT '1',
  `wb_fb_imagenes` int unsigned DEFAULT NULL,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_horarios
CREATE TABLE IF NOT EXISTS `wb_horarios` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `dia` varchar(50) NOT NULL,
  `horas` varchar(50) DEFAULT NULL,
  `estado` tinyint NOT NULL DEFAULT '1',
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_imagenes
CREATE TABLE IF NOT EXISTS `wb_imagenes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) DEFAULT NULL,
  `imagen` longtext NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_interaccion
CREATE TABLE IF NOT EXISTS `wb_interaccion` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `url` text NOT NULL,
  `date` date NOT NULL,
  `hour` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42454 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_mensajes
CREATE TABLE IF NOT EXISTS `wb_mensajes` (
  `id` mediumint NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `asunto` varchar(200) NOT NULL,
  `mensaje` text NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `etd` tinyint NOT NULL DEFAULT '1',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_preguntas
CREATE TABLE IF NOT EXISTS `wb_preguntas` (
  `id` mediumint NOT NULL AUTO_INCREMENT,
  `pregunta` text NOT NULL,
  `respuesta` text NOT NULL,
  `etd` tinyint NOT NULL DEFAULT '1',
  `f_crea` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.wb_seccion
CREATE TABLE IF NOT EXISTS `wb_seccion` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('B','C','P') NOT NULL DEFAULT 'B',
  `narchivo` varchar(20) DEFAULT NULL,
  `orden` tinyint unsigned DEFAULT NULL,
  `version` smallint unsigned NOT NULL DEFAULT '1',
  `categoria` smallint unsigned DEFAULT NULL,
  `producto` int unsigned DEFAULT NULL,
  `tpago` enum('cy','tc','ef') DEFAULT NULL,
  `ftran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.zona
CREATE TABLE IF NOT EXISTS `zona` (
  `id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `idsis` smallint DEFAULT NULL,
  `ciudad` smallint unsigned DEFAULT NULL,
  `nombre` varchar(60) NOT NULL,
  `u_actu` mediumint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ZONA_CIU` (`ciudad`),
  CONSTRAINT `FK_ZONA_CIU` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.zt_cargos
CREATE TABLE IF NOT EXISTS `zt_cargos` (
  `tipo` tinyint unsigned NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `rol` char(3) NOT NULL DEFAULT '',
  `proceso` enum('cm','lg','fn','th','gg','ss') NOT NULL DEFAULT 'cm',
  `aprueba` varchar(80) DEFAULT NULL,
  `scode` mediumint DEFAULT NULL,
  `etd` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`tipo`),
  UNIQUE KEY `UNIQ_CARGOS_ROL` (`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla test_gpi.zt_rutas
CREATE TABLE IF NOT EXISTS `zt_rutas` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `ruta` varchar(50) NOT NULL DEFAULT '',
  `tipo` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ_ST_RUTAS` (`ruta`),
  KEY `IDX_RUTA` (`ruta`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
